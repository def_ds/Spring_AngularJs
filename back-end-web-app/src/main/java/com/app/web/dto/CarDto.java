package com.app.web.dto;

import java.util.Date;

public class CarDto {


    private Date added;
    private Integer id;
    private String vin;
    private String year;
    private String mark;
    private String model;
    private String fuelType;
    private String description;
    private int mileag;


    public CarDto() {
    }

    public Date getAdded() {
        return added;
    }

    public CarDto setAdded(Date added) {
        this.added = added;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public CarDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getVin() {
        return vin;
    }

    public CarDto setVin(String vin) {
        this.vin = vin;
        return this;
    }

    public String getYear() {
        return year;
    }

    public CarDto setYear(String year) {
        this.year = year;
        return this;
    }

    public String getMark() {
        return mark;
    }

    public CarDto setMark(String mark) {
        this.mark = mark;
        return this;
    }

    public String getModel() {
        return model;
    }

    public CarDto setModel(String model) {
        this.model = model;
        return this;
    }

    public String getFuelType() {
        return fuelType;
    }

    public CarDto setFuelType(String fuelType) {
        this.fuelType = fuelType;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public CarDto setDescription(String description) {
        this.description = description;
        return this;
    }

    public int getMileag() {
        return mileag;
    }

    public CarDto setMileag(int mileag) {
        this.mileag = mileag;
        return this;
    }

}
