package com.app.web.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.*;


@MappedSuperclass
abstract class AbstractRefType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
