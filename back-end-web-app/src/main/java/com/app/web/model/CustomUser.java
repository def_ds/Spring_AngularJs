package com.app.web.model;


import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

public class CustomUser extends User {
    public CustomUser (com.app.web.entity.User entity){
        super(entity.getUsername(), entity.getPassword(), AuthorityUtils.createAuthorityList(entity.getRoleType().stream().map(it -> it.getName()).toArray(size -> new String[size])));
    }

}
