package com.app.web.service;

import com.app.web.dto.UserDto;
import com.app.web.entity.User;
import com.app.web.repository.RoleRepository;
import com.app.web.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


@Service
public class UserService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;


    @Autowired
    private RoleRepository roleRepository;


    public UserDto saveUser(UserDto userDto){
        User user = getUser(userDto);
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        User savedUser = userRepository.save(user);
        UserDto savedUserDto = getUserDto(savedUser);
        return savedUserDto;
    }
    public UserDto isUserInDB(UserDto userDto){
        if(userRepository.findByUsername(userDto.getUsername()) != null){
            return userDto;
        }
        if(userRepository.findByEmail(userDto.getEmail()) != null){
            return userDto;
        }
        return null;
    }


    private UserDto getUserDto(User user){
        ModelMapper modelMapper = new ModelMapper();
        UserDto userDto = modelMapper.map(user, UserDto.class);
        return userDto;
    }


    private User getUser(UserDto userDto){
        ModelMapper modelMapper = new ModelMapper();
        User user = modelMapper.map(userDto, User.class);
        return user;
    }


}
