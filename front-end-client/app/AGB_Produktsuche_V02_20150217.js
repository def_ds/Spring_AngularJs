
export function start() {
    var termsAndConditions = [
        /*0*/{header: '\n1. Allgemeines\n\n', description: '1.1 Diese Allgemeine Geschäftsbedingungen („AGB“) der HEROLD Business Data GmbH („HEROLD“) kommen bei Bereitstellung von Informationen zu Produkten und Dienstleistungen des Kunden („INHALTE“) auf HEROLD.at und HEROLD mobile im Rahmen des Service „Produkt & Servicedarstellungenauf den HEROLD Plattformen“ zur Anwendung.\n\n1.2 Die Bereitstellung der INHALTE durch den Kunden erfolgt regelmäßig, sodass die Aktualität der INHALTE gewährleistet ist. Herold behält sich das Recht vor, nicht aktuelle Inhalte zu löschen..\n\n'},

        /*1*/{header: '', description: '\nkommen, wird klarstellend festgehalten, dass sämtliche von HEROLD bereitgestellten Inhalte urheberrechtlich geschützt sind. Derartige Inhaltedürfen lediglich im Rahmen des gegenständlichen Services und nur für die Dauer der Vereinbarung verwendet werden, jede darüber hinausgehende Verwendung der Inhalte durch den Kunden ist unzulässig und kann'
        + 'schadenersatzrechtliche Konsequenzen nach sich ziehen.\n\n'},

        /*2*/{header: '2. Rechte und Pflichten der Vertragsparteien', description: '\n2.1 Die Struktur der bereitgestellten INHALTE und der zugehörigen Felder hat nach den Vorgaben von HEROLD zu erfolgen. Mit Bereitstellung der INHALTE an HEROLD durch den Kunden erteilt dieser HEROLD das zeitlich auf die Dauer dieser Vereinbarung beschränkte Recht zur Zurverfügung-stellung der INHALTE auf der Internetplattform von HEROLD. Eine Ver-'
        + 'pflichtung zur Bereitstellung der Inhalte auf der Internetplattform von HEROLD wird hierdurch nicht begründet.'
        + '\n\n2.2 HEROLD übernimmt keine Garantie für die vollständige Übernahme von Feeds, Dateien oder manuellen Eingaben. So werden z. B. falsch formatierte Produkte oder solche, die bestimmte Stoppworte enthalten nicht über-nommen.'
        + '\n\n2.3 HEROLD wird INHALTE auf der Internetplattform von HEROLD so darstellen, dass im Rahmen der Darstellung ein Link auf eine Landing Page, Website oder einen Webshop des Kunden platziert werden kann.'
        + '\n\n2.4 Der Kunde bestätigt, dass diesem sämtliche Rechte, insbesondere die urheberrechtlichen Verwertungsrechte an den INHALTEN in vollem Umfang zustehen und der Kunde zur Erteilung von Verwertungsrechten an HEROLD, wie in Punkt 2.1 beschrieben, berechtigt ist. Im Falle einer Geltendmachung von Ansprüchen berechtigter Dritter gegenüber HEROLD, hat der Kunde HEROLD auf erste Aufforderung hin in vollem Umfang schad- und klaglos zu halten.'
        + '\n\n2.5 Der Kunde ist dafür verantwortlich, dass die bereitgestellten INHALTE nicht gegen das Gesetz oder die guten Sitten verstoßen, insbesondere dass mit Bereitstellung der INHALTE nicht gegen wettbewerbsrechtliche und sonstige Vorschiften im Zusammenhang mit der Bewerbung von Produkten und Dienstleistungen verstoßen wird und keine Rechte Dritter verletzt werden. Im Falle einer Geltendmachung von Ansprüchen Dritter gegenüber HEROLD, hat der Kunde HEROLD auf erste Aufforderung hin in vollem'
        + 'Umfang schad- und klaglos zu halten. Der Kunde nimmt zur Kenntnis, dass HEROLD keine Verpflichtung zur Prüfung der Rechtmäßigkeit und Zu- lässigkeit der bereitgestellten INHALTE hat. HEROLD ist berechtigt, INHALTE jederzeit abzulehnen oder nach Zurverfügungstellung wieder zu entfernen, sofern sich für HEROLD der begründete Verdacht ergibt, dass die INHALTE gegen gesetzliche Bestimmungen oder gegen die guten Sitten verstoßen, Rechte Dritter verletzen oder mit den Geschäftsprinzipien von HEROLD in Widerspruch stehen.'
        + '\n\n2.6 Der Kunde ist dafür verantwortlich, dass sämtliche mit der Bereitstellung von INHALTEN allenfalls bestehenden Produkt-Kennzeichnungspflichten gemäß österreichischem oder EU-Recht (insbesondere auch jene für energieverbrauchsrelevante Produkte) eingehalten werden. HEROLD sorgt für die hierzu erforderlichen technischen Voraussetzungen auf der Internetplattform von HEROLD. Im Fall einer Geltendmachung von Ansprüchen Dritter oder behördlicher Ansprüche gegenüber HEROLD, hat der Kunde HEROLD auf erste Aufforderung hin in vollem Umfang schad- und klaglos zu halten.'
        + '\n\n2.7 Sollte es zu einer Bereitstellung von Inhalten (Texte, Fotos, Grafiken, etc.) durch HEROLD im Rahmen der Produkt- und Dienstleistungsdarstellungen'},

        /*3*/{header: '3. Entgelt', description: '\n3.1 Die Bereitstellung von INHALTEN auf der Internetplattform HEROLD erfolgt bis auf Widerruf kostenfrei für den Kunden.'
        + '\n\n3.2 HEROLD behält sich das Recht vor, für zusätzliche Services im Rahmen der Darstellung von INHALTEN gesondert ein entsprechendes Entgelt mit dem Kunden zu vereinbaren oder die gegenständliche Bereitstellung von INHALTEN von der Bezahlung eines Entgelts abhängig zu machen.\n\n'},

        /*4*/{header: '4. Vertragsdauer', description: '\n4.1 Diese Vereinbarung zur Bereitstellung von INHALTEN wird auf unbestimmte Dauer abgeschlossen und tritt mit Bereitstellung der INHALTE und Bestätigung dieser AGB in Kraft. HEROLD ist berechtigt, die Vereinbarung jederzeit durch Beseitigung der INHALTE zu beenden, wobei keine ge-'
        + 'sonderte Bekanntgabe der Beendigung erforderlich ist. Eine Beendigung der Inanspruchnahme des Services durch den Kunden kann jederzeit durch manuelle Löschung der Produkt- und Servicedarstellungen in der herold.at Userzone erfolgen. Im Falle des Uploads via .xls file bzw. bei Anbindung eines .xml feeds kann die Beendigung der Inanspruchnahme des Services durch schriftliche Mitteilung an unser Serviceteam unter kundenservice@herold.at erfolgen. HEROLD entfernt daraufhin die ent-sprechenden Daten aus seiner Datenbank.'},

        /*5*/{header: '\n5. Gewährleistungsausschluss', description: '\n5.1 HEROLD übernimmt keine Gewährleistung dafür, dass die bereitgestellten INHALTE dauerhaft und ohne Unterbrechungen bereitgestellt werden. Darüber hinaus wird keine Zusage betreffend einer Erreichung eines bestimmten Umsatzes oder Geschäftserfolges aufgrund der Bereitstellung von INHALTEN erteilt.'
        + '\n5.2 HEROLD haftet für Schäden im Zusammenhang mit der Bereitstellung der INHALTE nur bei Vorsatz und grober Fahrlässigkeit durch HEROLD. Sämtliche Ansprüche sind binnen drei Monaten ab Kenntnis des Schadens geltend zu machen. Eine Haftung für Folgeschäden, insbesondere für entgangenen Gewinn, ist ausdrücklich ausgeschlossen.'},

        /*6*/{header: '6. Zustimmung zur Verwendung personenbezogener Daten', description: '\n6.1 Mit Auftragserteilung erklärt sich der Kunde gemäß § 8 Abs. 1 Ziffer 2 DSG 2000 einverstanden, dass die im Rahmen der Registrierung anzu-gebenden Daten sowie die INHALTE durch HEROLD erfasst, gespeichert und für Marketingzwecke von HEROLD verwendet werden, dies auch über die Dauer gegenständlicher Vereinbarung hinaus.'
        + '\n\n6.2 Der Kunde erklärt sich mit Angabe seiner Telefonnummer und seiner elektronischen Postadresse ausdrücklich einverstanden, von HEROLD Telefonanrufe und elektronische Post zu Werbe- und Marketingzwecken zu erhalten.'
        + '\n\n6.3 Diese Zustimmungserklärungen können jederzeit widerrufen werden.'},


        /*7*/{header: '7. Sonstiges', description: '\n7.1 Die allfällige Unwirksamkeit einzelner Bestimmungen dieser AGB lässt die Geltung der restlichen Bestimmungen unberührt. An die Stelle der unwirksamen Bestimmung tritt eine wirksame Bestimmung, die ersterer nach deren Sinn und Zweck rechtlich und wirtschaftlich am nächsten kommt.'
        + '\n\n7.2 Es gilt österreichisches Recht.'
        + '\n\n7.3 Gerichtsstand für beide Teile ist das sowohl sachlich zuständige als auch wertzuständige Gericht für den ersten Wiener Gemeindebezirk.'},

    ];

    pdfMake.fonts = {
            Helvetica: {
                normal: 'HelveticaNeueLT-LightCond.ttf',
                bold: 'HelveticaNeue-BoldCond.ttf',
            }
        };



    function renderTermsAndConditions(termsAndCons) {
        var content = [];

        content.push(

            {
                columns: [
                    [
                        {text: termsAndCons[0].header,   style: 'terms_header'},
                        {text: termsAndCons[0].description,   style: 'terms'},
                        {text: termsAndCons[2].header,   style: 'terms_header'},
                        {text: termsAndCons[2].description,   style: 'terms'},
                    ],

                    [
                        {text: termsAndCons[1].header,   style: 'terms_header'},
                        {text: termsAndCons[1].description,   style: 'terms'},
                        {text: termsAndCons[3].header,   style: 'terms_header',},
                        {text: termsAndCons[3].description,   style: 'terms'},
                        {text: termsAndCons[4].header,   style: 'terms_header'},
                        {text: termsAndCons[4].description,   style: 'terms'},
                        {text: termsAndCons[5].header,   style: 'terms_header',},
                        {text: termsAndCons[5].description,   style: 'terms'},
                        {text: termsAndCons[6].header,   style: 'terms_header',},
                        {text: termsAndCons[6].description,   style: 'terms'},
                        {text: termsAndCons[7].header,   style: 'terms_header',},
                        {text: termsAndCons[7].description,   style: 'terms'},

                    ]
                ]
            }
        );

        return content;
    }


    var dd = {

        content: [
            renderTermsAndConditions(termsAndConditions),
        ],

        images: {
            mySuperImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAABHCAIAAAAC1/wuAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATuSURBVHhe7dzhSxtnHMDx/SeKdrKVjdp1shdzloFvOljpCnaMUcYY1b2Y615MKXRv2u1daRxFYQzK+iYtcxskL3xhXixQ3FihJ1MaGQqxxc5YZdEZNI7Eyxp9EpPncpe7KPGX8/vj86q5u1zKfTWJT/JCa1MzALFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRjk6iZ4aMVK5sFsKXTlo20zju1R4wMupfa5iMEThley/uxkyvJeLT0R8Dvd1t6lDl2nsGh++NTzyaX3y2lt5WeznNdnr12WL80W/j90YGegqnV1Tb/2GJS6GE2tHjZFIrib/nY7+PB2/0nT6uH9bXSFTbTCM70eJknkavv1deaWtH721jeUttUMtsLT+83fdaa8kxDy/RkjHTjyP6g/UzEtU20zRIos8v3H8igy/vHi2v7d1bxnpW3Vb7ZNcf3jrbUjysiETzk4mH+7v0g/sUiWqbaRom0VxuJXq1eNV2DkaWTPXv+xtzKTLQWTismERz5qbxbbd2cJ8iUW0zjcdEV41gIDDkzs0vz79key+biZlJw3amZxMpyw+Hrfngx+rEjl2JJCv+Cs2/1ExUHrsXq9lk5Ir9ee43UXM17vA4d2bmcTJd4cfNxsT1VyzH9yMS1TbTeEw0Eerd29e9Gs6t64uxhfILN5MI9atb3xye1gs1M4v3R+zeVdrV0t03fH8xo+eQnRruUtscfKIlTyUcdPYFZ9Jqj8Js/3Xn7JF434hEtc00YhNtbvtsbFltvDsliVqfT5pPwn0dxX3tdXwaeqI3uvegDivRis8LPN51wyJRbTON3EQtl7tjosnIwLHy3W28ejX6r9qnMBISbTo3MrWh9lFDon5jvbxqG8dE83++czdTP1x0Orfq15/lt+jG9PA5das1Ufc/O5z2PcRE933XDYtEvY5jou6nrJkarj/La1Ez/tNHhV1I1EdI1OvUJ1Hnd3Qnp+eWy9/lLP8DJon6CIl6nfok6n6208m48fPXF9pLlgGRqI+QqNdxTNR9CWX2l+jK7B+j1RJdHvt8b5GQE8tLXCGJXvh+ZlPto4ZE/aa2y8txLxGJ7k42Gb32RvGA1kS354Lvnyi5RzsnPgjO6SsYJCT6+jcT69p5kajf+DvRXO6/P0e6X1QHrLB0IZuK3b1c5TMix9/uvxtLWfY87KULbW99MhR9qr+kYOmC79Ql0dRsNBQKu/RLoFctfPd4bi2nL14Lz+qr4krW6NotADTTa0vqjz7WWVqttM6uygLATGrF/oh7M3nnw53f4ZZEzfRqlf1tTosFgP5Tl0Q9TcYYandxL5V1fRVdUduqSRmBM4Vb67aM3uUUHo71GXiNwzJ6H/JZoid7QwtqWzWliTa39Xw3ZXnK6n2qfhjN5Rx0onwYzY+OVqL1+ki3yznIRPlIt18dtUR35L8YZTT6IObxi1FiD34ddffFKC5n34nmX0XvfjHKzcvvnCo/K587OokCDYlEAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQBwZqa/wd0Ed3Ls5EBJgAAAABJRU5ErkJggg=='
        },

        header:  function() {

            return {
                table: {
                    widths: [140, '*'],
                    body: [
                        [{image: 'mySuperImage', width: 140, height: 40, margin: [-5, 0, 0, -2], border: [false, false, false, true]},
                            {text: 'Allgemeine Geschäftsbedingungen der HEROLD Business Data GmbH für\n das Service „Produkt- & Servicedarstellungen auf den HEROLD Plattformen“', fontSize: 11, alignment: 'right',  bold: true, margin: [0, 5, 0, 0], border: [false, false, false, true]}],
                    ]
                }, margin: [21,21]
            };
        },


        footer: function() {
            return {
                table: {
                    widths: ['*', '*'],
                    body: [
                        [{text: '',border: [false, false, false, false]},
                            {text: 'AGB Produktsuche (V02_20150217)', fontSize: 6 , alignment: 'right',  bold: false, border: [false, false, false, false], margin: [0, 0, 0, 0]  }],
                        [{colSpan: 2, text: 'HEROLD Business Data GmbH, A-2340 Mödling, Guntramsdorfer Straße 105, Telefon: +43 (0) 2236/401-38133, Fax +43 (0) 2236/401-35160 E-Mail: kundenservice@herold.at, www.herold.at Gerichtsstand Wien, UID: ATU57270679, DVR: 0871885, FN: 233171z AGB_CMS_Applikation (V19_20170522)', fontSize: 6, alignment: 'left', margin: [0, 0, 0, 0], border: [false, true, false, false]},{}]
                    ]
                }, margin: [21,0]
            };

        },


        // Styles
        styles: {
            terms_header: {
                bold: true,
                fontSize: 8,
                alignment: 'justify'
            },
            terms: {
                fontSize: 8.3,
                alignment: 'justify'
            },
        },

        // Default Styles
        defaultStyle: {
            columnGap: 10,
            font: 'Helvetica',
            // set font here to Calibri!!!
        },


        // Page margins
        pageMargins: [ 21, 70, 21, 55 ]
    };

    return dd;
}




