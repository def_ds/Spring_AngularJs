export default app => {
  app.config([configFn]);

  function configFn($routeProvider,$qProvider) {
      $qProvider.errorOnUnhandledRejections(false);
      $routeProvider
          .when('/', {
          templateUrl: '/components/home/car.html'
          });

  }

}
