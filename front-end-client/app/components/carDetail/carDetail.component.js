import template from './carDetail.html';
import controller from './carDetail.controller';

let carDetailComponent = function () {
    return {
        restrict: 'EA',
        scope: {},
        template: template,
        controller: controller,
        controllerAs: 'detCtrl',
        bindToController: true
    };
};
export default carDetailComponent;