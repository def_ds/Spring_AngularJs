import carDetailComponent from './carDetail.component';

export default app => {
    app.config(($stateProvider) => {
        $stateProvider
            .state('carDetail', {
                url: '/carDetail',
            });
    }).directive('carDetail', carDetailComponent);
}