import template from './carDetail.html';
import controller from './car.controller';

let carDetailComponentTest = function () {
    return {
        restrict: 'EA',
        scope: {},
        template: template,
        controller: controller,
        controllerAs: 'detCtrl',
        bindToController: true
    };
};
export default carDetailComponentTest;