import template from '../../components/login/login.html';
import controller from './login.controller';

let loginComponent = function () {
    return {
        restrict: 'EA',
        scope: {},
        template: template,
        controller: controller,
        controllerAs: 'loginCtrl',
        bindToController: true
    };
};
export default loginComponent;