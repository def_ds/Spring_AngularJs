import registerComponent from './register.component';
import registerController from './register.controller'


export default app => {
    app.config(($stateProvider) => {
        $stateProvider
            .state('/register', {
                url: '/register',
                template: '<register></register>',
                controller: registerController
            })
    }).directive('register', registerComponent);
}