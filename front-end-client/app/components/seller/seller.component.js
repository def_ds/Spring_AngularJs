import template from './seller.html';
import controller from './seller.controller';

let sellerComponent = function () {
    return {
        restrict: 'EA',
        scope: {},
        template: template,
        controller: controller,
        controllerAs: 'sellerCtrl',
        bindToController: true
    };
};

export default sellerComponent;
