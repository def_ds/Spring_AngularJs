class TestModalController {

    constructor($scope, $rootScope, $mdDialog) {
        this.$scope = $scope;
        this.$rootScope = $rootScope;
        this.$mdDialog = $mdDialog;
    }

    closeModal(){
        this.$mdDialog.cancel();
    }

}
TestModalController .$inject = ['$scope', '$rootScope','$mdDialog'];
export default TestModalController ;