class CarsService {

    constructor($http) {
        this.$http = $http;
        this.dataUrl = 'http://localhost:8082';
    }

    getCarList(){
        return this.$http({
            method: 'GET',
            url: this.dataUrl + '/car'
        });
    }

    saveCar(car){
        return this.$http({
            method: 'POST',
            url: this.dataUrl + '/car',
            data: car
        });
    }

    getCarDetail(id){
        return this.$http({
            method: 'GET',
            url: this.dataUrl + `/car/${id}`
        });
    }

}
export default CarsService;



