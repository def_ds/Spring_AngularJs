export function start() {
    var termsAndConditions = [
        /*0*/{
            header: '§ 1 Zustandekommen des Vertragsverhältnisses',
            description: 'Vom Kunden unterzeichnete Bestellformulare gelten als Angebot, welches HEROLD berechtigt ist, innerhalb von 4 Wochen ab Unterzeichnung des Bestellformulars abzulehnen. Das Angebot gilt als von HEROLD angenommen, wenn es nicht innerhalb dieser Frist schriftlich (auch Fax und E-Mail) oder mündlich zurückgewiesen wurde. Zur Fristwahrung genügt bei mündlicher Ablehnung der Ausspruch innerhalb der Frist bzw. bei schriftlicher Ablehnung die rechtzeitige Absendung. Für die Annahme des Angebots durch HEROLD ist allein die schriftliche Bestellung laut Bestellschein maßgeblich, mündliche Erläuterungen oder Zusagen werden keinesfalls Vertragsinhalt. Auch Bestellungen, die über Internet-Seiten aufgegeben werden, gelten lediglich als Angebot zum Vertragsabschluss; die Zusendung einer Zugangsbestätigung gilt nicht als Annahme der Kundenbestellung.'
        },

        /*1*/{
            header: '\n§ 2 Vertragsgegenstand',
            description: 'Vertragsgegenstand ist der Kauf des Datenträgers (CD-ROM, DVD, USB-Stick etc.) und – sofern vorhanden – die beiliegenden Dokumentationen, der Erwerb einer nicht ausschließlichen Nutzungsbewilligung (Lizenz) an der auf dem Datenträger enthaltenen Software sowie an der Datenbank für die vereinbarte Dauer in der jeweils aktuellen Version zum Zeitpunkt der Bestellung. Die Nutzungsbewilligung umfasst ausschließlich das Recht zur bestimmungsgemäßen Nutzung der Datenbank (samt den enthaltenen Daten) und der Software im Sinne des § 3 dieser Allgemeinen Geschäftsbedingungen. Ein Anspruch des Lizenznehmers auf Lieferung des Quellcodes besteht nicht. Die Installation, Einweisung und Softwarepflege gehören nach diesem Vertrag nicht zum Leistungsumfang von HEROLD.'
        },

        /*2*/{header: '\n§ 3 Benutzung', description: ''},

        /*3*/{
            header: '1. Allgemeines',
            description: 'Der Lizenznehmer erwirbt eine Einfachlizenz oder eine Mehrfachlizenz für eine bestimmte Userzahl (gemäß Bestellschein, Vertrag bzw. Produktbeschreibung). Im Falle des Erwerbs einer Mehrfachlizenz („Netzlizenz“) dürfen immer nur höchstens so viele Programminstallationen benützt werden und Zugriffsrechte an der Datenbank vergeben werden, wie Lizenzen (gemäß Bestellschein, Vertrag bzw. Produktbeschreibung) erworben wurden. Der Lizenznehmer hat dafür zu sorgen, dass die Benutzung nur im Rahmen der erworbenen Lizenz erfolgt. Die Lizenz ist zeitlich befristet für die Dauer von einem Jahr ab Rechnungsdatum, danach ist eine Verwendung des Datenträgers sowie der darauf enthaltenen oder über den Datenträger gewonnenen Daten nicht mehr zulässig. Nach Ablauf der Verwendungsfrist darf der Datenträger nicht mehr bzw. nur nach kostenpflichtigem Bezug einer Lizenzerneuerung (Update) verwendet werden.'
        },

        /*4*/{
            header: '2. Rechte des Lizenznehmers',
            description: 'Der Lizenznehmer ist berechtigt, die auf dem Datenträger enthaltene Datenbank und Software zur Durchführung von Abfragen zur eigenen Informationsbeschaffung zu verwenden. Die eigene Informationsbeschaffung besteht darin, bestimmte Privatpersonen und Firmen mit den dazugehörigen Informationen (Adresse, Rufnummer, etc.) aufzufinden. Die Nutzung der auf diese Art abgefragten Datensätze darf ausschließlich für eigene nichtkommerzielle Zwecke erfolgen. Sofern das Produkt das Abspeichern oder den Ausdruck von Datensätzen ermöglicht, darf die am Produkt angegebene Höchstanzahl an Datensätzen nicht überschritten werden, wobei diese Höchstanzahl pro Haushalt bzw. Firmenstandort gilt.'
        },

        /*5*/{
            header: '\n3. Pflichten des Lizenznehmers',
            description: 'Abfragen müssen unter Verwendung der mit dem Datenträger zur Verfügung gestellten Benutzeroberfläche erfolgen, es sei denn eine davon abweichende Nutzungs- und Abfragemöglichkeit wurde schriftlich mit HEROLD vereinbart. Neben den gesetzlichen Verboten ist bei allen HEROLD-Produkten die vollständige oder teilweise Vervielfältigung, Übersetzung sowie die Verwendung im Zusammenhang mit der gewerblichen Adressenverwertung, zum Aufbau oder zur Ergänzung von Teilnehmer-, Firmen- oder anderer Datenbanken jeder Art und in jeder medialen Form (in Printform, elektronisch, auf CD-ROM, etc.), zur Durchführung eines Auskunftsdienstes oder eines Call-Centers (wenn auch nur als Nebenleistung), zur Erteilung von Telefonauskünften, zu Marketing- und Werbezwecken, zum Aufbau von Konkurrenzprodukten zu HEROLD-Produkten (insbesondere HEROLD CD-ROM bzw. DVD, HEROLD Intranet-Versionen, HEROLD Einzeladressenverkauf etc.), die Verwendung zu sonstigen kommerziellen Zwecken sowie generell die Verwendung für Zwecke und im Interesse Dritter verboten. Bei Abschluss eines Abonnements oder der allfälligen Bereitstellung von sonstigen Daten-Updates dürfen Altversionen eines Produktes nicht mehr durch den Lizenznehmer oder einen Dritten verwendet und daher durch den Lizenznehmer auch nicht weitergegeben werden. Der Lizenznehmer hat den Datenträger ausreichend gegen einen unbefugten Zugriff Dritter zu schützen.'
        },

        /*6*/{
            header: '\n4. Für die Marketing CD business gelten die in § 3 enthaltenen Lizenzbedingungen mit folgenden Abweichungen:',
            description: 'Die Marketing CD business dient zur Durchführung von Marketing- und Werbemaßnahmen für Zwecke des vom Lizenznehmer angegebenen Unternehmensstandortes. Der Kauf der Produktvariante CD professional berechtigt zur Benutzung der CD-ROM auf zwei Arbeitsplätzen zu je einem Benutzer. Eine Verwendung für Zwecke Dritter – somit auch für Zwecke verbundener Unternehmen oder für andere Standorte desselben'
        },

        /*7*/{
            header: '',
            description: 'Unternehmens – ist untersagt. Die Verwendung der CD-ROM darf nur an und für einen Unternehmensstandort des Lizenznehmers erfolgen. Auch die Mitbenutzung der CD-ROM sowie daraus erstellter oder ergänzter Datenbanken durch andere Unternehmensstandorte oder durch verbundene Unternehmen ist unzulässig. Die Verwendung an bzw. für mehrere Unternehmensstandorte ist nur durch Erwerb von Filial-Lizenzen (samt der erforderlichen Anzahl von Zugriffsrechten) zulässig. Zulässig ist die Verwendung der auf der „Marketing CD business“ enthaltenen Daten zum Aufbau und zur Ergänzung von Datenbanken des Lizenznehmers, sofern diese Datenbanken ausschließlich zum Zwecke der Durchführung eigener (d.h. nicht im Auftrag oder Interesse Dritter) gesetzlich zulässiger Werbemaßnahmen des angegebenen Unternehmensstandortes innerhalb der vereinbarten Lizenzdauer (siehe § 3.1) verwendet werden. Die Verwendung der Marketing CD business oder der mittels dieses Produktes generierten oder ergänzten Datenbanken zu anderen Zwecken ist verboten. An rechtmäßig erstellten oder ergänzten Datenbanken dürfen höchstens so viele Zugriffsrechte erteilt werden, wie Lizenzen erworben wurden. Bei Durchführung von Werbeaussendungen ist HEROLD Business Data GmbH als Auftraggeber der Ursprungsdatei anzugeben.'
        },

        /*8*/{
            header: '\n§ 4 Rückübersetzung und Programmänderungen',
            description: 'Die Rückübersetzungen des überlassenen Programmcodes in andere Codeformen (Decompilierung) sowie sonstige Arten der Rückerschließung der verschiedenen Herstellungsstufen der Vertragssoftware (Reverse-Engineering) sind ebenso wie Änderungen der Vertragssoftware – beispielsweise durch eine nicht vorgesehene Erweiterung der Recherchemöglichkeiten – und die Ermöglichung eines Zugriffs auf den Inhalt der Datenbank, insbesondere wenn dies durch Umgehung des HEROLD User Interface erfolgt, unzulässig.'
        },

        /*9*/{
            header: '\n§ 5 Verbot der Weitergabe an Dritte',
            description: 'Der Lizenznehmer darf weder den Datenträger oder die auf dem Datenträger enthaltenen Daten und sonstigen Bestandteile, noch die mittels der Marketing CD business rechtmäßig erstellten oder ergänzten Datenbanken an Dritte weitergeben oder Dritten (auch nicht bloß vorübergehend) überlassen. Ausgenommen davon ist die Überlassung der Marketing CD business oder der darin enthaltenen Daten zum Zweck der Durchführung von Werbemaßnahmen für das Unternehmen des Lizenznehmers. Der Lizenznehmer hat dabei sicherzustellen, dass die zu diesem Zweck überlassenen Datenträger bzw. die überlassenen Daten nach Beendigung der Werbemaßnahme an den Lizenznehmer zurückgegeben bzw. gelöscht werden.'
        },

        /*10*/{
            header: '\n§ 6 Gewerblicher Rechtsschutz',
            description: 'Der Lizenznehmer anerkennt, dass die Vertragssoftware und die Datenbank sowie die in der Datenbank befindlichen Inhalte in all ihren Teilen urheberrechtlich geschützt sind, '
        },

        /*11*/{
            header: '',
            description: 'und dass alle Urheberrechte, Leistungsschutzrechte und sonstigen gesetzlich geschützten Rechte daran HEROLD oder deren Lizenzgebern zukommen. Die am Datenträger oder auf der Dokumentation befindlichen Urheberrechtsvermerke dürfen vom Lizenznehmer nicht entfernt werden.'
        },

        /*12*/{
            header: '\n§ 7 Obhutspflicht',
            description: 'Der Lizenznehmer hat die Vertragssoftware gegen missbräuchliche Nutzung zu sichern.'
        },

        /*13*/{
            header: '\n§ 8 Gewährleistung, Untersuchungs- und Rügepflicht',
            description: 'HEROLD gewährleistet für die Dauer von 6 Monaten, dass bei Vorliegen der Systemvoraussetzungen die fachgerecht installierte Vertragssoftware die Programminstruktionen ausführt. Die Systemvoraussetzungen ergeben sich aus gesonderter Vereinbarung, subsidiär aus der Verpackung. Insbesondere ist bei allen Produkten, für die Updates erforderlich sind, die Kompatibilität des Endgerätes zu beachten. Details zur Kompatibilität sind der Produktbeschreibung (siehe Verpackung bzw. Produktbeschriftung) zu entnehmen oder auf www.herold.at bzw. beim HEROLD Kundenservice unter der Nummer 02236/401-38133 erhältlich. HEROLD leistet keine Gewähr dafür, dass die Vertragssoftware mit sämtlichen Aktualisierungen oder neuen Versionen einzelner Systemkomponenten kompatibel ist. HEROLD ist berechtigt, den mangelhaften Datenträger durch einen mängelfreien Datenträger auszutauschen. Der Lizenznehmer wird den Datenträger unverzüglich nach Erhalt untersuchen, insbesondere im Hinblick auf die Funktionsfähigkeit grundlegender Programmfunktionen. Mängel, die hierbei festgestellt werden oder feststellbar sind, müssen HEROLD unverzüglich mittels eingeschriebenen Briefes (samt detaillierter Beschreibung des Mangels) angezeigt werden. Mängel, die im Rahmen der beschriebenen ordnungsgemäßen Untersuchung nicht feststellbar sind, müssen unverzüglich nach Entdeckung unter Einhaltung der dargelegten Rügeanforderungen gerügt werden. Dem Lizenznehmer obliegt der Beweis, dass der Mangel bereits bei Übergabe vorhanden war. Bei einer Verletzung der Untersuchungs- und Rügepflicht gilt die Vertragssoftware in Ansehung des betreffenden Mangels als genehmigt. Die Frist zur Geltendmachung von Gewährleistungsbehelfen, die einvernehmlich auf Austausch des mangelhaften Produktes gegen ein mangelfreies Produkt beschränkt werden, beträgt sechs Monate ab Bereitstellung des Datenträgers. Handelt es sich beim Lizenznehmer um einen Konsumenten im Sinne des Konsumentenschutzgesetzes, so sind die Gewährleistungsansprüche in Abänderung der vorstehenden Punkte nur insoweit beschränkt, als HEROLD innerhalb der gesetzlichen Gewährleistungsfrist statt einer vom Konsumenten geforderten Vertragsaufhebung oder Preisminderung berechtigt ist, innerhalb angemessener Frist ab Geltendmachung dieser Ansprüche die mangelhafte Sache'
        },

        /*12*/{
            header: '',
            description: 'durch eine mangelfreie Sache auszutauschen. Einige HEROLD Produkte sind nach Ablauf einer bestimmten, in der Produktbeschreibung angegebenen maximalen Verwendungsdauer, nicht mehr oder nur nach Bezug kostenpflichtiger Updates benutzbar. Details zur maximalen Verwendungsdauer und zu den kostenpflichtigen Updates sind der Produktbeschreibung (siehe Verpackung bzw. Produktbeschriftung) zu entnehmen oder auf www.herold.at bzw. beim HEROLD Kundenservice unter der Nummer 02236/401-38133 erhältlich. Nach Überschreitung der maximalen Verwendungsdauer kann die Funktionalität des Produktes nicht gewährleistet werden.'
        },

        /*13*/{
            header: '\n§ 9 Haftung',
            description: 'HEROLD haftet nicht – es sei denn HEROLD trifft ein vorsätzliches oder grob fahrlässiges Verschulden – für die Vollständigkeit und Richtigkeit der Daten sowie für allfällige Schäden aufgrund einer solchen Unvollständigkeit oder Unrichtigkeit. Darüber hinaus haftet HEROLD nicht für das fehlerfreie Funktionieren der Software sowie für Schäden, welche am Computer oder an sonstigen technischen Geräten des Lizenznehmers auftreten. Die HEROLD Telefonbuch-Datenbank enthält Daten aus der Teilnehmerdatenbank der A1 Telekom Austria AG. HEROLD haftet nicht für die Vollständigkeit und Richtigkeit der Einträge. Für eine bestmögliche Aktualität der durch den Lizenznehmer verwendeten Datenbank ist der Bezug der jeweils neuesten Produktversion erforderlich. Beim Bezug von Updates durch Übertragungseinrichtungen haftet HEROLD nicht für deren Verfügbarkeit. HEROLD haftet für Schäden aufgrund einer Vertragsverletzung nur bei Vorsatz und grober Fahrlässigkeit. Sofern Schäden nicht binnen einer Frist von sechs Monaten ab Schadenseintritt geltend gemacht werden, gelten diese als verjährt.'
        },

        /*14*/{
            header: '\n§ 10 Abonnement',
            description: 'Für den Fall des Erwerbs eines Abonnements gilt für den Lizenznehmer eine zweijährige Mindestlaufzeit. Der Vertrag kann in diesem Fall jährlich mit Ablauf eines jeden Vertragsjahres - erstmals jedoch mit Ablauf des zweiten Vertragsjahres - unter Einhaltung einer dreimonatigen Kündigungsfrist durch eingeschriebenen Brief gekündigt werden. Ist der Lizenznehmer Konsument im Sinne des Konsumentenschutzgesetzes, so kann der Lizenznehmer den Vertrag halbjährlich - erstmals jedoch mit Ablauf des ersten Vertragsjahres - unter Einhaltung einer zweimonatigen Kündigungsfrist schriftlich kündigen. HEROLD ist ohne Berücksichtigung der Mindestlaufzeit zur Kündigung des Abonnements unter Einhaltung einer dreimonatigen Kündigungsfrist berechtigt. Der Abonnementpreis wird entsprechend der Entwicklung des von der Statistik Österreich verlautbarten Verbraucherindex 2010 (VPI 2010) oder des an seine Stelle tretenden Index jährlich erhöht, wobei der durchschnittliche Indexwert des Kalenderjahres in dem der Auftrag'
        },

        /*15*/{
            header: '',
            description: 'erteilt wurde als Basiswert heranzuziehen ist. Darüber hinaus sind Preiserhöhungen generell bei Erhöhungen der Selbstkosten (z.B. Ansteigen der Materialkosten, Lohnkosten, etc.) auch während der Laufzeit des Abonnements möglich. Falls HEROLD die Lizenzbedingungen während der Laufzeit des Abonnements zum wesentlichen Nachteil des Lizenznehmers abändert (eine Peisanpassung wie oben beschrieben stellt keine solche nachteilige Änderung dar) ist der Lizenznehmer zur Auflösung des Abonnements berechtigt, sofern er den zuletzt zugesandten Datenträger noch nicht durch Öffnen der Verpackung entsiegelt hat und den noch versiegelten Datenträger samt zugehöriger Verpackung und Dokumentation binnen 14 Tagen nach Erhalt an HEROLD auf eigene Kosten eingeschrieben zurücksendet. Die Gefahr des Verlustes trägt der Besteller. Nach Erhalt der jeweils neuesten Ausgabe der HEROLD Datenträger ist die alte Version innerhalb von 14 Tagen nach Aufforderung durch HEROLD eingeschrieben zurückzusenden. Wird der Aufforderung nicht Folge geleistet, ist HEROLD berechtigt, eine Version zusätzlich in Rechnung zu stellen. Die alte Version darf nicht verwendet werden. Der Datenträger darf nur während der Laufzeit des Abonnements verwendet werden, eine Verwendung nach Ablauf der Vertragsdauer ist unzulässig.'
        },

        /*16*/{
            header: '\n§ 11 Zahlungskonditionen, Preise, Lieferkosten',
            description: 'Alle Rechnungen sind binnen 14 Tagen ab Rechnungsdatum zur Zahlung fällig. Bei Verzug werden Mahnspesen, Inkassokosten (zB durch Inkassobüro) und Verzugszinsen in der Höhe von 12 % p.a. verrechnet. Gegenüber Konsumenten werden neben Mahnspesen und Inkassokosten die gesetzlichen Verzugszinsen verrechnet. Bei Ermächtigung zum Einzug durch Lastschriften gewährt HEROLD 3 % Skonto. Für Updates, welche online über den HEROLD Webserver bezogen werden, gelten die auf der Website abrufbaren Zahlungsbedingungen. Bei Angaben in Preislisten, Anzeigen, Werbeunterlagen, Internet-Seiten und dergleichen sind jederzeitige Änderungen ausdrücklich vorbehalten. Bestellungen, die durch unmittelbare Lieferung ohne vorangehende Auftragsbestätigung angenommen werden, werden zu den am Bestelltag geltenden Listenpreisen ausgeführt. Sollten im Zuge des Versandes Export- oder Importabgaben fällig werden, gehen auch diese zu Lasten des Bestellers. Nähere Auskünfte unter der Telefonnummer 02236/401-38133. Bei Nachforschungen über die Zustellung des Datenträgers an den Besteller, wird dem Besteller ein angemessener Unkostenbeitrag, jedoch mindestens EUR 15,- verrechnet, wenn sich herausstellt, dass der Besteller den Datenträger ordnungsgemäß übernommen hat. Gegen Forderungen von HEROLD kann nicht aufgerechnet werden. Sofern die Bezahlung der Rechnungssumme in Teilbeträgen vereinbart ist, werden bei nicht fristgerechter Bezahlung auch nur eines Teilbetrages, bzw. im Falle der Einzugsermächtigung bei nicht ausreichender Konto'
        },

        /*17*/{
            header: '',
            description: 'abdeckung, sämtliche ausständigen Teilleistungen ohne weitere Nachfristsetzung fällig. Dem Preis wird eine Servicepauschale für mobile Datenaufbereitung aufgeschlagen. Die Höhe der Servicepauschale ist umsatzabhängig und wird sowohl bei der erstmaligen Auftragserteilung, als auch bei Folgeaufträgen verrechnet.'
        },

        /*18*/{
            header: '\n§ 12 Zahlung, Eigentumsvorbehalt, Erlöschen des Nutzungsrechtes',
            description: 'Die Ware bleibt bis zur vollständigen Bezahlung Eigentum von HEROLD. Bei Zahlungsverzug oder Verletzung der Nutzungsbestimmungen erlischt das Recht zur Nutzung der Vertragssoftware und der Datenbank.'
        },

        /*19*/{
            header: '\n§ 13 Vertragsabschluss mit Verbrauchern Rücktritts- und Widerrufsbelehrung',
            description: 'Sofern der Kunde Konsument im Sinne des Konsumentenschutzgesetzes ist und der Vertrag außerhalb der Geschäftsräumlichkeiten von HEROLD abgeschlossen wurde, ist er berechtigt, binnen 14 Tagen nach Unterzeichnung des Bestellscheins vom Vertrag, dies ohne Angabe von Gründen, zurücktreten. Die Rücktrittserklärung hat schriftlich an HEROLD Business Data GmbH, Guntramsdorfer Straße 105, 2340 Mödling oder per E-Mail an kundenservice@herold.at zu erfolgen. Bei Abschluss eines Vertrages im Fernabsatz (dh unter Verwendung von Fernkommunikationsmittel, wie zB Internet, Telefon, Fax) ist der Konsument berechtigt, binnen 14 Tagen ab Vertragsabschluss ohne Angaben von Gründen den Vertrag zu widerrufen. Um das Widerrufsrecht auszuüben, muss der Konsument HEROLD mittels einer eindeutigen Erklärung (z.B. ein mit der Post versandter Brief an HEROLD Business Data GmbH, Guntramsdorfer Straße 105, 2340 Mödling, ein Telefax an +43 (0) 2236 / 401-8 oder ein E-Mail an kundenservice@herold.at über den Entschluss, den Vertrag zu widerrufen, informieren. Zur Wahrung der Widerrufsfrist reicht es aus, dass die Mitteilung über die Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist abgesendet wird. Wenn der Konsument den Vertrag widerruft, hat HEROLD alle Zahlungen, die HEROLD vom Konsumenten erhalten hat, unverzüglich und spätestens binnen vierzehn Tagen ab dem Tag zurückzuzahlen, an dem die Mitteilung mit dem Widerruf des Vertrags bei HEROLD eingegangen ist. Für die Rückzahlung wird dasselbe Zahlungsmittel verwendet, das bei der ursprünglichen Transaktion eingesetzt wurde. In keinem Fall wird wegen der Rückzahlung ein Entgelt berechnet.'
        },

        /*20*/{
            header: '\n§ 14 Datenschutz und unerbetene kommerzielle Kommunikation',
            description: 'Alle Kunden sind verpflichtet, das Datenschutzgesetz und das Telekommunikationsgesetz zu beachten und HEROLD diesbezüglich schad- und klaglos zu halten. Bei auf den Datenträgern'
        },

        /*21*/{
            header: '',
            description: 'enthaltenen elektronischen Postadressen (E-Mail-Adressen), Telefon- und Faxnummern darf nicht auf eine Zustimmung des Anschlussinhabers zum Erhalt elektronischer Post, Anrufen und Faxnachrichten geschlossen werden. Insbesondere ist die bei der Rundfunk und Telekom Regulierungs-GmbH geführte Robinson-Liste zu beachten. Sofern Angebotsvorlagen für Brief, Fax oder E-Mail zur Verfügung gestellt werden, dürfen diese nicht zu Spamming-Zwecken verwendet werden. Bei Verwendung der Datenbank im Rahmen einer Datenanwendung im Sinne des Datenschutzgesetzes ist der Kunde selbst für die Rechtsmäßigkeit der Datenanwendung verantwortlich.'
        },

        /*22*/{
            header: '\n§ 15 Zustimmungserklärung',
            description: 'Mit Auftragserteilung erklärt sich der Besteller gemäß § 8 Abs. 1 Ziffer 2 DSG 2000 einverstanden, dass die am Bestellschein angegebenen Kundendaten erfasst und für Werbe- und Marketingzwecke von HEROLD verwendet werden. Der Besteller erklärt sich mit Angabe seiner Telefonnummer und seiner elektronischen Postadresse ausdrücklich einverstanden, von HEROLD Telefonanrufe und elektronische Post zu Werbe- und Marketingzwecken, insbesondere zu Zwecken der Zusendung von Angeboten und Newsletter mit werblichen Informationen über HEROLD und über Kunden von HEROLD, zu erhalten. Diese Zustimmungserklärungen können jederzeit durch E-Mail an (kundenservice@herold.at) widerrufen werden und sind mangels Widerruf auch über das Vertragsverhältnis hinaus wirksam.'
        },

        /*23*/{
            header: '\n§ 16 Sonstiges',
            description: 'Es gilt österreichisches Recht. Verstöße gegen diese Allgemeinen Geschäftsbedingungen werden unter Ausschöpfung des Rechtsweges verfolgt. HEROLD ist berechtigt, diese Allgemeinen Geschäftsbedingungen jederzeit zu ändern. Die Allgemeinen Geschäftsbedingungen sind in der jeweils gültigen Fassung zum Zeitpunkt der Auftragserteilung (bei einem Abonnement zum Zeitpunkt der Lieferung) anwendbar. Wir empfehlen daher, die dem Datenträger beiliegenden Allgemeinen Geschäftsbedingungen vor jedem Vertragsabschluss oder bei jeder Lieferung des Datenträgers im Abonnement erneut zu lesen. Die allfällige Unwirksamkeit einzelner Bestimmungen dieser Allgemeinen Geschäftsbedingungen lässt die Geltung der übrigen Bestimmungen dieser Allgemeinen Geschäftsbedingungen unberührt. An die Stelle der unwirksamen Bestimmung tritt eine solche wirksame Bestimmung, die ersterer nach deren Sinn und Zweck rechtlich und wirtschaftlich am nächsten kommt. Ausschließlicher Gerichtsstand ist das sachlich zuständige Gericht für den ersten Wiener Gemeindebezirk. Für Klagen gegen Verbraucher gilt § 14 KSchG.'
        },

    ];

    pdfMake.fonts = {
        Helvetica: {
            normal: 'HelveticaNeueLT-LightCond.ttf',
            bold: 'HelveticaNeue-BoldCond.ttf',
        }
    };

    function renderTermsAndConditions(termsAndCons) {
        var content = [];

        content.push(
            {
                columns: [
                    [
                        {text: termsAndCons[0].header, style: 'terms_header'},
                        {text: termsAndCons[0].description, style: 'terms'},
                        {text: termsAndCons[1].header, style: 'terms_header'},
                        {text: termsAndCons[1].description, style: 'terms'},
                        {text: termsAndCons[2].header, style: 'terms_header'},
                        {text: termsAndCons[2].description, style: 'terms'},
                        {text: termsAndCons[3].header, style: 'terms_header'},
                        {text: termsAndCons[3].description, style: 'terms'},
                    ],
                    [
                        {text: termsAndCons[4].header, style: 'terms_header'},
                        {text: termsAndCons[4].description, style: 'terms'},
                        {text: termsAndCons[5].header, style: 'terms_header'},
                        {text: termsAndCons[5].description, style: 'terms'},
                        {text: termsAndCons[6].header, style: 'terms_header'},
                        {text: termsAndCons[6].description, style: 'terms'},
                    ]
                ]
            }
        );


        content.push({
            columns: [
                {
                    pageBreak: 'after',
                    text: ''
                }
            ]
        });


        content.push(
            {
                columns: [
                    [
                        {text: termsAndCons[7].header, style: 'terms_header'},
                        {text: termsAndCons[7].description, style: 'terms'},
                        {text: termsAndCons[8].header, style: 'terms_header'},
                        {text: termsAndCons[8].description, style: 'terms'},
                        {text: termsAndCons[9].header, style: 'terms_header'},
                        {text: termsAndCons[9].description, style: 'terms'},
                        {text: termsAndCons[10].header, style: 'terms_header'},
                        {text: termsAndCons[10].description, style: 'terms'},
                    ],
                    [
                        {text: termsAndCons[11].header, style: 'terms_header'},
                        {text: termsAndCons[11].description, style: 'terms'},
                        {text: termsAndCons[12].header, style: 'terms_header'},
                        {text: termsAndCons[12].description, style: 'terms'},
                        {text: termsAndCons[13].header, style: 'terms_header'},
                        {text: termsAndCons[13].description, style: 'terms'},
                    ]
                ]
            }
        );

        content.push({
            columns: [
                {
                    pageBreak: 'after',
                    text: ''
                }
            ]
        });


        content.push(
            {
                columns: [
                    [
                        {text: termsAndCons[14].header, style: 'terms_header'},
                        {text: termsAndCons[14].description, style: 'terms'},
                        {text: termsAndCons[15].header, style: 'terms_header'},
                        {text: termsAndCons[15].description, style: 'terms'},
                        {text: termsAndCons[16].header, style: 'terms_header'},
                        {text: termsAndCons[16].description, style: 'terms'},
                    ],
                    [
                        {text: termsAndCons[17].header, style: 'terms_header'},
                        {text: termsAndCons[17].description, style: 'terms'},
                        {text: termsAndCons[18].header, style: 'terms_header'},
                        {text: termsAndCons[18].description, style: 'terms'},
                    ]
                ]
            }
        );


        content.push({
            columns: [
                {
                    pageBreak: 'after',
                    text: ''
                }
            ]
        });


        content.push(
            {
                columns: [
                    [
                        {text: termsAndCons[19].header, style: 'terms_header'},
                        {text: termsAndCons[19].description, style: 'terms'},
                        {text: termsAndCons[20].header, style: 'terms_header'},
                        {text: termsAndCons[20].description, style: 'terms'},
                        {text: termsAndCons[21].header, style: 'terms_header'},
                        {text: termsAndCons[21].description, style: 'terms'},
                        {text: termsAndCons[22].header, style: 'terms_header'},
                        {text: termsAndCons[22].description, style: 'terms'},
                    ],
                    [
                        {text: termsAndCons[23].header, style: 'terms_header'},
                        {text: termsAndCons[23].description, style: 'terms'},
                        {text: termsAndCons[24].header, style: 'terms_header'},
                        {text: termsAndCons[24].description, style: 'terms_bold'},
                        {text: termsAndCons[25].header, style: 'terms_header'},
                        {text: termsAndCons[25].description, style: 'terms'},
                    ]
                ]
            }
        );


        return content;
    }


    var dd = {

        content: [
            renderTermsAndConditions(termsAndConditions),
        ],

        images: {
            mySuperImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAABHCAIAAAAC1/wuAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATuSURBVHhe7dzhSxtnHMDx/SeKdrKVjdp1shdzloFvOljpCnaMUcYY1b2Y615MKXRv2u1daRxFYQzK+iYtcxskL3xhXixQ3FihJ1MaGQqxxc5YZdEZNI7Eyxp9EpPncpe7KPGX8/vj86q5u1zKfTWJT/JCa1MzALFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRjk6iZ4aMVK5sFsKXTlo20zju1R4wMupfa5iMEThley/uxkyvJeLT0R8Dvd1t6lDl2nsGh++NTzyaX3y2lt5WeznNdnr12WL80W/j90YGegqnV1Tb/2GJS6GE2tHjZFIrib/nY7+PB2/0nT6uH9bXSFTbTCM70eJknkavv1deaWtH721jeUttUMtsLT+83fdaa8kxDy/RkjHTjyP6g/UzEtU20zRIos8v3H8igy/vHi2v7d1bxnpW3Vb7ZNcf3jrbUjysiETzk4mH+7v0g/sUiWqbaRom0VxuJXq1eNV2DkaWTPXv+xtzKTLQWTismERz5qbxbbd2cJ8iUW0zjcdEV41gIDDkzs0vz79key+biZlJw3amZxMpyw+Hrfngx+rEjl2JJCv+Cs2/1ExUHrsXq9lk5Ir9ee43UXM17vA4d2bmcTJd4cfNxsT1VyzH9yMS1TbTeEw0Eerd29e9Gs6t64uxhfILN5MI9atb3xye1gs1M4v3R+zeVdrV0t03fH8xo+eQnRruUtscfKIlTyUcdPYFZ9Jqj8Js/3Xn7JF434hEtc00YhNtbvtsbFltvDsliVqfT5pPwn0dxX3tdXwaeqI3uvegDivRis8LPN51wyJRbTON3EQtl7tjosnIwLHy3W28ejX6r9qnMBISbTo3MrWh9lFDon5jvbxqG8dE83++czdTP1x0Orfq15/lt+jG9PA5das1Ufc/O5z2PcRE933XDYtEvY5jou6nrJkarj/La1Ez/tNHhV1I1EdI1OvUJ1Hnd3Qnp+eWy9/lLP8DJon6CIl6nfok6n6208m48fPXF9pLlgGRqI+QqNdxTNR9CWX2l+jK7B+j1RJdHvt8b5GQE8tLXCGJXvh+ZlPto4ZE/aa2y8txLxGJ7k42Gb32RvGA1kS354Lvnyi5RzsnPgjO6SsYJCT6+jcT69p5kajf+DvRXO6/P0e6X1QHrLB0IZuK3b1c5TMix9/uvxtLWfY87KULbW99MhR9qr+kYOmC79Ql0dRsNBQKu/RLoFctfPd4bi2nL14Lz+qr4krW6NotADTTa0vqjz7WWVqttM6uygLATGrF/oh7M3nnw53f4ZZEzfRqlf1tTosFgP5Tl0Q9TcYYandxL5V1fRVdUduqSRmBM4Vb67aM3uUUHo71GXiNwzJ6H/JZoid7QwtqWzWliTa39Xw3ZXnK6n2qfhjN5Rx0onwYzY+OVqL1+ki3yznIRPlIt18dtUR35L8YZTT6IObxi1FiD34ddffFKC5n34nmX0XvfjHKzcvvnCo/K587OokCDYlEAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQBwZqa/wd0Ed3Ls5EBJgAAAABJRU5ErkJggg=='
        },

        header: function () {

            return {
                table: {
                    widths: [130, '*'],
                    body: [
                        [{
                            image: 'mySuperImage',
                            width: 130,
                            height: 40,
                            margin: [-5, 0, 0, -2],
                            border: [false, false, false, true]
                        },
                            {
                                text: 'Allgemeine Geschäftsbedingungen\n für CD-ROMs, DVDs und andere Datenträger',
                                fontSize: 9,
                                alignment: 'right',
                                bold: false,
                                margin: [0, 10, 0, 0],
                                border: [false, false, false, true]
                            }],
                    ]
                }, margin: [40, 40]
            };
        },


        footer: function () {
            return {
                table: {
                    widths: ['*', '*'],
                    body: [
                        [{text: '', border: [false, false, false, false]},
                            {
                                text: '',
                                fontSize: 6,
                                alignment: 'right',
                                bold: false,
                                border: [false, false, false, false],
                                margin: [0, 0, 0, 0]
                            }],
                        [{
                            colSpan: 2,
                            text: 'AGB CD-ROMs, DVDs und andere Datenträger_V13 (20150909)\n + HEROLD Business Data GmbH, A-2340 Mödling, Guntramsdorfer Straße 105, Telefon: +43 (0) 2236/401-0, Fax +43 (0) 2236/401-8, E-Mail:\n  kundenservice@herold.at, www.herold.at, Gerichtsstand Wien, UID: ATU57270679, DVR: 0871885, FN: 233171z, FB-Gericht : Landesgericht Wr. Neustadt',
                            fontSize: 4.5,
                            alignment: 'left',
                            margin: [0, 0, 0, 0],
                            border: [false, false, false, false]
                        }, {}]
                    ]
                }, margin: [40, 0]
            };

        },

        // Styles
        styles: {
            terms_header: {
                bold: true,
                fontSize: 7.1,
                alignment: 'justify',
                lineHeight: 1.3
            },
            terms: {
                fontSize: 7,
                alignment: 'justify'
            },
            terms_bold: {
                bold: true,
                fontSize: 7,
                alignment: 'justify',
                lineHeight: 1.2
            },

        },

        // Default Styles
        defaultStyle: {
            columnGap: 8,
            font: 'Helvetica',

        },
        pageSize: {height: 605, width: 405},

        // Page margins
        pageMargins: [40, 100, 40, 70]
    };
    return dd;
}
