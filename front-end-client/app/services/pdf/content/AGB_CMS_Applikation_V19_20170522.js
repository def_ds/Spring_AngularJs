export function start() {
    var termsAndConditions = [

        /*0*/{header: '§ 1 Allgemeines und Auftragsannahme', description: 'Diese AGB gelten für folgende Services und Dienstleistungen: die Bereitstellung des Zugangs zur CMS-Applikation, zur Bereitstellung von Websites und Online Shops, das Hosting, die Bereitstellung von Domains, die Bereitstellung von Facebook- Fanpages, die Bereitstellung von Besucher- und Begleiterpaketen, von E-Mail-Services sowie für Dienstleistungen im Zusammenhang mit Websites (zB Ranking-Boost & Analyse-Pakete). AGB und sonstige Bedingungen des Kunden sind nicht anwendbar. Unterzeichnete Bestellscheine gelten als verbindliches Angebot des Kunden, das von HEROLD innerhalb von vier Wochen ab Unterzeichnung abgelehnt werden kann. Der Kunde garantiert, dass alle im Zuge der Beauftragung erteilten Informationen vollständig und richtig sind und, dass dieser berechtigt ist, gegenständlichen Auftrag zu erteilen.'},

        /*1*/{header: '§ 2 Nutzung der CMS-Applikation, Urheberrechte und sonstige Vertragspflichten', description: 'Bei der CMS-Applikation handelt es sich um urheberrechtlich geschützte Software. Sämtliche Urheber- und sonstige Immaterialgüterrechte an der CMS-Applikation und an den darüber generierten Websites und Facebook-Fanpages, dies hinsichtlich Gestaltung, Aufbau, Design, Quellcode und von HEROLD bereitgestellten Inhalten, stehen ausschließlich HEROLD und/oder seinen Technologiepartnern zu. Der Kunde erwirbt ein einfaches, nicht ausschließliches und für die Dauer der Vereinbarung zeitlich beschränktes Nutzungsrecht an der CMS-Applikation, jedoch kein Recht zur Vervielfältigung,'
        +' Bereitstellung, Zurverfügungstellung oder Bearbeitung, sowie das Recht zur Bereitstellung der über die CMS-Applikation erstellten Website für die Dauer der Vereinbarung. Das Nutzungsrecht an der CMS-Applikation ist nicht auf Dritte übertragbar und es dürfen keine Unterlizenzen erteilt werden. Das „Reverse Engineering“, oder sonstige Verfahren zur Feststellung des Source Codes der CMS-Applikation sind ausdrücklich untersagt. Das Hosting der über die CMS-Applikation generierten Websites erfolgt durch HEROLD. Die Nutzung der von HEROLD bereitgestellten Inhalte (Fotos, Texte und Grafiken) darf ausschließlich zur Bereitstellung auf diesen Websites bzw. Facebook-Fanpages und unter Einhaltung der in diesen AGB festgesetzten Pflichten erfolgen. Der Kunde hat dafür zu sorgen, dass seine Zugangsdaten sorgfältig verwahrt werden, damit Dritten kein unberechtigter Zugriff auf die CMS-Applikation ermöglicht wird. Der Kunde darf keinerlei Veränderungen an der CMS-Applikation vornehmen. Ebenso ist es dem Kunden untersagt, Änderungen an den über die CMS-Applikation generierten Websites vorzunehmen, sofern diese Änderungen nicht ausdrücklich über bestehende Funktionen der CMS-Applikation erlaubt sind. Nach Auftragserteilung wird für den Kunden eine Website erstellt. Inhalte der Website sind vom Kunden bereitzustellen und in der Folge – sofern nichts Abweichendes vereinbart ist – vom Kunden zu warten. Sollte der Kunde trotz Aufforderung keine Inhalte bereitstellen, können branchentypische Texte und Bilder, einschließlich eigener Inhalte, wie'
        +' Werbung für HEROLD oder Dritte, in die Website integriert werden. Der Kunde wird von der Fertigstellung der Website und den über Zeitpunkt der Online-Stellung (in Form von Mustertexten) informiert. Sofern keine Änderungswünsche fristgerecht bekannt gegeben werden, ist HEROLD berechtigt, die Website auf der vereinbarten Domain und/oder auf einer Subdomain von herold.at online zu stellen. Die diesbezügliche Korrespondenz mit dem Kunden erfolgt per E-Mail. Das Service-Paket, welches der Kunde mit bestimmten Website-Paketen nutzen kann und das sich auf vom Kunden beauftragte Änderungen an dessen Website bezieht, unterliegt der Fair-Use-Klausel, wonach diese Änderungen nur in adäquatem Ausmaß von HEROLD umgesetzt werden müssen. HEROLD behält sich vor, Änderungen die eine übliche Nutzung des Services überschreiten, dem Kunden in Rechnung zu stellen. Der Kunde ist für die rechtliche Zulässigkeit aller von ihm an HEROLD zum Zweck der Website-Erstellung sowie für die mittels der CMS-Applikation auf der Website bereitgestellten Inhalte (insbesondere Texte, Grafiken, Bilder, Fotos und Sounddateien) selbst verantwortlich. Er ist weiters verantwortlich, dass weder er noch Nutzer der Website auf dieser keine Inhalte bereitstellen, die Viren, Trojaner oder sonstige Programme enthalten, die schädigend wirken können, oder, die rechtswidrig, beleidigend, bedrohlich, vulgär, rassistisch oder Straftaten verharmlosend oder begünstigend sind oder Anlass zur Zivilklagen geben oder HEROLD oder Technologiepartner von HEROLD in Misskredit bringen. Im Falle einer Datenermittlung durch den Kunden über die Website ist der Kunde für den rechtmäßigen Umgang mit Daten selbst verantwortlich. Eine Überwachung oder Überprüfung der Inhalte durch HEROLD findet nicht statt. HEROLD behält sich vor, bei'
        +' Verstößen gegen diese AGB sowie im Fall, dass Websites von Kunden inhaltlich bedenklich erscheinen oder die berechtigten Interessen Dritter verletzen diese von einer Speicherung auf dem Server auszunehmen und zu löschen bzw. zu sperren. Der Kunde stellt HEROLD und den für das Hosting verantwortlichen Vertragspartner von HEROLD von jeglicher Haftung im Zusammenhang mit den vom Kunden bereitgestellten Inhalten frei. HEROLD ist berechtigt, bei Verdacht des Missbrauchs von Zugangsdaten oder einer sonstigen vereinbarungswidrigen Verwendung der CMS-Applikation den Zugang unverzüglich und ohne vorherige Mitteilung zu sperren. Der Entgeltanspruch von HEROLD besteht fort, solange eine Löschung oder Sperrung des Zuganges zur CMS-Applikation oder der über diese generierten Website aus vom Kunden verursachten Gründen vorgenommen wurde. Die vom Kunden auf seiner Website bereitgestellten Inhalte dürfen den üblichen Umfang von Websites für Klein- und Mittelbetriebe nicht übersteigen, anderenfalls ist HEROLD zur Sperre und/oder Löschung aller oder einzelner Inhalte ohne vorangehender Mitteilung berechtigt. Die Website enthält ein Muster-Impressum, welches vom Kunden gemäß den geltenden gesetzlichen Bestimmungen eigenständig anzupassen bzw. abzuändern ist. Für die Gestaltung des Impressums ist HEROLD nicht verantwortlich. Der Kunde hat weiters selbst für gültige Nutzungsbedingungen und Datenschutzbestimmungen für seine Website zu sorgen.'},

        /*2*/{header: '§ 3 Leistungen zur Domain-Registrierung', description: 'Der Kunde ist verpflichtet, die von der jeweiligen Domain-Vergabestelle vorgeschriebenen Bedingungen zur Nutzung der Domain zu beachten. Eine Garantie für die Zuteilung einer bestellten Domain kann daher von HEROLD nicht übernommen werden. Der Kunde ist verpflichtet, an allen Handlungen, die für die Registrierung, Übertragung oder Löschung der bestellten Domain erforderlich sind, vollumfänglich mitzuwirken. Der Kunde ist für die rechtliche Zulässigkeit der über die bestellte'
        +' Domain bereitgestellten Inhalte (einschließlich Verlinkungen) selbst verantwortlich. Der Kunde verpflichtet sich weiters, keine pornografischen, gewaltverherrlichenden oder volksverhetzenden Leistungen und/oder Inhalte darzustellen, nicht zu Straftaten aufzurufen und/oder anzuleiten. Eine Überwachung oder Überprüfung der Inhalte durch HEROLD findet nicht statt. HEROLD behält sich das Recht vor, die Domain bei Verdacht auf Rechtswidrigkeit vorübergehend oder dauerhaft zu sperren, jedenfalls aber so lange, wie die Rechtsverletzung bzw. der Verdacht einer Rechtsverletzung besteht. Alternativ besteht für HEROLD die Möglichkeit, den Vertrag außerordentlich, d.h. fristlos, zu kündigen. Der Entgeltanspruch von HEROLD besteht fort, solange eine Sperrung aus vom Kunden verursachten Gründen vorgenommen wurde. Mit Vertragsbeendigung erfolgt eine unwiderrufliche Rückgabe der Domain an HEROLD.'},

        /*3*/{header: '§ 4 Leistungen zur Bereitstellung eines E-Mail-Postfaches', description: 'Es gelten die im Angebot bzw. am Bestellschein angegebenen Speicherkapazitäten sowie technischen Daten. Eine Bereitstellung des E-Mail-Postfaches ist nur für die Dauer der Vereinbarung zur Bereitstellung einer Domain möglich. Im Fall der Beendigung der Vereinbarung zur Bereitstellung einer Domain endet auch die Bereitstellung des E-Mail-Postfaches automatisch; eine gesonderte Kündigung ist hierfür nicht erforderlich. Der Kunde ist für die rechtliche Zulässigkeit der verwendeten E-Mail-Adressen sowie der darüber versendeten Inhalte selbst verantwortlich. Es sind bei dem Versand von EMails'
        +' insbesondere auch die telekommunikationsrechtlichen Vorschriften einzuhalten. Dem Kunden ist bekannt, dass der Versand von Spam-E-Mails sowie generell der Versand von E-Mails zu Werbezwecken unzulässig ist. HEROLD ist generell bei Verletzung sowie beim Verdacht einer Verletzung von gesetzlichen Vorschriften bei der Verwendung des E-Mail-Postfaches zur Sperre des E-Mail-Postfaches berechtigt. HEROLD übernimmt keine Gewähr für die Weiterleitung von E-Mails, SMS oder sonstigen Nachrichten an den Empfänger, ebenso wenig für die richtige Wiedergabe der Nachrichten des Kunden. Der Kunde hat in regelmäßigen Abständen (mindestens einmal täglich) für eine aktuelle Sicherung seiner Daten Sorge zu tragen. HEROLD kann für eine Beschädigung von Daten und/oder Soft- und Hardware aufgrund von Viren in keinem Fall verantwortlich gemacht werden. Der Kunde verpflichtet sich, die von HEROLD zugeteilten Passwörter streng geheim zu halten und HEROLD unverzüglich zu informieren, sobald er davon Kenntnis erlangt, dass unbefugten Dritten das Passwort bekannt ist. Sollten infolge Verschuldens des Kunden Dritte durch Gebrauch der Passwörter Leistungen von HEROLD nutzen, haftet der Kunde gegenüber HEROLD auf Nutzungsentgelt und Schadensersatz.'},

        /*4*/{header: '§ 5 Preise und Zahlungskonditionen', description: 'Es gelten die am Bestellformular angeführten Preise. Alle angegebenen Preise verstehen sich exklusive der gesetzlichen Umsatzsteuer. Rechnungen sind binnen 14 Tagen ab Rechnungsdatum zur Zahlung fällig. Die Rechnungslegung erfolgt unmittelbar nach Auftragserteilung und unabhängig davon, ob seitens HEROLD bereits Leistungen erbracht wurden, sodass der Kunde vorleistungspflichtig ist. Bei Verzug werden Verzugszinsen in Höhe von 12% p.a. sowie Mahn- und Inkassospesen verrechnet. Sofern eine Bezahlung der Rechnungssumme in Teilbeträgen vereinbart ist, werden bei nicht fristgerechter Bezahlung auch nur eines Teilbetrages sämtliche ausständigen Teilleistungen bis zum Ende des jeweiligen Vertragsjahres ohne weitere Nachfristsetzung fällig. Das vereinbarte Entgelt wird entsprechend der Entwicklung des von der Statistik Österreich verlautbarten Verbraucherindex 2015 (VPI 2015) oder des an seine Stelle tretenden Index angepasst, wobei die Indexzahl des Monats des Vertragsabschlusses als Basiswert heranzuziehen ist. Darüber hinaus sind Preiserhöhungen generell bei Erhöhung der Selbstkosten jederzeit möglich. Gegen Forderungen von HEROLD kann nicht aufgerechnet werden. Dem Preis wird eine Servicepauschale für mobile Datenaufbereitung aufgeschlagen. Die Höhe der Servicepauschale ist umsatzabhängig und wird sowohl bei der erstmaligen Auftragserteilung, als auch bei Folgeaufträgen verrechnet.'},

        /*5*/{header: '§ 6 Gewährleistung und Haftung', description: 'Der Kunde ist verpflichtet, die Website unverzüglich nach Fertigstellung zu prüfen und allfällige Mängel innerhalb der von HEROLD mitgeteilten Frist schriftlich anzuzeigen, anderenfalls können keine Gewährleistungs- und/oder Schadenersatzrechte geltend gemacht werden. Rechtzeitig bekannt gegebene technisch behebbare Mängel werden von HEROLD innerhalb angemessener Frist beseitigt. Bei Vorliegen von Mängeln ist der Kunde nicht zur Zurückbehaltung des Entgelts berechtigt, seine Gewährleistungsbehelfe beschränken sich auf die Vornahme von Verbesserungen. Die CMS-Applikation verwendet ein Vorlagesystem zur Erstellung von Websites, die mit Inhalten des Kunden befüllt, aber nur eingeschränkt individuell programmiert werden. HEROLD und deren Erfüllungsgehilfen haften nur für Vorsatz und grobe Fahrlässigkeit; eine Haftung für leichte Fahrlässigkeit wird ausdrücklich ausgeschlossen. Die Höhe von Schadenersatzansprüchen des Kunden ist grundsätzlich auf die Höhe des Auftragswertes beschränkt und kann bei Dauerschuldverhältnissen das vereinbarte Entgelt für den Abrechnungszeitraum von einem Jahr nicht überschreiten. Eine Haftung für Folgeschäden, insbesondere für entgangenen Gewinn, ist ausgeschlossen. Weiters gilt ein Haftungsausschluss für jegliche Form höherer Gewalt und die hierdurch hervorgerufenen Leistungsausfälle sowie generell für Schäden aus Störungen und Ausfällen von Kunden Websites (einschließlich Webshops) durch Vornahme von SEO-Maßnahmen. Die Frist zur Geltendmachung von Gewährleistungs- und Schadenersatzansprüchen beträgt sechs Monate ab Bereitstellung des Website-Entwurfs. Fehler und Störungen bei der Nutzung der CMS-Applikation sind dem von HEROLD eingesetzten technischen Support unverzüglich mitzuteilen. Dieser wird Fehler und Mängel im Zusammenhang mit der Nutzung der CMS-Applikation zeitnah beseitigen. Der technische Support ist'},

        /*6*/{header: '', description: 'ausschließlich auf die Aufrechterhaltung und Wiederherstellung vertraglich ausdrücklich vereinbarter Eigenschaften beschränkt. Mindestreaktionszeiten werden nicht zugesagt. Der Kunde verpflichtet sich, an den technischen Support alle zur Problembehebung notwendigen Daten zu übermitteln. In der Regel stehen die von HEROLD im Rahmen der Vertragserfüllung verwendeten Serversysteme sowie das E-Mail-Postfach 24 Stunden an 7 Tagen der Woche zur Verfügung. Die übliche Verfügbarkeit innerhalb eines Vertragsjahres beträgt 99%. Eine Unterschreitung der Verfügbarkeit ist möglich im Fall von Wartungsarbeiten, eines Leistungsausfalls dritter Anbieter sowie bei Ausfällen aufgrund höherer Gewalt oder sonstigen technischen Problemen, die nicht im Einflussbereich von HEROLD liegen, insbesondere Störungen bei Übertragungsleistungen oder Störungen bei Vornahme von SEO-Maßnahmen für den Kunden. In diesen Fällen kann der Kunde keine Ansprüche aus einer Nichtverfügbarkeit ableiten. Der Kunde haftet gegenüber HEROLD sowie gegenüber den Technologiepartnern von HEROLD im Falle der Verletzung der Pflichten dieser AGB.'},

        /*7*/{header: '§ 7 Vertragslaufzeit', description: 'Es gilt die am Bestellschein angegebene Mindestvertragslaufzeit. Nach Ablauf der Mindestvertragslaufzeit verlängert sich der Vertrag, sofern dieser nicht zum Ende der Mindestvertragslaufzeit unter Einhaltung einer Kündigungsfrist von drei Monaten (bei Ranking-Boost & Analyse-Pakete von einem Monat) schriftlich gekündigt wird, um die Dauer der erstmals vereinbarten Mindestvertragslaufzeit. Der Vertrag kann sodann erst wieder zum Ende der verlängerten Vertragslaufzeit, dies unter Einhaltung einer Kündigungsfrist von drei Monaten (bei Ranking-Boost & Analyse-Pakete ein Monat), schriftlich gekündigt werden. Bei einer Kündigung vor Ablauf der jeweiligen Vertragslaufzeit durch den Kunden hat dieser das auf die jeweilige Vertragslaufzeit ausständige Entgelt zu entrichten. Mit Beendigung des Vertrages erlischt das Recht zur Nutzung der CMSApplikation sowie der mittels der CMS-Applikation erstellten Websites und deren Hosting; eine Weiterverwendung bzw. Mitnahme der über die CMS-Applikation erstellten Websites, deren Gestaltung, Text, Design und Inhalten nach Vertragsbeendigung ist aufgrund bestehender Urheberrechte ausgeschlossen. HEROLD ist zur außerordentlichen Kündigung aus wichtigem Grund berechtigt. Ein wichtiger Grund liegt insbesondere dann vor, wenn der Kunde wesentliche Pflichten verletzt, was insbesondere dann der Fall ist, wenn der Kunde seinen Zahlungspflichten trotz Nachfristsetzung nicht nachkommt, die CMS-Applikation übermäßig belastet oder eine unangemessen hohen Traffic auf der Website generiert oder die Pflichten dieser AGB verletzt. HEROLD ist berechtigt, aber nicht verpflichtet, den Kunden zur Wiederherstellung des vertragsgemäßen Zustandes mit oder ohne Setzung einer vorübergehenden Zugangssperre aufzufordern.'},

        /*8*/{header: '§ 8 Zustimmungserklärung Datenschutz- und Telekommunikationsgesetz', description: 'Mit Auftragserteilung erklärt sich der Kunde gemäß § 8 Abs. 1 Z 2 DSG 2000 einverstanden, dass die am Bestellschein und am Datenblatt vom Kunden bereitgestellten Daten erfasst und für Werbe- und Marketingzwecke verwendet werden. Der Kunde erklärt sich mit Angabe seiner Telefonnummer und seiner elektronischen Postadresse ausdrücklich einverstanden, von HEROLD Telefonanrufe und elektronische Post zu Werbe- und Marketingzwecken, insbesondere zu Zwecken der Zusendung von Angeboten und Newsletter mit werblichen Informationen zum Unternehmen von HEROLD und Kunden von HEROLD, zu erhalten. Diese Zustimmungserklärung gilt über die vereinbarte oder tatsächliche Vertragsdauer hinaus, können jedoch jederzeit durch Übermittlung eines E-Mails an kundenservice@herold.at widerrufen werden. Der Kunde erklärt sich damit einverstanden, dass HEROLD besuchsfördernde Maßnahmen hinsichtlich der Website des Kunden, insbesondere durch Anlage eines Google Plus- und eines Google My Business-Kontos und Verknüpfung mit der Website des Kunden, durchführen kann. Insbesondere wird HEROLD Maßnahmen setzen, damit der Standort des Kunden in Google Maps sowie im Rahmen der Google Suchergebnisse angezeigt wird. HEROLD übernimmt weder Gewährleistung dafür, dass die Informationen veröffentlicht werden noch dafür, dass sich die Maßnahmen tatsächlich besuchsfördernd auswirken.'},

        /*9*/{header: '§ 9 Domains und Provider', description: 'Bei Registrierung einer im Website-Paket inkludierten Wunsch-Domain durch HEROLD, erwirbt HEROLD sämtliche Rechte an der im Auftrag des Kunden registrierten Domain. Die Rechte an der Domain verbleiben auch nach Beendigung gegenständlicher Vereinbarung bei HEROLD. Eine Übertragung der Rechte an der Domain auf den Kunden während oder nach Beendigung der Vereinbarung mit HEROLD ist nur mit ausdrücklicher Zustimmung von HEROLD kostenpflichtig möglich. Der Kunde erwirbt ausschließlich bei einer kostenpflichtigen Bestellung einer Domain als Zusatzprodukt sämtliche Rechte an der Domain. Bei einem Providerwechsel ist der Kunde verpflichtet alle Vorkehrungen zu treffen, damit die Domain durch HEROLD übernommen werden kann. Sofern der Kunde die von HEROLD bekannt gegebenen Vorkehrungen nicht trifft, ist HEROLD von seiner Leistungspflicht befreit. Im Falle der Bereitstellung der Domain durch den Kunden, sind die Kosten der Domain vom Kunden zu tragen. Im Falle der Sperre der Domain aufgrund eines Zahlungsrückstandes kann die Website des Kunden nicht bereitgestellt werden, wofür HEROLD keine Verantwortung trägt.'},

        /*10*/{header: '§ 10 Belehrung KSchG', description: 'Sofern der Kunde Verbraucher im Sinne des KSchG ist und der Vertrag außerhalb der Geschäftsräumlichkeiten von HEROLD abgeschlossen wurde, ist er berechtigt, binnen 14 Tagen nach Unterzeichnung des Bestellscheins vom Vertrag ohne Angabe von Gründen, zurücktreten. Die Rücktrittserklärung hat schriftlich an HEROLD Business Data GmbH, Guntramsdorfer Straße 105, 2340 Mödling, oder per Telefax an +43 (0) 2236 / 401-8 oder per E-Mail an kundenservice@herold.at zu erfolgen. Um das Widerrufsrecht auszuüben, muss der Verbraucher HEROLD mittels einer eindeutigen Erklärung über den Entschluss, den Vertrag zu widerrufen, informieren. Zur Wahrung der Widerrufsfrist reicht es aus, dass die Mitteilung über die Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist abgesendet wird. Wenn der Verbraucher den Vertrag widerruft, hat HEROLD alle Zahlungen, die HEROLD vom Verbraucher erhalten hat, unverzüglich und spätestens binnen 14 Tagen ab dem Tag zurückzuzahlen, an dem die Mitteilung mit dem Widerruf des Vertrags bei HEROLD eingegangen ist.'},

        /*11*/{header: '§ 11 Drittanbieter', description: 'Der Kunde ist verpflichtet, die Nutzungsbedingungen nachstehender Drittanbieter zu akzeptieren und zu beachten: Google Analytics – Web Analytics and Reporting (http://www.google.com/analytics/), Let’s Encrypt – SSL Certificates (https://letsencrypt.org/repository/).'},

        /*12*/{header: '§ 12 Sonstiges', description: 'Es gilt österreichisches Recht unter Ausschluss von UN-Kaufrecht. Ausschließlicher Gerichtsstand ist für beide Teile das sachlich zuständige und wertzuständige Gericht für den ersten Wiener Gemeindebezirk.'},


        /*13*/{header: 'Zusätzliche spezielle Bedingungen für das Besucherpaket:', description: 'Eine bestimmte Reihung bzw. Platzierung von Anzeigen oder die Erreichung eines bestimmten Geschäftserfolges wird nicht garantiert. Für eine optimale Festlegung von Suchbegriffen und Gestaltung des Anzeigentextes ist die rechtzeitige Bereitstellung von Inhalten und Informationen durch den Besteller erforderlich. Es gilt die im Bestellschein angegebene Vertragslaufzeit; vor Ablauf der Vertragslaufzeit ist eine Kündigung nicht möglich. Der Einsatz des Werbebudgets des Bestellers wird gleichmäßig über die Vertragslaufzeit verteilt. Eine Rückzahlung des Werbebudgets ist ausgeschlossen. Für das Besucherpaket gelten zusätzlich die AGB für Adwords-Kampagnen (www.herold.at).'},

        /*14*/{header: 'Zusätzliche spezielle Bedingungen für die Erstellung von Facebook-Fanpages:', description: 'Der Besteller ist mit der Anlage eines Kontos bei Facebook sowie mit dem Zugriff darauf zur Bereitstellung der Fanpage und der allfälligen Wartung und Kontenbetreuung einverstanden. Der Besteller hat sich mit den Nutzungsbedingungen und Werberichtlinien von Facebook vertraut zu machen und ist für deren Einhaltung verantwortlich. Insbesondere akzeptiert der Besteller, dass mit Eröffnung eines Facebook Kontos und der Bereitstellung von Inhalten an Facebook eine nichtexklusive, übertragbare, unterlizenzierbare, unentgeltliche, weltweite Lizenz für die Nutzung dieser Inhalte erteilt wird. HEROLD übernimmt keine Gewährleistung zur Verfügbarkeit von Facebook, oder der Fanpage. Sperren des Kontos bzw. der Fanpage sowie Änderungen der technischen oder sonstigen Gegebenheiten durch Facebook, welche der dauerhaften Bereitstellung der Fanpage in der bestellten Form entgegen stehen, sind nicht von HEROLD zu vertreten und es besteht in diesem Fall keine Gewährleistungs- oder Schadenersatzrechte gegenüber HEROLD. Der Besteller ist damit einverstanden, dass HEROLD zur Gestaltung der Fanpage sämtliche auf der Website des Bestellers veröffentlichten Inhalte übernehmen und zum Zwecke der Gestaltung der Fanpage verwendet. Mit Unterzeichnung des Bestellscheins erklärt sich der Besteller ausdrücklich einverstanden, dass einer derartigen Verwendung von Inhalten keine Rechte Dritter entgegenstehen. Sämtliche Inhalte sind urheberrechtlich geschützt und dürfen vom Besteller nur für die Dauer der Vereinbarung zur Veröffentlichung auf der Fanpage verwendet werden.'},

        /*15*/{header: 'Zusätzliche spezielle Bedingungen für das Modul Online Shop:', description: 'Die Anlage des Online Shops sowie die laufende Änderung und Wartung der Inhalte erfolgt durch den Besteller, es sei denn, es wird eine abweichende Regelung getroffen. HEROLD trägt keine Verantwortung für den Verlust von Inhalten des Online Shops oder der nicht erfolgten Zustellung von Bestellungen im Online Shop. Die Abwicklung von Zahlungen erfolgt über gesonderte Verträge mit dritten Anbietern und liegt nicht im Verantwortungsbereich von HEROLD. Für den Online Shop gelten zusätzlich die AGB für HEROLD Online Shop (www.herold.at).'},

        /*16*/{header: 'Zusätzliche spezielle Bedingungen für das Begleiterpaket:', description: 'Bei Bestellung des Begleiterpakets wird Online-Werbung für den Besteller auf dritten Websites ausgeliefert. Der Besteller ist damit einverstanden, dass spezielle Cookies auf seiner Website implementiert werden, die eine Erkennbarkeit der Nutzer ermöglichen. Der Besteller erhält einen Endbericht über die veröffentlichte Werbung, welcher ausschließlich die Sichtkontakte gesamt enthält. Für die Gestaltung der Online-Werbung gelten die AGB für Werbeeintragungen, Zeileneinträge und Werbedienstleistungen (www.herold.at ).'},

        /*17*/{header: 'Zusätzliche spezielle Bedingungen für das Erklärvideo:', description: 'Nach Bereitstellung des Erklärvideos gilt dieses binnen 5 Werktagen als abgenommen, sofern innerhalb dieser Frist keine schriftliche Mängelanzeige an das HEROLD Kundenservice erfolgt. In diesem Fall wird das Erklärvideo in der gelieferten Form veröffentlicht und Gewährleistungs- und Schadenersatzansprüche sind ausgeschlossen. Die Bereitstellung des Erklärvideos erfolgt durch HEROLD in dem vom Besteller in Auftrag gegebenen Werbemedium für die Dauer der Werbeeinschaltung, längstens jedoch für die Dauer von einem Jahr ab erstmaliger Veröffentlichung des Werbematerials im jeweiligen Werbemedium. HEROLD ist weiters berechtigt, an einer geeigneten Stelle einen Hinweis auf seine Urheberschaft bzw. die Urheberschaft des Dienstleisters anzubringen. HEROLD und der Dienstleister, der mit der Produktion des Erklärvideos beauftragt ist, sind berechtigt, das Werbematerial zu eigenen Werbe-, Marketing- und Promotionzwecken zu verwenden. Der Besteller ist nicht berechtigt, das Erklärvideo für andere Zwecke zu verwenden, dieses zu verändern oder über andere Werbeformen zugänglich zu machen. Für gemeinsam mit den diesen AGB gemäß § 1 unterliegenden Leistungen vom Kunden beauftragte Erklärvideos gelten zusätzlich die AGB für Werbeeintragungen, Zeileneinträge und Werbedienstleistungen (www.herold.at).'},

    ];

    pdfMake.fonts = {
        Helvetica: {
            normal: 'HelveticaNeueLT-LightCond.ttf',
            bold: 'HelveticaNeue-BoldCond.ttf',
        }
    };

    function renderTermsAndConditions(termsAndCons) {
        var content = [];

        content.push(
            {
                columns: [
                    [
                        {text: termsAndCons[0].header,   style: 'terms_header'},
                        {text: termsAndCons[0].description,   style: 'terms'},
                        {text: termsAndCons[1].header,   style: 'terms_header'},
                        {text: termsAndCons[1].description,   style: 'terms'},
                        {text: termsAndCons[2].header,   style: 'terms_header'},
                        {text: termsAndCons[2].description,   style: 'terms'},
                        {text: termsAndCons[3].header,   style: 'terms_header'},
                        {text: termsAndCons[3].description,   style: 'terms'},
                        {text: termsAndCons[4].header,   style: 'terms_header'},
                        {text: termsAndCons[4].description,   style: 'terms'},
                        {text: termsAndCons[5].header,   style: 'terms_header'},
                        {text: termsAndCons[5].description,   style: 'terms'},

                    ],
                    [
                        {text: termsAndCons[6].header,   style: 'terms_header'},
                        {text: termsAndCons[6].description,   style: 'terms'},
                        {text: termsAndCons[7].header,   style: 'terms_header'},
                        {text: termsAndCons[7].description,   style: 'terms'},
                        {text: termsAndCons[8].header,   style: 'terms_header'},
                        {text: termsAndCons[8].description,   style: 'terms_bold'},
                        {text: termsAndCons[9].header,   style: 'terms_header'},
                        {text: termsAndCons[9].description,   style: 'terms'},
                        {text: termsAndCons[10].header,   style: 'terms_header'},
                        {text: termsAndCons[10].description,   style: 'terms'},
                        {text: termsAndCons[11].header,   style: 'terms_header'},
                        {text: termsAndCons[11].description,   style: 'terms'},
                        {text: termsAndCons[12].header,   style: 'terms_header'},
                        {text: termsAndCons[12].description,   style: 'terms'},
                        {text: termsAndCons[13].header,   style: 'terms_header'},
                        {text: termsAndCons[13].description,   style: 'terms'},
                        {text: termsAndCons[14].header,   style: 'terms_header'},
                        {text: termsAndCons[14].description,   style: 'terms'},
                        {text: termsAndCons[15].header,   style: 'terms_header'},
                        {text: termsAndCons[15].description,   style: 'terms'},
                        {text: termsAndCons[16].header,   style: 'terms_header'},
                        {text: termsAndCons[16].description,   style: 'terms'},
                        {text: termsAndCons[17].header,   style: 'terms_header'},
                        {text: termsAndCons[17].description,   style: 'terms'},
                    ]
                ]
            }
        );


        return content;
    }


    var dd = {

        content: [
            renderTermsAndConditions(termsAndConditions),
        ],

        images: {
            mySuperImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAABHCAIAAAAC1/wuAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATuSURBVHhe7dzhSxtnHMDx/SeKdrKVjdp1shdzloFvOljpCnaMUcYY1b2Y615MKXRv2u1daRxFYQzK+iYtcxskL3xhXixQ3FihJ1MaGQqxxc5YZdEZNI7Eyxp9EpPncpe7KPGX8/vj86q5u1zKfTWJT/JCa1MzALFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRjk6iZ4aMVK5sFsKXTlo20zju1R4wMupfa5iMEThley/uxkyvJeLT0R8Dvd1t6lDl2nsGh++NTzyaX3y2lt5WeznNdnr12WL80W/j90YGegqnV1Tb/2GJS6GE2tHjZFIrib/nY7+PB2/0nT6uH9bXSFTbTCM70eJknkavv1deaWtH721jeUttUMtsLT+83fdaa8kxDy/RkjHTjyP6g/UzEtU20zRIos8v3H8igy/vHi2v7d1bxnpW3Vb7ZNcf3jrbUjysiETzk4mH+7v0g/sUiWqbaRom0VxuJXq1eNV2DkaWTPXv+xtzKTLQWTismERz5qbxbbd2cJ8iUW0zjcdEV41gIDDkzs0vz79key+biZlJw3amZxMpyw+Hrfngx+rEjl2JJCv+Cs2/1ExUHrsXq9lk5Ir9ee43UXM17vA4d2bmcTJd4cfNxsT1VyzH9yMS1TbTeEw0Eerd29e9Gs6t64uxhfILN5MI9atb3xye1gs1M4v3R+zeVdrV0t03fH8xo+eQnRruUtscfKIlTyUcdPYFZ9Jqj8Js/3Xn7JF434hEtc00YhNtbvtsbFltvDsliVqfT5pPwn0dxX3tdXwaeqI3uvegDivRis8LPN51wyJRbTON3EQtl7tjosnIwLHy3W28ejX6r9qnMBISbTo3MrWh9lFDon5jvbxqG8dE83++czdTP1x0Orfq15/lt+jG9PA5das1Ufc/O5z2PcRE933XDYtEvY5jou6nrJkarj/La1Ez/tNHhV1I1EdI1OvUJ1Hnd3Qnp+eWy9/lLP8DJon6CIl6nfok6n6208m48fPXF9pLlgGRqI+QqNdxTNR9CWX2l+jK7B+j1RJdHvt8b5GQE8tLXCGJXvh+ZlPto4ZE/aa2y8txLxGJ7k42Gb32RvGA1kS354Lvnyi5RzsnPgjO6SsYJCT6+jcT69p5kajf+DvRXO6/P0e6X1QHrLB0IZuK3b1c5TMix9/uvxtLWfY87KULbW99MhR9qr+kYOmC79Ql0dRsNBQKu/RLoFctfPd4bi2nL14Lz+qr4krW6NotADTTa0vqjz7WWVqttM6uygLATGrF/oh7M3nnw53f4ZZEzfRqlf1tTosFgP5Tl0Q9TcYYandxL5V1fRVdUduqSRmBM4Vb67aM3uUUHo71GXiNwzJ6H/JZoid7QwtqWzWliTa39Xw3ZXnK6n2qfhjN5Rx0onwYzY+OVqL1+ki3yznIRPlIt18dtUR35L8YZTT6IObxi1FiD34ddffFKC5n34nmX0XvfjHKzcvvnCo/K587OokCDYlEAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQBwZqa/wd0Ed3Ls5EBJgAAAABJRU5ErkJggg=='
        },

        header:  function() {

            return {
                table: {
                    widths: [140, '*'],
                    body: [
                        [{image: 'mySuperImage', width: 140, height: 40, margin: [-5, 0, 0, -2], border: [false, false, false, true]},
                            {text: 'Allgemeine Geschäftsbedingungen für Website, CMS-Applikation, Hosting und verwandte Produkte', fontSize: 15, alignment: 'right',  bold: false, margin: [0, 0, 0, 0], border: [false, false, false, true]}],
                    ]
                }, margin: [21,21]
            };
        },


        footer: function() {
            return {
                table: {
                    widths: [400, '*'],
                    body: [
                        [{text: 'HEROLD Business Data GmbH, A-2340 Mödling, Guntramsdorfer Straße 105, Telefon: +43 (0) 2236/401-38133, Fax +43 (0) 2236/401-35160 Gerichtsstand Wien, UID: ATU57270679, DVR: 0871885, FN: 233171z', fontSize: 6, alignment: 'left', margin: [0, 0, 0, 0], border: [false, true, false, false]},
                            {text: 'E-Mail: kundenservice@herold.at, www.herold.at\n AGB_CMS_Applikation (V19_20170522)', fontSize: 6, alignment: 'right', margin: [0, 0, 0, 0], border: [false, true, false, false]}]
                    ]
                }, margin: [21,0]
            };

        },

        // Styles
        styles: {
            terms_header: {
                bold: true,
                fontSize: 5.8,
                alignment: 'justify',
                lineHeight: 1.1,
            },
            terms: {
                fontSize: 5.7,
                alignment: 'justify'
            },
            terms_bold: {
                bold: true,
                fontSize: 5.7,
                alignment: 'justify'
            },

        },

        // Default Styles
        defaultStyle: {
            columnGap: 5,
            font: 'Helvetica',
        },

        // Page margins
        pageMargins: [ 21, 70, 21, 55 ]
    };

    return dd;
}
