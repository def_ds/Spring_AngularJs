export function start() {
    var termsAndConditions = [

        /*0*/{header: '§ 1 Vertragsabschluss und Leistungsgegenstand', description: '1.1 Diese Allgemeinen Geschäftsbedingungen (AGB) gelten für die Bereitstellung von Domains, die Bereitstellung von EMail- Postfächern, die SSL Zertifizierung und das Hosting (die „Vertragsprodukte“). Der konkrete Leistungsumfang ist im Angebot bzw. auf dem Bestellschein definiert. Das Produkt „Hosting“ in Zusammenhang mit WordPress-Websites ist nur gemeinsam mit dem kostenpflichtigen Produkt „Software-Update-Service“ zu beziehen. Bei einem Bezug des Produktes „Hosting“ und des Produktes „Software Update-Service“ kommt zusätzlich die „Leistungsbeschreibung für Hosting und Software-Update-Service sowie Software-Support-Service für WordPress-Websites“ zur Anwendung, deren Übernahme der Kunde mit Unterzeichnung des Bestellscheins bestätigt.'
        + '\n1.2 Der Vertrag kommt durch Annahme des von HEROLD unterbreiteten Angebots innerhalb der Annahmefrist oder durch Unterzeichnung des Bestellformulars zustande. Bei einem Vertragsabschluss durch Unterzeichnung eines Bestellformulars gelten die vom Kunden unterzeichneten Bestellformulare als Angebot, das von HEROLD innerhalb von vier Wochen ab Unterzeichnung des Bestellformulars abgelehnt werden kann. Zur Fristwahrung genügt bei mündlicher Ablehnung der Ausspruch innerhalb der Frist bzw. bei schriftlicher Ablehnung die rechtzeitige Absendung.'
        + '\n1.3 HEROLD liefert und leistet ausschließlich zu diesen AGB. Allfällige bestehende Liefer-, Einkaufs- oder Vertragsbedingungen des Kunden finden keine Anwendung. Grundlage dieses Vertrages ist ausschließlich das von HEROLD unterbereitete Angebot (bzw. der Bestellschein) sowie diese AGB. Mündliche Zusagen werden nicht Vertragsinhalt.'},

        /*1*/{header: '\n§ 2 Leistungen zur Domain-Registrierung und Webhosting', description: '2.1 Das zur Registrierung der jeweiligen Domain erforderliche Vertragsverhältnis kommt direkt zwischen dem Kunden und der jeweiligen Domainvergabestelle und/oder dem jeweiligen Registrar zustande. HEROLD wird lediglich als Vermittler tätig und hat keinen Einfluss auf die Vergabe einer Domain.'
        + '\n2.2 Der Kunde ist verpflichtet die von der jeweiligen Domain-Vergabestelle vorgeschriebenen Bedingungen betreffend der Nutzung der Domain zu beachten.'
        + '\n2.3 Die Registrierung der Domains erfolgt in einem automatisierten Verfahren; hierbei verfährt die Registrierungsstelle nach dem Prioritätsprinzip, d.h. die erste eingehende Registrierung erhält den Zuschlag. Eine Gewähr für die Zuteilung einer bestellten Domain kann daher nicht übernommen werden.'
        + '\n2.4 Der Kunde ist verpflichtet, an allen Handlungen, die für die Registrierung, Übertragung oder Löschung der bestellten Domain erforderlich sind, vollumfänglich mitzuwirken.'
        + '\n2.5 Der Kunde ist für die Zulässigkeit der über die bestellte Domain bereitgestellten Inhalte (dazu zählen auch Verlinkungen), insbesondere in urheberrechtlicher, wettbewerbsrechtlicher und strafrechtlicher Hinsicht, selbst verantwortlich. Insbesondere verpflichtet sich der Kunde, im Rahmen seiner Präsenz keine pornografischen, gewaltverherrlichenden oder volksverhetzenden Inhalte darzustellen, nicht zu Straftaten aufzurufen oder Anleitungen hierfür darzustellen und keine Leistungen anzubieten oder anbieten zu lassen, die pornografische und/oder erotische Inhalte (z.B. Nacktbilder, Peepshows etc.) zum Gegenstand haben. Gleiches gilt für die Versendung von E-Mails. Eine Überwachung oder Überprüfung der Inhalte durch HEROLD findet nicht statt. HEROLD behält sich das Recht vor, die Domain bei Verdacht auf Rechtswidrigkeit vorübergehend oder dauerhaft zu sperren, jedenfalls aber so lange, wie die Rechtsverletzung bzw. der Verdacht einer Rechtsverletzung besteht. Alternativ besteht für HEROLD die Möglichkeit, den Vertrag außerordentlich, dh fristlos, zu kündigen. Der Entgeltanspruch von HEROLD betreffend die Bereitstellung der Domain besteht fort, solange eine Sperrung der Domain aus vom Kunden verursachten Gründen vorgenommen wurde.'
        + '\n2.6 Solange der Kunde selbst zu einer Domain noch keine Inhalte bereitstellt, ist HEROLD berechtigt, eigene Inhalte, wie zB auch Werbung für HEROLD oder Dritte, einzublenden.'
        + '\n2.7 HEROLD nimmt keine Datensicherung der gehosteten Seiten vor. Der Kunde hat selbst dafür zu sorgen, Sicherheitskopien seiner Web-Daten zu erstellen.'
        + '\n2.8 Mit Beendigung der Vereinbarung wird HEROLD die Daten des Kunden (Inhalte der Website, E-Mail-Postfach) unwiderruflich löschen. Weiters erfolgt eine unwiderrufliche Rückgabe der Domain.'
        + '\n2.9 Der Kunde hat dafür Sorge zu tragen, dass durch seine eigene Web-Präsenz (inkl. Scripte, Datenbanken, Programme, etc) keine Web-Präsenzen oder Angebote Dritter, sowie die Serverstabilität, Serverperformance oder Verfügbarkeit von Diensten nicht beeinträchtigt werden .HEROLD ist berechtigt, die Serverstabilität störende Dienste des Kunden ganz oder teilweise zu sperren, insbesondere wenn die Dienste und/oder Inhalte ein übliches Maß überschreiten (Fair-Use- Policy). Das Versenden von Massenmails ist nicht erlaubt. Der Kunde verpflichtet sich, den Server nur für allgemein übliche Dienste zu verwenden und haftet HEROLD für alle Schäden, die der Kunde HEROLD durch einen vereinbarungs-, gesetzwidrigen oder unüblichen Gebrauch des Servers zufügt.'},

        /*2*/{header: '\n§ 3 Leistungen zur Bereitstellung eines E-Mail-Postfaches', description: '3.1 Es gelten die im Angebot bzw. am Bestellschein angegebenen Speicherkapazitäten sowie technischen Daten. Eine Bereitstellung des E-Mail-Postfaches ist nur für die Dauer der Vereinbarung zur Bereitstellung einer Domain gemäß § 2 möglich. Im Falle der Beendigung der Vereinbarung zur Bereitstellung einer Domain endet auch die Bereitstellung des E-Mail-Postfaches automatisch; eine gesonderte Kündigung ist hierfür nicht erforderlich.'
        + '\n3.2 Der Kunde ist für die Zulässigkeit der verwendeten E-Mail-Adressen sowie die Zulässigkeit der über die E-Mail- Adressen versendeten Inhalte, insbesondere in urheberrechtlicher, wettbewerbsrechtlicher und strafrechtlicher Hinsicht, selbst verantwortlich. Es sind bei dem Versand von E-Mails insbesondere auch die telekommunikationsrechtlichen Vorschriften einzuhalten. Dem Kunden ist bekannt, dass der Versand von Spam-E-Mails unzulässig ist. HEROLD ist generell bei Verletzung von gesetzlichen Vorschriften bei der Verwendung des E-Mail-Postfaches, insbesondere jedoch in folgenden Fällen, zur Sperre des E-Mail-Postfaches berechtigt:'
        + '\n• E-Mail-Verkehr, der hinsichtlich Form, Inhalt oder verfolgtem Zweck gegen gesetzliche Verbote/Gebote, Rechte Dritter oder die guten Sitten verstößt;'
        + '\n• Verwendung von Systemen, die Angriffe auf Computersysteme ermöglichen (Viren, Würmer, Trojaner);'
        + '\n• Verwendung unrichtiger oder verschleierter Absenderinformationen;'
        + '\n• Versand unzulässiger Spam-E-Mails; Klarstellend wird festgehalten, dass bereits der Verdacht der unzulässigen Verwendung des E-Mail-Postfaches zur Sperre berechtigt und eine Sperre des E-Mail-Postfaches den Kunden nicht von seiner Zahlungspflicht befreit.'
        + '\n3.3 HEROLD übernimmt keine Gewähr für die Weiterleitung von E-Mails, SMS oder sonstigen Nachrichten an den Empfänger, ebenso wenig für die richtige Wiedergabe der Nachrichten des Kunden.'
        + '\n3.4 Der Kunde wird per E-Mail informiert, sobald das vereinbarte Datentransfervolumen (Traffic) oder das vereinbarte Datenspeichervolumen die für den jeweiligen Monat zu 90% erreicht wurde. Bei vollständigem Erreichen des vereinbarten Datentransfervolumens oder des Datenspeichervolumens erfolgt eine Sperre, sodass eine weitere Übertragung von Inhalten sowie ein Erhalt von Nachrichten nicht mehr möglich sind. Eine Kapazitätserweiterung ist kostenpflichtig und gesondert zu beauftragen; die Konditionen werden dem Kunden bei Anfrage mitgeteilt.'
        + '\n3.5 Das E-Mail Postfach sieht standardmäßig einen Anti-Spam-Filter und ein Virenschutzprogramm vor, das der Kunde in seinem E-Mail-Account selbst administrieren kann. HEROLD weist darauf hin, dass kein auf dem Markt befindliches Virenschutzprogramm eine hundertprozentige Sicherheit bieten kann. Dies beruht u.a. auf der Vielzahl der sich im Umlauf befindlichen Viren und deren ständiger Aktualisierung. Der Kunde wird deshalb ausdrücklich darauf hingewiesen, dass auch geprüfte E-Mails einen Virus enthalten können. Der Kunde hat insbesondere vor diesem Hintergrund für eine aktuelle Sicherung seiner Daten Sorge zu tragen. HEROLD kann für eine Beschädigung von Daten und/oder Soft- und Hardware aufgrund von Viren in keinem Fall verantwortlich gemacht werden. Der Kunde wird darauf hingewiesen, dass es ihm obliegt, in regelmäßigen Abständen (mindestens einmal täglich) eine Datensicherung durchzuführen.'
        + '\n3.6 Der Kunde verpflichtet sich, die von HEROLD zugeteilten Passwörter streng geheim zu halten und HEROLD unverzüglich zu informieren, sobald er davon Kenntnis erlangt, dass unbefugten Dritten das Passwort bekannt ist. Sollten infolge Verschuldens des Kunden Dritte durch Gebrauch der Passwörter Leistungen von HEROLD nutzen, haftet der Kunde gegenüber HEROLD auf Nutzungsentgelt und Schadensersatz.'
        + '\n3.7 Die Verfügbarkeit des E-Mail-Postfaches beträgt 99% pro Kalendermonat. Eine Unterschreitung der Verfügbarkeit ist möglich im Falle von Wartungsarbeiten, im Falle eines Leistungsausfalls dritter Anbieter sowie bei Ausfällen aufgrund höherer Gewalt oder sonstigen technischen Problemen, die nicht im Einflussbereich von HEROLD stehen (insbesondere Störungen bei Übertragungsleitungen). In diesen Fällen kann der Kunde keine Ansprüche aus einer Nichtverfügbarkeit ableiten.'},

        /*3*/{header: '\n§ 4 Besondere Konditionen für das Produkt SSL Zertifizierung', description: '4.1 Eine Vergabe von SSL Zertifikaten ist nur an den jeweiligen Domaininhaber möglich. Mit Beendigung der Domainbereitstellung endet die SSL Zertifizierung.'
        + '\n4.2 Die Implementierung des SSL Zertifikates hat der Kunde vorzunehmen (ausgenommen HEROLD Webpakete, bei denen die Implementierung durch HEROLD erfolgt). HEROLD stellt dem Kunden die hierfür erforderlichen Daten zur Verfügung.'},

        /*4*/{header: '', description: '4.3 Der Kunde ist zur Bereitstellung aller notwendigen Informationen an die Zertifizierungsstelle und/oder an HEROLD verpflichtet.'},

        /*5*/{header: '\n§ 5 Besondere Konditionen für das Produkt HEROLD File Tresor', description: '5.1 Die Basislizenz berechtigt zur Nutzung der Software auf zwei Geräten. Die Verwendung der Software auf zusätzlichen Geräten bedarf kostenpflichtiger Zusatzlizenzen.'
        + '\n5.2 Der Kunde ist zur regelmäßigen Speicherung der Daten verpflichtet. HEROLD kann für Schäden, die sich aus einer nicht erfolgten oder nicht ordnungsgemäßen Speicherung der Daten ergeben, nicht haftbar gemacht werden.'
        + '\n5.3 Bei Vertragsbeendigung kommt es zu einer Deaktivierung des Kundenaccounts. Gleichzeitig mit Beendigung des Vertrages werden sämtliche Daten gelöscht. Der Kunde ist dafür verantwortlich, die Daten entsprechend abzusichern.'
        + '\n5.4 Der Kunde beauftragt HEROLD mit der Verarbeitung von Daten. Diesbezüglich schließen Kunde und HEROLD eine Dienstleistervereinbarung gemäß § 10 DSG2000.'
        + '\n5.5 Der Kunde ist dafür verantwortlich, dass die bereitgestellten Daten rechtlich zulässig sind. HEROLD ist berechtigt, solche Daten, bei denen ein begründeter Verdacht besteht, dass diese unzulässig sind oder eine Speicherung dieser Daten gegen das Gesetz oder gegen die guten Sitten verstoßen würde, unverzüglich und unwiederbringlich zu löschen.'},

        /*6*/{header: '\n§ 6 Preise und Zahlungskonditionen', description: '6.1 Es gelten die im Angebot bzw. am Bestellschein angeführten Preise. Alle angegebenen Preise verstehen sich exklusive der gesetzlichen Umsatzsteuer. Sämtliche Rechnungen sind binnen 14 Tagen netto ab Rechnungsdatum zur Zahlung fällig. Die Rechnungslegung erfolgt, sofern nichts Abweichendes schriftlich vereinbart wird, unverzüglich nach Bereitstellung der Domain.'
        + '\n6.2 Im Falle des Zahlungsverzuges werden Verzugszinsen in Höhe von 12% p.a. sowie Mahn- und Inkassospesen verrechnet.'
        + '\n6.3 Sofern eine Bezahlung der Rechnungssumme in Teilbeträgen vereinbart ist, werden bei nicht fristgerechter Bezahlung auch nur eines Teilbetrages sämtliche ausständigen Teilleistungen bis zum Ende des jeweiligen Vertragsjahres ohne weitere Nachfristsetzung fällig.'
        + '\n6.4 Das Entgelt wird entsprechend des von der Statistik Österreich verlautbarten Verbraucherpreisindex 2010 (VPI) oder des an seine Stelle tretenden Index angepasst, wobei die Indexzahl des Monats des Vertragsabschlusses als Basiswert heranzuziehen ist. Darüber hinaus sind Preiserhöhungen generell bei Erhöhung der Selbstkosten jederzeit möglich.'
        + '\n6.5 Der Kunde darf nur mit von HEROLD anerkannten oder rechtskräftig festgestellten Forderungen aufrechnen.'
        + '\n6,6 Dem Preis wird eine Servicepauschale für mobile Datenaufbereitung aufgeschlagen. Die Höhe der Servicepauschale ist umsatzabhängig und wird sowohl bei der erstmaligen Auftragserteilung, als auch bei Folgeaufträgen verrechnet.'},

        /*7*/{header: '\n§ 7 Vertragslaufzeit und Vertragsbeendigung', description: '7.1 Die Vereinbarung tritt mit Annahme des Angebotes durch den Kunden bzw. mit Unterzeichnung des Bestellscheins in Kraft und gilt für die Dauer von einem Jahr ab Bereitstellung des jeweiligen Vertragsproduktes. Danach verlängert sich die Vereinbarung automatisch um jeweils ein weiteres Jahr, sofern die Vereinbarung nicht unter Einhaltung einer Kündigungsfrist von drei Monaten zum Ende des jeweiligen Vertragsjahres durch den Kunden oder durch HEROLD schriftlich gekündigt wird.'
        + '\n7.2 Darüber hinaus ist eine außerordentliche Kündigung durch HEROLD jederzeit möglich, sofern der Kunde die an ihn gelegten Rechnungen nicht rechtzeitig bezahlt oder die Domain und/oder das E-Mail Postfach missbräuchlich, insbesondere entgegen der Bestimmungen dieser allgemeinen Geschäftsbedingungen, nutzt.'
        + '\n7.3 Mit Vertragsbeendigung endet das Recht zur Nutzung der Vertragsprodukte durch den Kunden. Es kommt zu einer Rückgabe der Domain und zu einer Löschung der Inhalte (einschließlich Mails).'},

        /*8*/{header: '\n§ 8 Zustimmungserklärung Datenschutz- und Telekommunikationsgesetz', description: '8.1 Mit Auftragserteilung erklärt sich der Kunde gemäß § 8 Abs. 1 Z 2 DSG 2000 einverstanden, dass die vom Kunden – in welcher Form auch immer – bereitgestellten, personenbezogenen Daten erfasst und für Werbe- und Marketingzwecke verwendet werden. Der Kunde erklärt sich mit Angabe seiner Telefonnummer und seiner elektronischen Postadresse ausdrücklich einverstanden, von HEROLD Telefonanrufe und elektronische Post zu Werbeund Marketingzwecken, insbesondere zu Zwecken der Zusendung von Angeboten und Newsletter mit werblichen Informationen zum Unternehmen von HEROLD und Kunden von HEROLD, zu erhalten.'
        + '\n8.2 Diese Zustimmungserklärung gilt über die vereinbarte oder tatsächliche Vertragsdauer hinaus, kann jedoch jederzeit durch Übermittlung eines E-Mails an kundenservice@herold.at widerrufen werden.'
        + '\n8.3 Der Kunde ist damit einverstanden, dass personenbezogene Daten sowie sonstige Daten, die im Rahmen des Hosting bereitgestellt werden, sowie die zur Vertragserfüllung erforderlichen Informationen an den Subauftragnehmer Binder Trittenwein Kommunikation GmbH, Lessingstraße 32/1, 8010 Graz, überlassen werden. Der Zweck der Überlassung liegt in der technischen Bereitstellung der Website für den Kunden (Hosting).'},

        /*9*/{header: '\n§ 9 Belehrung betreffend das Rücktrittsrecht für Konsumenten', description: 'Sofern der Kunde Konsument im Sinne des Konsumentenschutzgesetzes ist und der Vertrag außerhalb der Geschäftsräumlichkeiten von HEROLD abgeschlossen wurde, ist er berechtigt, binnen 14 Tagen ab Vertragsunterzeichnung vom Vertrag, dies ohne Angabe von Gründen, zurücktreten. Die Rücktrittserklärung hat schriftlich an HEROLD Business Data GmbH, Guntramsdorfer Straße 105, 2340 Mödling oder per E-Mail an kundenservice@herold.at zu erfolgen. Bei Abschluss eines Vertrages im Fernabsatz (dh unter Verwendung von Fernkommunikationsmittel, wie zB Internet, Telefon, Fax) ist der Konsument berechtigt, binnen 14 Tagen ab Vertragsabschluss ohne Angaben von Gründen den Vertrag zu widerrufen. Um das Widerrufsrecht auszuüben, muss der Konsument HEROLD mittels einer eindeutigen Erklärung (zB ein mit der Post versandter Brief an HEROLD Business Data GmbH, Guntramsdorfer Straße 105, 2340 Mödling, ein Telefax an +43 (0) 2236 / 401-8 oder ein E-Mail an kundenservice@herold.at über den Entschluss, den Vertrag zu widerrufen, informieren. Zur Wahrung der Widerrufsfrist reicht es aus, dass die Mitteilung über die Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist abgesendet wird. Wenn der Konsument den Vertrag widerruft, hat HEROLD alle Zahlungen, die HEROLD vom Konsumenten erhalten hat, unverzüglich und spätestens binnen vierzehn Tagen ab dem Tag zurückzuzahlen, an dem die Mitteilung mit dem Widerruf des Vertrags bei HEROLD eingegangen ist. Die Rückzahlung erfolgt durch Rücküberweisung.'},

        /*10*/{header: '\n§ 10 Gewährleistung und Haftung', description: '10.1 Der Kunde hat eventuell auftretende Mängel zu dokumentieren und insbesondere unter Protokollierungangezeigter Fehlermeldungen unverzüglich schriftlich zu melden, anderenfalls können keine Gewährleistungsund/ oder Schadenersatzansprüche geltend gemacht werden. Der Kunde hat HEROLD bei einer möglichen Mangelbeseitigung umfassend zu unterstützen.'
        + '\n10.2 Rechtzeitig bekannt gegebene, technisch behebbare Mängel werden von HEROLD innerhalb angemessener Frist beseitigt. Bei Vorliegen von Mängeln ist der Kunde nicht zur Zurückbehaltung des Entgelts berechtigt, seine Gewährleistungsbehelfe beschränken sich auf die Vornahme von Verbesserungen.'
        + '\n10.3 HEROLD haftet nur für Vorsatz und grobe Fahrlässigkeit; eine Haftung für leichte Fahrlässigkeit wird ausdrücklich ausgeschlossen. Die Höhe von Schadenersatzansprüchen des Kunden ist auf die Höhe des Auftragswertes beschränkt. Eine Haftung für Folgeschäden, insbesondere für entgangenen Gewinn, ist ausgeschlossen.'
        + '\n10.4 HEROLD haftet nicht für einen allfälligen Datenverlust oder die Beschädigung von Daten. HEROLD haftet weiters nicht für Schäden, die am Computer oder an sonstigen technischen Geräten des Kunden auftreten. HEROLD haftet auch nicht für die korrekte Funktion von Infrastrukturen des Kunden, insbesondere von Übertragungswegen des Internet. Weiters gilt ein Haftungsausschluss für jegliche Form höherer Gewalt und die hierdurch hervorgerufenen Leistungsausfälle.'
        + '\n10.5 Die Frist zur Geltendmachung von Gewährleistungsbehelfen, die einvernehmlich auf Verbesserung beschränkt werden, beträgt sechs Monate ab erstmaliger Bereitstellung der Leistung. Sofern Schäden nicht binnen einer Frist von sechs Monaten ab Schadenseintritt geltend gemacht werden, gelten diese als verjährt.'},

        /*11*/{header: '\n§ 11 Sonstiges', description: '11.1 HEROLD ist berechtigt, den Firmennamen, das Logo und die Art der dem Kunden erbrachten Leistung als Referenz gegenüber Dritten zu verwenden, selbst wenn das Vertragsverhältnis mit dem Kunden schon beendet ist.'
        + '\n11.2 Die allfällige Unwirksamkeit einzelner Bestimmungen dieser allgemeinen Geschäftsbedingungen lässt die Geltung der restlichen Bestimmungen unberührt. An die Stelle der unwirksamen Bestimmung tritt eine wirksame Bestimmung, die erster nach deren Sinn und Zweck rechtlich und wirtschaftlich am nächsten kommt.'
        + '\n11.3 Es gilt österreichisches Recht unter Ausschluss von UN-Kaufrecht. Ausschließlicher Gerichtsstand ist für beide Teile das sachlich zuständige und wertzuständige Gericht für den ersten Wiener Gemeindebezirk.'},

    ];

    pdfMake.fonts = {
        Helvetica: {
            normal: 'HelveticaNeueLT-LightCond.ttf',
            bold: 'HelveticaNeue-BoldCond.ttf',
        }
    };

    function renderTermsAndConditions(termsAndCons) {
        var content = [];

        content.push(
            {
                columns: [
                    [
                        {text: termsAndCons[0].header,   style: 'terms_header'},
                        {text: termsAndCons[0].description,   style: 'terms'},
                        {text: termsAndCons[1].header,   style: 'terms_header'},
                        {text: termsAndCons[1].description,   style: 'terms'},
                        {text: termsAndCons[2].header,   style: 'terms_header'},
                        {text: termsAndCons[2].description,   style: 'terms'},
                        {text: termsAndCons[3].header,   style: 'terms_header'},
                        {text: termsAndCons[3].description,   style: 'terms'},
                    ],
                    [
                        {text: termsAndCons[4].header,   style: 'terms_header'},
                        {text: termsAndCons[4].description,   style: 'terms'},
                        {text: termsAndCons[5].header,   style: 'terms_header'},
                        {text: termsAndCons[5].description,   style: 'terms'},
                        {text: termsAndCons[6].header,   style: 'terms_header'},
                        {text: termsAndCons[6].description,   style: 'terms'},
                        {text: termsAndCons[7].header,   style: 'terms_header'},
                        {text: termsAndCons[7].description,   style: 'terms'},
                        {text: termsAndCons[8].header,   style: 'terms_header'},
                        {text: termsAndCons[8].description,   style: 'terms_bold'},
                        {text: termsAndCons[9].header,   style: 'terms_header'},
                        {text: termsAndCons[9].description,   style: 'terms'},
                        {text: termsAndCons[10].header,   style: 'terms_header'},
                        {text: termsAndCons[10].description,   style: 'terms'},
                        {text: termsAndCons[11].header,   style: 'terms_header'},
                        {text: termsAndCons[11].description,   style: 'terms'},

                    ]
                ]
            }
        );


        return content;
    }


    var dd = {

        content: [
            renderTermsAndConditions(termsAndConditions),
        ],

        images: {
            mySuperImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAABHCAIAAAAC1/wuAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATuSURBVHhe7dzhSxtnHMDx/SeKdrKVjdp1shdzloFvOljpCnaMUcYY1b2Y615MKXRv2u1daRxFYQzK+iYtcxskL3xhXixQ3FihJ1MaGQqxxc5YZdEZNI7Eyxp9EpPncpe7KPGX8/vj86q5u1zKfTWJT/JCa1MzALFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRjk6iZ4aMVK5sFsKXTlo20zju1R4wMupfa5iMEThley/uxkyvJeLT0R8Dvd1t6lDl2nsGh++NTzyaX3y2lt5WeznNdnr12WL80W/j90YGegqnV1Tb/2GJS6GE2tHjZFIrib/nY7+PB2/0nT6uH9bXSFTbTCM70eJknkavv1deaWtH721jeUttUMtsLT+83fdaa8kxDy/RkjHTjyP6g/UzEtU20zRIos8v3H8igy/vHi2v7d1bxnpW3Vb7ZNcf3jrbUjysiETzk4mH+7v0g/sUiWqbaRom0VxuJXq1eNV2DkaWTPXv+xtzKTLQWTismERz5qbxbbd2cJ8iUW0zjcdEV41gIDDkzs0vz79key+biZlJw3amZxMpyw+Hrfngx+rEjl2JJCv+Cs2/1ExUHrsXq9lk5Ir9ee43UXM17vA4d2bmcTJd4cfNxsT1VyzH9yMS1TbTeEw0Eerd29e9Gs6t64uxhfILN5MI9atb3xye1gs1M4v3R+zeVdrV0t03fH8xo+eQnRruUtscfKIlTyUcdPYFZ9Jqj8Js/3Xn7JF434hEtc00YhNtbvtsbFltvDsliVqfT5pPwn0dxX3tdXwaeqI3uvegDivRis8LPN51wyJRbTON3EQtl7tjosnIwLHy3W28ejX6r9qnMBISbTo3MrWh9lFDon5jvbxqG8dE83++czdTP1x0Orfq15/lt+jG9PA5das1Ufc/O5z2PcRE933XDYtEvY5jou6nrJkarj/La1Ez/tNHhV1I1EdI1OvUJ1Hnd3Qnp+eWy9/lLP8DJon6CIl6nfok6n6208m48fPXF9pLlgGRqI+QqNdxTNR9CWX2l+jK7B+j1RJdHvt8b5GQE8tLXCGJXvh+ZlPto4ZE/aa2y8txLxGJ7k42Gb32RvGA1kS354Lvnyi5RzsnPgjO6SsYJCT6+jcT69p5kajf+DvRXO6/P0e6X1QHrLB0IZuK3b1c5TMix9/uvxtLWfY87KULbW99MhR9qr+kYOmC79Ql0dRsNBQKu/RLoFctfPd4bi2nL14Lz+qr4krW6NotADTTa0vqjz7WWVqttM6uygLATGrF/oh7M3nnw53f4ZZEzfRqlf1tTosFgP5Tl0Q9TcYYandxL5V1fRVdUduqSRmBM4Vb67aM3uUUHo71GXiNwzJ6H/JZoid7QwtqWzWliTa39Xw3ZXnK6n2qfhjN5Rx0onwYzY+OVqL1+ki3yznIRPlIt18dtUR35L8YZTT6IObxi1FiD34ddffFKC5n34nmX0XvfjHKzcvvnCo/K587OokCDYlEAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQBwZqa/wd0Ed3Ls5EBJgAAAABJRU5ErkJggg=='
        },

        header:  function() {

            return {
                table: {
                    widths: [140, '*'],
                    body: [
                        [{image: 'mySuperImage', width: 140, height: 40, margin: [-5, 0, 0, -2], border: [false, false, false, false]},
                            {text: 'Allgemeine Geschäftsbedingungen für Domain-Bereitstellung,\n die Bereitstellung eines E-Mail-Postfaches, die SSL Zertifizierung,\n das Hosting und die Datensicherung via HEROLD File Tresor', fontSize: 11, alignment: 'right',  bold: true, margin: [0, 0, 0, 0], border: [false, false, false, false]}],
                    ]
                }, margin: [21,21]
            };
        },


        footer: function() {
            return {
                table: {
                    widths: [400, '*'],
                    body: [
                        [{text: 'HEROLD Business Data GmbH, A-2340 Mödling, Guntramsdorfer Straße 105, Telefon: +43 (0) 2236/401-38133, Fax +43 (0) 2236/401-35160 Gerichtsstand Wien, UID: ATU57270679, DVR: 0871885, FN: 233171z', fontSize: 6, alignment: 'left', margin: [0, 0, 0, 0], border: [false, false, false, false]},
                            {text: 'E-Mail: kundenservice@herold.at, www.herold.at\n AGB_Domain_Bereitstellung_V03_20160901', fontSize: 6, alignment: 'right', margin: [0, 0, 0, 0], border: [false, false, false, false]}]
                    ]
                }, margin: [21,0]
            };

        },


        // Styles
        styles: {
            terms_header: {
                bold: true,
                fontSize: 6.2,
                alignment: 'justify',
                lineHeight: 1.2
            },
            terms: {
                fontSize: 6.2,
                alignment: 'justify'
            },
            terms_bold: {
                bold: true,
                fontSize: 6.2,
                alignment: 'justify'
            },
        },

        // Default Styles
        defaultStyle: {
            columnGap: 10,
            font: 'Helvetica',
            // set font here to Proxima Nova Cond!!!
        },

        // Page margins
        pageMargins: [ 21, 70, 21, 55 ]
    };

    return dd;
}