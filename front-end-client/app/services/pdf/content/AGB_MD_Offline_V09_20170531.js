export function start() {
    var termsAndConditions = [

        /*0*/{header: '§ 1 Zustandekommen des Vertragsverhältnisses', description: 'Diese allgemeinen Geschäftsbedingungen gelten für den Zugang zum Dienst MDOnline, MDOffline und für den Bezug von Adressdaten. Ein Zugang zu MDOnline, MDOffline bzw. die Bereitstellung von Adressdaten an Verbraucher iSd Konsumentenschutzgesetzes ist ausgeschlossen. Vom Kunden unterzeichnete Bestellformulare gelten als Angebot, welches HEROLD berechtigt, innerhalb von 4 Wochen ab Unterzeichnung des Bestellformulars abzulehnen. Das Angebot gilt als von HEROLD angenommen, wenn es nicht innerhalb dieser Frist schriftlich (auch Fax und E-Mail) oder mündlich zurückgewiesen wurde. Zur Fristwahrung genügt bei mündlicher Ablehnung der Ausspruch innerhalb der Frist bzw. bei schriftlicher Ablehnung die rechtzeitige Absendung. Für die Annahme des Angebots durch HEROLD ist allein die schriftliche Bestellung laut Bestellschein maßgeblich, mündliche Erläuterungen oder Zusagen werden keinesfalls Vertragsinhalt. Auch Bestellungen, die über Internet-Seiten aufgegeben werden, gelten lediglich als Angebot zum Vertragsabschluss; die Zusendung einer Zugangsbestätigung gilt nicht als Annahme der Kundenbestellung.'},

        /*1*/{header: '\n§ 2 Vertragsgegenstand', description: 'Vertragsgegenstand ist die Einräumung des Rechts auf bestimmungsgemäße Nutzung der mittels MDOnline oder MDOffline oder auf sonstige Weise bereitgestellten Datenbank (samt den enthaltenen Daten) oder einzelner Daten (Adressdatenbezug) im Sinne des § 3 dieser Allgemeinen Geschäftsbedingungen. Bei Zugang zu MDOnline und bei MDOffline wird auch eine Software bereitgestellt; ein Anspruch des Lizenznehmers auf Lieferung des Quellcodes besteht nicht. Die Installation, Einweisung und Softwarepflege gehören nach diesem Vertrag nicht zum Leistungsumfang von HEROLD. Beim Adressdatenbezug kann die gelieferte Adressmenge aufgrund laufender Zu- und Abgänge von der in der Bestellung angegebenen Menge abweichen. In diesem Fall erhöht bzw. verringert sich der in der Bestellung angegebene Preis entsprechend. HEROLD übernimmt keine Gewähr für die Vollständigkeit einer Adressengruppe sowie die richtige Zuordnung zu einer Adressengruppe. Die Zuordnung der einzelnen Adressen erfolgt aufgrund von Feststellungen durch HEROLD oder aufgrund von Meldungen Dritter.'},

        /*2*/{header: '\n§ 3 Benutzung', description: '3.1 Nutzung von MDOnline und MDOffline Business'
        + '\n1. Allgemeines: Der Lizenznehmer erwirbt eine Einfachlizenz (Zugang für eine Person) oder eine Mehrfachlizenz für eine bestimmte Userzahl (gemäß Bestellschein, Vertrag bzw. Produktbeschreibung). Im Falle des Erwerbs einer Mehrfachlizenz dürfen immer nur höchstens so viele Zugriffsrechte vergeben werden, wie Lizenzen (gemäß Bestellschein, Vertrag bzw. Produktbeschreibung) erworben wurden. Der Lizenznehmer hat dafür zu sorgen, dass die Benutzung nur im Rahmen der erworbenen Lizenz erfolgt.'
        + '\n2. Pflichten des Lizenznehmers: Abfragen müssen unter Verwendung der zur Verfügung gestellten Benutzeroberfläche erfolgen. Der Lizenznehmer hat den Zugang zu MDOnline und MDOffline ausreichend gegen einen unbefugten Zugriff Dritter zu schützen. Die über MDOnline oder MDOfflinebereitgestellten Daten sind binnen 14 Tagen zu verwenden und danach zu löschen. Marketing- und Werbemaßnahmen dürfen daher immer nur mit aktuellen und maximal 14 Tage vor Durchführung der Marketing- und Werbemaßnahmen bezogenen Daten durchgeführt werden.'
        + '\n3. Nutzungsrechte MDOnline und MDOffline: Die über MDOnline und MDOffline bereitgestellten Daten dürfen zur Durchführung von Marketing- und Werbemaßnahmen für Zwecke des vom Lizenznehmer angegebenen Unternehmensstandortes verwendet werden. Eine Verwendung für Zwecke Dritter – somit auch für Zwecke verbundener Unternehmen oder für andere Standorte desselben Unternehmens – ist untersagt. Die Nutzung des Zugangs zu MDOnline bzw. MDOffline darf nur an und für einen Unternehmensstandort des Lizenznehmers erfolgen. Auch die Mitbenutzung des Zugangs sowie daraus bezogener Daten und erstellter oder ergänzter Datenbanken durch andere Unternehmensstandorte oder durch verbundene Unternehmen ist unzulässig. Die Verwendung an bzw. für mehrere Unternehmensstandorte ist nur durch Erwerb von Filial-Lizenzen (samt der erforderlichen Anzahl von Zugriffsrechten) zulässig. Bei Durchführung von Werbeaussendungen ist HEROLD Business Data GmbH als Auftraggeber der Ursprungsdatei anzugeben. Die Nutzung der über MDOnline oder MDOffline bezogenen Daten ist nur während der Laufzeit des Vertrages zulässig. Eine spätere Nutzung ist untersagt.'
        + '\n3.2 Nutzungsrechte beim Adressdatenbezug'
        + '\nHat sich der Kunde für die Option „Adressenmiete“ entschieden, steht ihm das Recht zu, die Adressen einmalig für eigene Marketingzwecke zu nutzen. Neben den in den §§ 3. und 4. angeführten Verboten, ist auch jede über die einmalige Nutzung hinausgehende Verwendung dieser Adressen untersagt. Die Einhaltung dieser Verwendungsvorschriften wird durch die in den Kollektionen eingearbeiteten Kontrolladressen überwacht. Zum Nachweis des Verstoßes genügt die Vorlage einer Kontrolladresse. Nach der einmaligen Verwendung sind die Daten zu löschen. Hat sich der Kunde für die Option „Adressenkauf“ entschieden, steht ihm das Recht zu, die Adressen innerhalb eines Zeitraums von 12 Monaten ab Rechnungsdatum mehrmalig für eigene Marketingzwecke unter Beachtung der in den §§ 3. und 4. angeführten Verboten zu nutzen. Die Einhaltung dieser Verwendungsvorschriften wird durch die in den Kollektionen eingearbeiteten Kontrolladressen überwacht. Zum Nachweis des Verstoßes genügt die Vorlage einer Kontrolladresse. Nach Ende des Nutzungszeitraums sind die Daten zu löschen.'
        + '\n3.3 Verwendungsbeschränkung (gilt für MDOnline und MDOffline Business sowie beim Adressdatenbezug) Neben den gesetzlichen Verboten ist bei allen HEROLD-Produkten die Verwendung im Zusammenhang mit der gewerblichen Adressenverwertung, zum Aufbau oder zur Ergänzung von Teilnehmer-, Firmen- oder anderer Datenbanken jeder Art und in jeder medialen Form (in Printform, elektronisch, auf CD-ROM, etc.), zum Aufbau kommerziell verwertbarer Firmenverzeichnisse oder anderer Verzeichnisse, zur Durchführung eines Auskunftsdienstes oder eines Call-Centers (wenn auch nur als Nebenleistung), zur Erteilung von Telefonauskünften, zum Aufbau von Konkurrenzprodukten zu HEROLD-Produkten (insbesondere HEROLD CD-ROM bzw. DVD, HEROLD Intranet-Versionen, HEROLD Einzeladressenverkauf, MDOffline, MDOnline etc.), für nicht in direktem Zusammenhang mit eigenen Werbemaßnahmen stehende Anwendungen (zum Beispiel für das Inkassowesen), generell zu Zwecken oder im Interesse Dritter und für andere kommerzielle Zwecke als für eigene Marketingzwecke verboten. Eigene Marketingzwecke sind aber auch dann verboten, wenn sie im Zusammenhang mit einer anderen verbotenen Verwertung stehen. Beim Adressdatenbezug ist die Verwendung der Adressen oder Teilen davon zum Aufbau von kommerziell verwertbaren Datenbanken oder zur Einbindung in solche Datenbanken über eigene Kunden ausschließlich für eigene (dh nicht im Auftrag oder Interesse Dritter erfolgende) Werbemaßnahmen zulässig, sofern hierdurch die Nutzungsbeschränkungen gemäß § 3.2. nicht verletzt werden.'},

        /*3*/{header: '\n§ 4 Verbot der Weitergabe an Dritte', description: 'Dem Lizenznehmer ist es untersagt, MDOnline bzw. MDOffline Dritten zugänglich zu machen oder die bereitgestellten Daten an Dritte weiterzugeben oder Dritten (auch nicht bloß vorübergehend) zu überlassen.'},

        /*4*/{header: '\n§ 5 Gewerblicher Rechtsschutz', description: 'Der Lizenznehmer anerkennt, dass die über MDOnline oder MDOffline bereitgestellte Datenbank sowie die in der Datenbank befindlichen Inhalte in all ihren Teilen urheberrechtlich geschützt sind,'},

        /*5*/{header: '', description: 'und, dass alle Urheberrechte, Leistungsschutzrechte und sonstigen gesetzlich geschützten Rechte daran HEROLD oder deren Lizenzgebern zukommen'},

        /*6*/{header: '\n§ 6 Obhutspflicht', description: 'Der Lizenznehmer hat die Zugangsdaten zu MDOnline oder MDOffline sowie die bereitgestellten Daten gegen missbräuchliche Nutzung zu sichern.'},

        /*7*/{header: '\n§ 7 Gewährleistung, Untersuchungs- und Rügepflicht', description: 'Der Zugang zu MDOnline sowie die Applikation MDOffline ist auf eine dauerhafte Benützung ausgelegt. HEROLD leistet jedoch keine Gewähr für Störungen des Zugangs zu MDOnline oder etwaiger Programmfehler, insbesondere im Falle der notwendigen Wartung. Trotz laufender Aktualisierung der Daten wird keine Gewähr geleistet, dass zum Zeitpunkt der Bereitstellung an den Lizenznehmer sämtliche Daten richtig und vollständig sind. Retouren sind unvermeidlich und stellen keinen Mangel dar. Die Frist zur Geltendmachung von Gewährleistungsbehelfen, die einvernehmlich auf Verbesserung beschränkt werden, beträgt sechs Monate ab erstmaliger Bereitstellung des Zugangs zu MDOnline bzw. MDOffline bzw ab erstmaliger Bereitstellung der Daten (Adressdatenbezug). Der Lizenznehmer hat durch zumutbare Untersuchungen feststellbare Mängel unverzüglich, längstens binnen einer Woche nach Download der Daten, schriftlich anzuzeigen.'},

        /*8*/{header: '\n§ 8 Haftung', description: 'HEROLD haftet für Schäden aufgrund einer Vertragsverletzung nur bei Vorsatz und grober Fahrlässigkeit. Sofern Schäden nicht binnen einer Frist von sechs Monaten ab Schadenseintritt geltend gemacht werden, gelten diese als verjährt. HEROLD haftet jedoch nicht – es sei denn HEROLD trifft ein vorsätzliches oder grob fahrlässiges Verschulden – für die Vollständigkeit und Richtigkeit der bereitgestellten Daten sowie für allfällige Schäden aufgrund der Unvollständigkeit oder Unrichtigkeit.'},

        /*9*/{header: '\n§ 9 Laufzeit', description: 'Bei einer Bestellung von MDOnline oder MDOffline gilt die am Bestellschein vereinbarte Mindestvertragslaufzeit. Der Vertrag kann in diesem Fall erst nach Ablauf der Mindestvertragslaufzeit und danach jährlich mit Ablauf eines jeden Vertragsjahres unter Einhaltung einer dreimonatigen Kündigungsfrist durch eingeschriebenen Brief gekündigt werden. HEROLD ist ohne Berücksichtigung der Mindestlaufzeit zur Kündigung des Abonnements unter Einhaltung einer dreimonatigen Kündigungsfrist berechtigt. Der Abonnementpreis wird entsprechend der Entwicklung des von der Statistik Österreich verlautbarten Verbraucherindex 2010 (VPI 2010) oder des an seine Stelle tretenden Index jährlich erhöht, wobei der durchschnittliche Indexwert des Kalenderjahres in dem der Auftrag erteilt wurde als Basiswert heranzuziehen ist. Darüber hinaus sind Preiserhöhungen generell bei Erhöhungen der Selbstkosten (z.B. Ansteigen der Materialkosten, Lohnkosten, Kosten der Datenbeschaffung etc.) auch während der Laufzeit des Abonnements möglich.'},

        /*10*/{header: '\n§ 10 Zahlungskonditionen, Preise, Lieferkosten', description: 'Alle Rechnungen sind binnen 14 Tagen ab Rechnungsdatum und noch vor Erbringung der vertragsgegenständlichen Leistung zur Zahlung fällig. Bei Verzug werden Mahnspesen, Inkassokosten (zB durch Inkassobüro) und Verzugszinsen in der Höhe von 12% p.a. verrechnet. Bei Zahlungsverzug erlischt jedes dem Kunden eingeräumten Nutzungsrecht. Bei Ermächtigung zum Einzug durch Lastschriften gewährt HEROLD 3% Skonto. Bei Angaben in Preislisten, Anzeigen, Werbeunterlagen, Internet-Seiten und dergleichen sind jederzeitige Änderungen ausdrücklich vorbehalten. Bestellungen, die durch unmittelbare Lieferung ohne vorangehende Auftragsbestätigung angenommen werden, werden zu den am Bestelltag geltenden Listenpreisen ausgeführt. Gegen Forderungen von HEROLD kann nicht aufgerechnet werden. Sofern die Bezahlung der Rechnungssumme in Teilbeträgen vereinbart ist, werden bei nicht fristgerechter Bezahlung auch nur eines Teilbetrages, bzw. im Falle der Einzugsermächtigung bei nicht ausreichender Kontoabdeckung, sämtliche ausständigen Teilleistungen ohne weitere Nachfristsetzung fällig. Dem Preis wird eine Servicepauschale für mobile Datenaufbereitung aufgeschlagen. Die Höhe der Servicepauschale ist umsatzabhängig und wird sowohl bei der erstmaligen Auftragserteilung, als auch bei Folgeaufträgen verrechnet. Alle Preise verstehen sich ohne Umsatzsteuer. Die angeführten Entgelte unterliegen gemäß § 2 aufgrund von Mehr- und Minderlieferungen gewissen Schwankungen.'},

        /*11*/{header: '\n§ 11 Datenschutz und unerbetene kommerzielle Kommunikation', description: 'Alle Kunden sind verpflichtet, das Datenschutzgesetz und das Telekommunikationsgesetz zu beachten und HEROLD diesbezüglich schad- und klaglos zu halten. Sofern elektronische Postadressen (E-Mail-Adressen), Telefon- und Faxnummern bereitgestellt werden, darf nicht auf eine Zustimmung des Anschlussinhabers zum Erhalt elektronischer Post, Anrufen und Faxnachrichten geschlossen werden. Insbesondere ist die bei der Rundfunk und Telekom Regulierungs-GmbH geführte Robinson-Liste zu beachten. Sofern Angebotsvorlagen für Brief, Fax oder E-Mail zur Verfügung gestellt werden sowie generell bei Durchführung von Werbeaussendungen ist sicherzustellen, dass keine Spamming-Zwecke verfolgt werden und HEROLD stets als Auftraggeber der benutzten Ursprungsdatei genannt wird. Der Kunde wird davon in Kenntnis gesetzt, dass eine Zustellung von Werbematerial durch Untersagung der Verwendung von Daten durch den Empfänger ausgeschlossen werden kann. In diesem Fall verpflichtet sich der Kunde, keine Zusendungen an den Betroffenen vorzunehmen. Der Kunde ist bei Verwendung der bereitgestellten Daten selbst für die Rechtsmäßigkeit der Datenanwendung verantwortlich.'},

        /*12*/{header: '\n§ 12 Zustimmungserklärung', description: 'Mit Auftragserteilung erklärt sich der Kunde gemäß § 8 Abs. 1 Z 2 DSG 2000 einverstanden, dass die am Bestellschein und am Datenblatt bereitgestellten Daten erfasst und für Werbe- und Marketingzwecke im Zusammenhang mit von HEROLD angebotenen Produkten und Dienstleistungen (www.herold.at) verwendet werden, dies auch über die Vertragsdauer hinaus. Der Kunde erklärt sich mit der Angabe seiner Telefonnummer und seiner elektronischen Postadresse ausdrücklich einverstanden, von HEROLD Telefonanrufe und elektronische Post zu Werbe- und Marketingzwecken, insbesondere zu Zwecken der Zusendung von Angeboten und Newsletter mit werblichen Informationen zum Unternehmen von HEROLD und Kunden von HEROLD, zu erhalten. Diese Zustimmungserklärung gilt über die vereinbarte oder tatsächliche Vertragsdauer hinaus, sie kann jedoch jederzeit durch Übermittlung eines E-Mails an kundenservice@herold.at widerrufen werden.'},

        /*13*/{header: '\n§ 13 Sonstiges', description: 'Es gilt österreichisches Recht. Verstöße gegen diese Allgemeinen Geschäftsbedingungen werden unter Ausschöpfung des Rechtsweges verfolgt. HEROLD ist berechtigt, diese Allgemeinen Geschäftsbedingungen jederzeit zu ändern. Die Allgemeinen Geschäftsbedingungen sind in der jeweils gültigen Fassung zum Zeitpunkt der Auftragserteilung (bei einem Abonnement zum Zeitpunkt der Lieferung) anwendbar. HEROLD empfiehlt daher, die Allgemeinen Geschäftsbedingungen vor jedem Vertragsabschluss oder bei jeder Lieferung des Datenträgers im Abonnement erneut zu lesen. Die allfällige Unwirksamkeit einzelner Bestimmungen dieser Allgemeinen Geschäftsbedingungen lässt die Geltung der übrigen Bestimmungen dieser Allgemeinen Geschäftsbedingungen unberührt. An die Stelle der unwirksamen Bestimmung tritt eine solche wirksame Bestimmung, die ersterer nach deren Sinn und Zweck rechtlich und wirtschaftlich am nächsten kommt. Ausschließlicher Gerichtsstand ist für beide Teile das sachlich zuständige Gericht für den ersten Wiener Gemeindebezirk.'},

    ];

    pdfMake.fonts = {
        Helvetica: {
            normal: 'HelveticaNeueLT-LightCond.ttf',
            bold: 'HelveticaNeue-BoldCond.ttf',
        }
    };

    function renderTermsAndConditions(termsAndCons) {
        var content = [];

        content.push(
            {
                columns: [
                    [
                        {text: termsAndCons[0].header,   style: 'terms_header'},
                        {text: termsAndCons[0].description,   style: 'terms'},
                        {text: termsAndCons[1].header,   style: 'terms_header'},
                        {text: termsAndCons[1].description,   style: 'terms'},
                        {text: termsAndCons[2].header,   style: 'terms_header'},
                        {text: termsAndCons[2].description,   style: 'terms'},
                        {text: termsAndCons[3].header,   style: 'terms_header'},
                        {text: termsAndCons[3].description,   style: 'terms'},
                        {text: termsAndCons[4].header,   style: 'terms_header'},
                        {text: termsAndCons[4].description,   style: 'terms'},
                    ],
                    [
                        {text: termsAndCons[5].header,   style: 'terms_header'},
                        {text: termsAndCons[5].description,   style: 'terms'},
                        {text: termsAndCons[6].header,   style: 'terms_header'},
                        {text: termsAndCons[6].description,   style: 'terms'},
                        {text: termsAndCons[7].header,   style: 'terms_header'},
                        {text: termsAndCons[7].description,   style: 'terms'},
                        {text: termsAndCons[8].header,   style: 'terms_header'},
                        {text: termsAndCons[8].description,   style: 'terms'},
                        {text: termsAndCons[9].header,   style: 'terms_header'},
                        {text: termsAndCons[9].description,   style: 'terms'},
                        {text: termsAndCons[10].header,   style: 'terms_header'},
                        {text: termsAndCons[10].description,   style: 'terms'},
                        {text: termsAndCons[11].header,   style: 'terms_header'},
                        {text: termsAndCons[11].description,   style: 'terms'},
                        {text: termsAndCons[12].header,   style: 'terms_header'},
                        {text: termsAndCons[12].description,   style: 'terms_bold'},
                        {text: termsAndCons[13].header,   style: 'terms_header'},
                        {text: termsAndCons[13].description,   style: 'terms'},
                    ]
                ]
            }
        );


        return content;
    }


    var dd = {

        content: [
            renderTermsAndConditions(termsAndConditions),
        ],

        images: {
            mySuperImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAABHCAIAAAAC1/wuAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATuSURBVHhe7dzhSxtnHMDx/SeKdrKVjdp1shdzloFvOljpCnaMUcYY1b2Y615MKXRv2u1daRxFYQzK+iYtcxskL3xhXixQ3FihJ1MaGQqxxc5YZdEZNI7Eyxp9EpPncpe7KPGX8/vj86q5u1zKfTWJT/JCa1MzALFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRjk6iZ4aMVK5sFsKXTlo20zju1R4wMupfa5iMEThley/uxkyvJeLT0R8Dvd1t6lDl2nsGh++NTzyaX3y2lt5WeznNdnr12WL80W/j90YGegqnV1Tb/2GJS6GE2tHjZFIrib/nY7+PB2/0nT6uH9bXSFTbTCM70eJknkavv1deaWtH721jeUttUMtsLT+83fdaa8kxDy/RkjHTjyP6g/UzEtU20zRIos8v3H8igy/vHi2v7d1bxnpW3Vb7ZNcf3jrbUjysiETzk4mH+7v0g/sUiWqbaRom0VxuJXq1eNV2DkaWTPXv+xtzKTLQWTismERz5qbxbbd2cJ8iUW0zjcdEV41gIDDkzs0vz79key+biZlJw3amZxMpyw+Hrfngx+rEjl2JJCv+Cs2/1ExUHrsXq9lk5Ir9ee43UXM17vA4d2bmcTJd4cfNxsT1VyzH9yMS1TbTeEw0Eerd29e9Gs6t64uxhfILN5MI9atb3xye1gs1M4v3R+zeVdrV0t03fH8xo+eQnRruUtscfKIlTyUcdPYFZ9Jqj8Js/3Xn7JF434hEtc00YhNtbvtsbFltvDsliVqfT5pPwn0dxX3tdXwaeqI3uvegDivRis8LPN51wyJRbTON3EQtl7tjosnIwLHy3W28ejX6r9qnMBISbTo3MrWh9lFDon5jvbxqG8dE83++czdTP1x0Orfq15/lt+jG9PA5das1Ufc/O5z2PcRE933XDYtEvY5jou6nrJkarj/La1Ez/tNHhV1I1EdI1OvUJ1Hnd3Qnp+eWy9/lLP8DJon6CIl6nfok6n6208m48fPXF9pLlgGRqI+QqNdxTNR9CWX2l+jK7B+j1RJdHvt8b5GQE8tLXCGJXvh+ZlPto4ZE/aa2y8txLxGJ7k42Gb32RvGA1kS354Lvnyi5RzsnPgjO6SsYJCT6+jcT69p5kajf+DvRXO6/P0e6X1QHrLB0IZuK3b1c5TMix9/uvxtLWfY87KULbW99MhR9qr+kYOmC79Ql0dRsNBQKu/RLoFctfPd4bi2nL14Lz+qr4krW6NotADTTa0vqjz7WWVqttM6uygLATGrF/oh7M3nnw53f4ZZEzfRqlf1tTosFgP5Tl0Q9TcYYandxL5V1fRVdUduqSRmBM4Vb67aM3uUUHo71GXiNwzJ6H/JZoid7QwtqWzWliTa39Xw3ZXnK6n2qfhjN5Rx0onwYzY+OVqL1+ki3yznIRPlIt18dtUR35L8YZTT6IObxi1FiD34ddffFKC5n34nmX0XvfjHKzcvvnCo/K587OokCDYlEAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQBwZqa/wd0Ed3Ls5EBJgAAAABJRU5ErkJggg=='
        },

        header:  function() {

            return {
                table: {
                    widths: [400, '*'],
                    body: [
                        [{text: 'Allgemeine Geschäftsbedingungen für Marketing Daten Business',bold: true, width: 140, height: 40, margin: [-5, 0, 0, -2], border: [false, false, false, true]},
                            {text: '', fontSize: 11, alignment: 'right',  bold: true, margin: [0, 0, 0, 0], border: [false, false, false, true]}],
                    ]
                }, margin: [21,21]
            };
        },


        footer: function() {
            return {
                table: {
                    widths: [400, '*'],
                    body: [
                        [{text: 'HEROLD Business Data GmbH, A-2340 Mödling, Guntramsdorfer Straße 105, Telefon: +43 (0) 2236/401-38133, Fax +43 (0) 2236/401-35160 Gerichtsstand Wien, UID: ATU57270679, DVR: 0871885, FN: 233171z', fontSize: 6, alignment: 'left', margin: [0, 0, 0, 0], border: [false, true, false, false]},
                            {text: 'E-Mail: kundenservice@herold.at, www.herold.at\n AGB_MD_Online_Business_V09_20170531', fontSize: 6, alignment: 'right', margin: [0, 0, 0, 0], border: [false, true, false, false]}]
                    ]
                }, margin: [21,0]
            };

        },


        // Styles
        styles: {
            terms_header: {
                bold: true,
                fontSize: 7.1,
                alignment: 'justify',
                lineHeight: 1.1
            },
            terms: {
                fontSize: 7.1,
                alignment: 'justify'
            },
            terms_bold: {
                bold: true,
                fontSize: 7.1,
                alignment: 'justify'
            },
        },

        // Default Styles
        defaultStyle: {
            columnGap: 10,
            font: 'Helvetica',
            // set font here to Proxima Nowa Cond!!!
        },

        // Page margins
        pageMargins: [ 21, 50, 21, 35 ]
    };

    return dd;
}