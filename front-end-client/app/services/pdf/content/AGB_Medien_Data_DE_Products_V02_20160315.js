export function start() {
    var termsAndConditions = [
        /*0*/{header: '1. Geltungsbereich und Vertragsgegenstand', description: '1.1 Diese Allgemeinen Geschäftsbedingungen gelten für alle Lieferungen und Leistungen, die HEROLD gegenüber einem Unternehmer, einer juristischen Person des öffentlichen Rechts oder einem öffentlich-rechtlichen Sondervermögen erbringt, mit Ausnahme der Bereitstellung des Zugangs zur CMS-Applikation, von Website Produkten einschließlich Online-Shop, Facebook-Fanpage, Hosting und damit verwandte Produkte.'
        + '\n1.2 Abweichende oder ergänzende Bedingungen des Kunden gelten nicht. Das gilt auch, wenn den entgegenstehenden Bedingungen nicht ausdrücklich von HEROLD widersprochen wurde.'
        + '\n1.3 Diese Allgemeinen Geschäftsbedingungen gelten in ihrer jeweiligen Fassung auch ohne ausdrücklichen Hinweis auf ihre Einbeziehung für alle zukünftigen Geschäfte mit dem Kunden.'},

        /*1*/{header: '', description: 'Hat der Kunde Kenntnis von einem Missbrauch der Zugangsdaten, hat er HEROLD unverzüglich schriftlich oder per E-Mail zu informieren. HEROLD kann in diesem Fall den Zugang sperren. HEROLD ist berechtigt, im Falle des Verdachts des Missbrauchs der Zugangsdaten oder einer vereinbarungswidrigen Verwendung, den Zugang unverzüglich und ohne vorherige Mitteilung zu sperren.'
        + '\n4.2 Der Kunde hat die technischen Voraussetzungen in seinem Bereich für den Zugang zu schaffen und aufrechtzuerhalten. HEROLD wird den Kunden auf Anfrage über die technischen Voraussetzungen informieren.'
        + '\n4.3 Der Online-Zugang steht grundsätzlich sieben Tage pro Woche jeweils 24 Stunden pro Tag zur Verfügung.'
        + '\n4.4 Ausnahmsweise kann der Zugang unterbrochen sein:'
        + '\n• durch Störungen des Internet oder durch sonstige von HEROLD nicht zu vertretende Umstände, die insbesondere auf höherer Gewalt beruhen;'
        + '\n• durch Wartungs- und Instandsetzungsarbeiten. HEROLD wird sie regelmäßig außerhalb der üblichen Geschäftszeiten (8.00 Uhr bis 17.00 Uhr) vornehmen. Bei akuten Notwendigkeiten der Wartung innerhalb dieser Zeiten wird HEROLD sich bemühen, den Kunden online zu informieren.'
        + '\n4.5 HEROLD behält sich Änderungen des Systems zur Anpassung an den Stand der Technik oder aus anderen Gründen vor.'
        + '\n4.6 Soweit für die Durchführung erforderlich, hat der Kunde den Mitarbeitern von HEROLD oder von HEROLD beauftragte Dritten Zugang zum Server oder auf die Website des Kunden zu gewähren.'
        + '\n4.7 Weist die Leistung von HEROLD Schnittstellen zu Leistungen von Dritten auf, hat der Kunde diese Fremdleistungen gesondert in Auftrag zu geben. HEROLD übernimmt dafür keine Verantwortung. HEROLD wird den Kunden über die notwendigen Fremdleistungen informieren.'
        + '\n4.8 Bei HEROLD Online Marketing Assistent handelt es sich um ein online-basiertes Software-Tool, mit dem der Kunde entweder selbst („Do it yourself“) Daten, Bilder und Texte (nachfolgend „Inhalte“) auf mehreren, von HEROLD vorgegebenen Plattformen im Internet einstellen oder HEROLD damit beauftragen kann („We do it for you“).'
        + '\n4.8.1 Der Kunde erwirbt ein nicht ausschließliches und nicht übertragbares, zeitlich auf die Dauer des Vertragsverhältnisses mit HEROLD befristetes Recht zum Zugang zu den Leistungen der HEROLD Online Marketing Assistent Software. Der Kunde hat die Vorgaben des Systems bei der Dateneingabe zu beachten. Den Umfang der Platzierung gibt der jeweilige Plattformbetreiber vor. Für Änderungen bei den einzelnen Plattformbetreibern übernimmt HEROLD keine Verantwortung. Gleiches gilt für Löschungen der Inhalte auf Plattformen nach Beendigung des Vertragsverhältnisses mit HEROLD.'
        + '\n4.8.2 Es ist dem Kunden untersagt, ohne vorherige schriftliche Zustimmung von HEROLD, Inhalte Dritter über die Software auf Plattformen zu veröffentlichen. Für die Nutzung einzelner Plattformen, z.B. von Facebook, benötigt der Kunde ein eigenes Konto. Sofern der Kunde HEROLD mit der Einstellung und/oder mit Änderungen von Inhalten auf Plattformen beauftragt, hat der Kunde für die Zugangsmöglichkeit von HEROLD zu sorgen.'
        + '\n4.8.3 Der Kunde trägt allein die Verantwortung für die Zulässigkeit der platzierten Inhalte unter allen rechtlichen Gesichtspunkten. Eine Überwachung oder Überprüfung der Inhalte durch HEROLD findet nicht statt. Abweichend zu Ziffer 12 räumt der Kunde HEROLD das Recht ein, die Inhalte frei zu verwenden, einschließlich der Weitergabe an Dritte.'
        + '\n4.8.4 Rechteinhaber ist Yext Inc. mit Sitz in New York / USA. Yext Inc. haftet gegenüber dem Kunden weder auf Schadensersatz noch wegen Gewährleistungsansprüchen. Yext Inc. gilt als Drittbegünstigter und kann Ansprüche wegen einer Verletzung der Lizenzrechte auch im eigenen Namen gegen den Kunden geltend machen.'},

        /*2*/{header: '\n2. Vertragsschluss, Pflichten des Kunden, Leistungen von HEROLD', description: '2.1 Angebote von HEROLD sind freibleibend und unverbindlich.'
        + '\n2.2 Ein vom Kunden unterzeichnetes Bestellformular gilt als Angebot des Kunden. Unterschriften von HEROLD Mitarbeitern auf Bestellformularen bestätigen nur den Empfang. Bestellt der Kunde online oder telefonisch, gilt eine Bestellbestätigung von HEROLD nicht als Annahme des Angebots.'
        + '\n2.3 Der Kunde ist an das von ihm abgegebene Angebot 14 Tage gebunden. Das Angebot gilt als angenommen, wenn es von HEROLD nicht innerhalb von 14 Tagen ab Unterzeichnung des Bestellformulars durch den Kunden abgelehnt wird. HEROLD kann das Angebot des Kunden schriftlich, per Telefax, per E-Mail oder mündlich ablehnen.'
        + '\n2.4 Mündliche Nebenabreden und Zusagen sind ohne schriftliche Bestätigung von HEROLD unwirksam.'
        + '\n2.5 Solange der Kunde trotz Aufforderung von HEROLD seinen Mitwirkungspflichten nicht nachkommt, ist HEROLD von der Leistungspflicht befreit.'
        + '\n2.6 Der Kunde garantiert und trägt allein die Verantwortung für die Zulässigkeit der von ihm jeweils bereitgestellten Inhalte unter allen rechtlichen Gesichtspunkten sowie für deren Richtigkeit und Vollständigkeit. HEROLD muss diese nicht prüfen oder überwachen. Entsteht HEROLD wegen Aktionen des Kunden oder durch die Veröffentlichung von rechtswidrigen oder die Rechte Dritter verletzende Inhalten, die der Kunde zur Verfügung gestellt hat, ein Schaden, hat der Kunde HEROLD den Schaden zu ersetzen. Werden von Dritten wegen Aktionen des Kunden oder wegen vom Kunden zur Verfügung gestellte oder veröffentlichte Inhalte Ansprüche gegen HEROLD geltend gemacht, hat der Kunde HEROLD von diesen Ansprüchen freizustellen und die zur Rechtsverfolgung angefallenen notwendigen Kosten zu erstatten. HEROLD wird den Kunden über eine solche Inanspruchnahme umgehend informieren, um ihm die Regulierung zu ermöglichen. HEROLD ist berechtigt, in solchen Fällen mit dem Dritten einen Vergleich abzuschließen oder den Anspruch anzuerkennen.'
        + '\n2.7 HEROLD ist berechtigt, Subunternehmer einzuschalten.'},

        /*3*/{header: '\n3. Nutzung von Programmen und Daten', description: '3.1 Zur Nutzung von Programmen und Daten von HEROLD wird dem Kunden mit vollständiger Bezahlung der in Rechnung gestellten ersten Vergütung das einfache, nicht ausschließliche Recht eingeräumt, und zwar nur für eigene Zwecke und nur an einem beim Kunden eingerichteten Arbeitsplatz.'
        + '\n3.2 Programme oder Daten dürfen nicht vervielfältigt, verändert, oder Dritten in irgendeiner Form zugänglich gemacht werden. Zulässig ist die Verwendung der Daten zum Aufbau und zur Ergänzung von eigenen Datenbanken des Kunden, sofern diese Datenbanken ausschließlich für eigene gesetzlich zulässige Werbemaßnahmen des angegebenen Unternehmensstandortes verwendet werden. Geschieht dies durch Dritte, hat der Kunde die Geheimhaltung der Daten sowie deren Löschung oder Rückgabe nach Abschluss der Maßnahme durch den Dritten zu sichern. Spamming mit Daten und Programmen ist nicht erlaubt.'
        + '\n3.4 Ein Anspruch des Kunden auf Lieferung des Quellcodes besteht nicht. Eine Installation, soweit erforderlich, ist Sache des Kunden.'
        + '\n3.5 Der Kunde darf Datenträger an Dritte nicht weitergeben. Bei der Nutzung von Daten wird der Kunde datenschutzrechtliche Bestimmungen beachten; HEROLD gewährleistet nicht, dass Dritte zustimmen, vom Kunden angesprochen zu werden.'
        + '\n3.6 Der Kunde hat Programme, Daten und Zugangscodes vor unbefugtem Zugriff Dritter angemessen zu sichern und auch seine Mitarbeiter zur Geheimhaltung zu verpflichten. Erfährt der Kunde, dass Dritte Zugriff auf die Daten nehmen, hat er HEROLD unverzüglich zu informieren. Hat der Kunde den unbefugten Zugriff nicht zu vertreten, wird er von HEROLD unverzüglich einen neuen Zugang erhalten.'
        + '\n3.7 HEROLD hat technisch die Möglichkeit, den Zugriff auf die Daten zu sperren. HEROLD ist berechtigt, bei einem konkreten Verdacht auf Missbrauch des Datenträgers oder der Zugangsdaten den Datenzugriff ohne vorige Ankündigung zu sperren.'
        + '\n3.8 Soweit erforderlich, sind Benutzerhandbücher als pdf verfügbar.'
        + '\n3.9 Urheberrechtsvermerke dürfen nicht entfernt oder geändert werden.'},

        /*4*/{header: '\n4. Online-Services (z.B. MD Online, Online Buchungs-Tool oder HEROLD Online Marketing Assistent)', description: '4.1 Der Kunde erhält Zugang passwortgeschützt durch die ihm von HEROLD per E-Mail bekanntgegebenen Zugangsdaten. Abfragen müssen über die von HEROLD zur Verfügung gestellte Benutzeroberfläche erfolgen. Der Kunde darf die Zugangsdaten nur für die Dauer des Vertragsverhältnisses mit HEROLD und zu eigenen Zwecken nutzen. Der Kunde hat seine Zugangsdaten sorgfältig zu verwahren und vor einem unberechtigten Zugriff Dritter zu schützen.'},

        /*5*/{header: '\n5. Platzierung von Werbung, Newsletter', description: '5.1 Soweit HEROLD Werbung für den Kunden in Internetportalen, Websites, mobilen Websites und Suchmaschinen platziert, wählt HEROLD, sofern mit dem Kunden nicht anders schriftlich vereinbart, die Platzierung selbst aus. HEROLD legt für die Platzierung in Suchmaschinen Suchworte fest. Der Kunde hat die für die Werbung von HEROLD angeforderten Inhalte, insbesondere Texte und Bilder, bereitzustellen. Sollte der Kunde trotz Aufforderung keine Inhalte bereitstellen, können branchentypische Texte und Bilder von HEROLD für die Werbung verwendet werden. Die Format-Auswahl erfolgt durch den Kunden anhand von Formatvorlagen, die nicht abgeändert werden können. Vor der Freischaltung der Werbung wird der Kunde von der Fertigstellung informiert und aufgefordert, Änderungswünsche innerhalb einer von HEROLD gesetzten Frist bekannt zu geben. Sofern keine Änderungswünsche fristgerecht bekannt gegeben werden, ist HEROLD zur Freischaltung der Werbung berechtigt. Soweit Fremdkosten für Platzierungen innerhalb eines Auftrages ansteigen, hat der Kunde HEROLD die Differenz zu erstatten. Wird ein Budget während der Vertragslaufzeit nicht ausgeschöpft, kann HEROLD nicht verbrauchte Beträge bis zu deren Ausschöpfung für weitere Platzierungen verwenden. Wird von HEROLD Werbung für den Kunden auf Websites Dritter geschaltet, gelten ergänzend die Bedingungen des jeweiligen Betreibers. Auf Wunsch wird HEROLD dem Kunden diese Bedingungen zur Verfügung stellen.'
        + '\n5.2 Ist Kunde eine Werbeagentur, hat er HEROLD den Werbetreibenden namentlich zu nennen. HEROLD ist berechtigt, von der Werbeagentur einen Nachweis über ihre Beauftragung zu verlangen.'
        + '\n5.3 Die Urheberrechte an der Werbung stehen hinsichtlich Gestaltung, Aufbau, Design und Inhalten HEROLD zu. Unter „Inhalten“ sind in diesem Zusammenhang Bilder, Texte und Graphiken, welche von HEROLD bei Erstellung der Werbung bereitgestellt werden, zu verstehen.'},

        /*6*/{header: '', description: '5.4 HEROLD ist berechtigt, den vom Kunden bereitgestellten Werbeinhalt insgesamt oder teilweise zurückzuweisen und auf (mobilen) Websites, Internetportalen sowie Suchmaschinen zu entfernen, wenn konkrete Anhaltspunkte bestehen, dass der Inhalt rechts- oder sittenwidrig ist, Rechte Dritter verletzt oder eine Veröffentlichung aus technischen Gründen nicht möglich ist.'
        + '\n5.5 Die Werbung wird entsprechend der vertraglichen Vereinbarung, ansonsten nach billigem Ermessen von HEROLD unter Berücksichtigung der Interessen des Kunden platziert. HEROLD ist während der Vertragslaufzeit jederzeit berechtigt, die Platzierung der Werbung zu ändern, wenn durch diese Umplatzierung die Werbewirkung nicht wesentlich beeinträchtigt wird.'
        + '\n5.6 Wird von HEROLD im Rahmen des „Begleiterpakets“ online Werbung für den Kunden auf dritten Websites ausgeliefert, darf HEROLD auf der Website des Kunden Cookies platzieren, die eine Erkennbarkeit der Besucher ermöglichen. Der Kunde erhält einen Endbericht über die veröffentlichte Werbung, der die Sichtkontakte pro Tag enthält.'
        + '\n5.7 Besteht die Aufgabe von HEROLD im Versand von Newsletters, stellt der Kunde die Inhalte bereit und garantiert die datenschutzrechtliche Zulässigkeit des Versands. Er sorgt auch für die rechtzeitige Löschung von Adressen, falls ein Adressat keine Sendungen wünscht oder andere Gründe dies gebieten.'},

        /*7*/{header: '', description: '8.4 Zum Zeitpunkt der Beendigung des Vertragsverhältnisses erlischt die Zugangs- und Nutzungsberechtigung des Kunden, auch für bereits exportierte Datensätze. Der Zugang des Kunden zu Programmen und Daten, auch auf gelieferte Datenträger, wird sofort gesperrt. Er hat sämtliche gespeicherten Daten, egal in welcher Form, nachweislich zu löschen und daraus gezogene Daten in Papierform zu vernichten. '
        + '\n8.5 Erhält der Kunde vertragsgemäß neue Datenträger, hat er die alten binnen 14 Tagen an die von HEROLD angegebene Adresse zurückzusenden. Unterbleibt dies trotz einer Mahnung, kann HEROLD eine Version zusätzlich berechnen.'},

        /*8*/{header: '\n6. Eigentumsvorbehalt, Provider und Domains', description: '6.1  HEROLD  behält  sich  das  Eigentum  an  gelieferten  Waren,  auch  Datenträgern,  bis  zur vollständigen Zahlung des vereinbarten Preises vor.'
        + '\n6.2 HEROLD behält sämtliche Rechte an den im Auftrag des Kunden registrierten Domains. Nur mit Zustimmung von HEROLD dürfen diese auf den Kunden übertragen werden. Wechselt der Kunde den Provider, hat er HEROLD die Möglichkeit zu verschaffen, die Domain zu übernehmen. Solange das nicht geschieht, hat HEROLD keine Leistungspflicht.'},

        /*9*/{header: '\n7. Gewährleistung und Haftung', description: '7.1 Die gesetzliche Untersuchungs- und Rügepflicht nach § 377 HGB wird in Bezug auf Datenträger wie folgt konkretisiert: Der Kunde hat Datenträger von HEROLD unverzüglich nach Erhalt zu untersuchen, insbesondere alle Funktionen einer Prüfung zu unterziehen. Mängel, die hierbei festgestellt werden oder feststellbar sind, müssen HEROLD unverzüglich, spätestens innerhalb von drei Werktagen ab Erhalt, schriftlich unter detaillierter Beschreibung des Mangels angezeigt werden. Mängel, die im Rahmen der ordnungsgemäßen Untersuchung nicht feststellbar sind, müssen unverzüglich, spätestens innerhalb von drei Werktagen, nach Entdeckung unter detaillierter Beschreibung des Mangels schriftlich angezeigt werden.'
        + '\n7.2 Ist eine Lieferung oder Leistung von HEROLD mangelhaft, darf HEROLD nach eigener Wahl den Mangel beseitigen oder eine mangelfreie Sache liefern.'
        + '\n7.3 Für die Richtigkeit, Aktualität und Vollständigkeit der Daten sowie für die rechtliche Zulässigkeit ihrer Verwendung übernimmt HEROLD keine Gewähr.'
        + '\n7.4 HEROLD haftet nicht für Schäden, die auf Störungen der Internetverbindung, an Netzwerken, Leitungen, Servern und sonstigen Systemen und Einrichtungen zurückzuführen sind, die nicht im Verantwortungsbereich von HEROLD liegen. HEROLD übernimmt keine Haftung für die Kompatibilität der technischen Einrichtungen beim Kunden und hat diesbezüglich keine Warn- und Prüfpflicht. Auch für unsachgemäße Handhabung von Lieferungen und Leistungen durch den Kunden haftet HEROLD nicht. HEROLD haftet weiter nicht für Aktionen oder Inhalte des Kunden. Ebenso wenig haftet HEROLD für Fälle höherer Gewalt und die hierdurch hervorgerufenen Leistungs- und/oder Stromausfälle.'
        + '\n7.5 HEROLD haftet nur für Schäden, die von HEROLD vorsätzlich, grob fahrlässig oder in Verletzung wesentlicher Vertragspflichten leicht fahrlässig verursacht wurden. Sonst ist die Haftung von HEROLD auf Schadensersatz ausgeschlossen. Der Schadensersatz für die Verletzung wesentlicher Vertragspflichten ist in Fällen leichter Fahrlässigkeit auf den vertragstypischen und vorhersehbaren Schaden begrenzt. Diese Beschränkungen gelten nicht für Schadensersatzansprüche nach dem Produkthaftungsgesetz und wegen der Verletzung von Leben, Körper oder Gesundheit.'
        + '\n7.6 Soweit nach diesen Bedingungen die Haftung ausgeschlossen oder beschränkt ist, gilt dies auch für die Haftung der gesetzlichen Vertreter von HEROLD sowie für die Haftung ihrer Mitarbeiter, Erfüllungs- und Verrichtungsgehilfen.'
        + '\n7.7 Gewährleistungs- und Schadensersatzansprüche verjähren mit Ablauf von 12 Monaten ab ihrer Entstehung. Bei vorsätzlicher oder grob fahrlässiger Pflichtverletzung, bei arglistigem Verhalten, bei Verletzung von Leben, Körper oder Gesundheit sowie bei Ansprüchen nach dem Produkthaftungsgesetz gelten die gesetzlichen Fristen.'},

        /*10*/{header: '\n8. Laufzeit und Vertragsbeendigung', description: '8.1 Die Vertragslaufzeit ergibt sich aus dem Bestellformular. Bei einer Befristung endet das Vertragsverhältnis zwischen dem Kunden und HEROLD automatisch, ohne dass es einer gesonderten Kündigung bedarf. Ist das Vertragsverhältnis unbefristet, kann es unter Einhaltung einer Kündigungsfrist von drei Monaten zum Ende eines jeden Vertragsjahres, erstmals zum Ende der vereinbarten Mindestvertragslaufzeit, gekündigt werden.'
        + '\n8.2 Das beiderseitige Recht zur Kündigung aus wichtigem Grund bleibt unberührt. Ein wichtiger Grund liegt für HEROLD insbesondere vor, wenn der Kunde länger als zwei Monate mit Zahlungen im Rückstand ist, der Kunde gegen eine wesentliche Bestimmung des Vertrages verstößt, z.B. Geheimhaltungsbestimmungen verletzt, über das Vermögen des Kunden das Insolvenzverfahren eröffnet oder die Eröffnung mangels Masse abgelehnt wird.'
        + '\n8.3 Jede Kündigung bedarf der Schriftform.'},


        /*11*/{header: '\n9. Zahlungskonditionen, Preise, Lieferkosten', description: '9.1 Es gelten die im Bestellformular angeführten Preise. Zu allen Preisen ist die zum Zeitpunkt des Entstehens der Steuerschuld geltende gesetzliche Umsatzsteuer zu zahlen wenn solche anfällt.'
        + '\n9.2 Der Kunde ist vorleistungspflichtig. Der Rechnungsbetrag ist innerhalb von 14 Tagen ab Rechnungsdatum zu zahlen. Danach kommt der Kunde in Zahlungsverzug, ohne dass es einer Mahnung von HEROLD bedarf. Bei Verzug sind Mahnspesen und Verzugszinsen in Höhe von 12% p.a. zu zahlen.'
        + '\n9.3 Ist die Rechnungssumme in Teilbeträgen zu bezahlen, werden bei nicht fristgerechter Bezahlung auch nur eines Teilbetrages, im Falle der Einzugsermächtigung bei nicht ausreichender Kontodeckung, sämtliche Zahlungsansprüche ohne weitere Nachfristsetzung sofort fällig.'
        + '\n9.4 Ändert sich die Kalkulationsgrundlage von HEROLD, kann HEROLD die Preise angemessen für Lieferungen und Leistungen erhöhen, die später als vier Monate nach Vertragsabschluss erbracht werden. Handelt es sich um ein Dauerschuldverhältnis, ist die Erhöhung dem Kunden mindestens 2 Monate vor deren Wirksamwerden schriftlich oder per E-Mail bekanntzugeben und es darf höchstens einmal pro Kalenderjahr erhöht werden. Übersteigt die Erhöhung in solchen Fällen 5 %, kann der Kunde schriftlich oder per E-Mail innerhalb von 6 Wochen nach Erhalt der Erhöhungsnachricht zu dem Zeitpunkt kündigen, an dem die Erhöhung wirksam wird.'
        + '\n9.5 Andere, nicht im Bestellformular preislich ausgewiesene Dienstleistungen, wie Programmierung, Schulung, Beratung, etc. werden dem Kunden nach dem tatsächlichen Zeitaufwand zu dem am Tag der Leistungserbringung gültigen Stundensatz von HEROLD in Rechnung gestellt.'},

        /*12*/{header: '\n10. Aufrechnung, Zurückbehaltung, Abtretung', description: '10.1 Der Kunde darf nur mit von HEROLD anerkannten oder rechtskräftig festgestellten Forderungen aufrechnen oder Zahlungen zurückhalten.'
        + '\n10.2 Der Kunde darf ohne vorherige schriftliche Zustimmung von HEROLD Rechte aus dem Vertragsverhältnis nicht an Dritte abtreten.'},

        /*13*/{header: '\n11. Personenbezogene Daten, Datenschutz', description: '11.1 Personenbezogene Bestands- und Nutzungsdaten des Kunden selbst werden, sofern nicht anders vereinbart, nur im Rahmen der Zweckbestimmung des Vertragsverhältnisses erhoben, verarbeitet und genutzt. Diese Daten werden vertraulich behandelt, ausschließlich zum Zweck der Vertragserfüllung genutzt und nicht an Dritte weitergegeben, ausgenommen an Partnerfirmen, auch im EU-Ausland, die die Daten zur Abwicklung der Bestellung und zur Vertragserfüllung benötigen. Dem Kunden wird auf Anfrage Auskunft über die über ihn gespeicherten Daten erteilt.'
        + '\n11.2 Soweit im Rahmen des Hosting HEROLD auf personenbezogene Daten beim Kunden Zugriff hat, gelten für HEROLD folgende Pflichten:'
        + '\n11.2.1 Der Eingriff in Daten des Kunden und deren Kenntnisnahme ist HEROLD nur gestattet, soweit dies nötig ist, um die vereinbarten Hostingleistungen zu erbringen.'
        + '\n11.2.2 HEROLD darf Daten des Kunden nur soweit verändern, als dies zu einer Problembehebung erforderlich ist. Etwaige Veränderungen bedürfen der vorherigen schriftlichen Zustimmung des Kunden. HEROLD darf die Daten nicht ohne Zustimmung des Kunden sperren, ist jedoch zu einer solchen Sperrung auf Weisung des Kunden verpflichtet.'
        + '\n11.2.3 HEROLD trifft folgende Maßnahmen:'
        + '\n• HEROLD hat seine Mitarbeiter auf das Datengeheimnis zu verpflichten und über die Folgen von Verstößen zu informieren.'
        + '\n• HEROLD hat einen Datenschutzbeauftragten bestellt.'
        + '\n• HEROLD stellt sicher, dass nur von HEROLD eigens bestimmte Mitarbeiter Zugang zu den Daten haben und dokumentiert deren Bestimmung.'
        + '\n11.2.4 Die Maßnahmen nach 11.2.3 reichen für den Zeitraum aus, da HEROLD die technische Beratung des Hosting durch einen Subunternehmer besorgen lässt. Ändert sich dies, sind die Maßnahmen einvernehmlich neu festzulegen.'
        + '\n11.2.5 HEROLD wird Weisungen des Kunden beachten. Das Weisungsrecht des Kunden erstreckt sich auf alle Maßnahmen, die der Erfüllung datenschutzrechtlicher Anforderungen dienen.'
        + '\n11.2.6 HEROLD stellt dem Kunden auf Anforderung die Angaben nach § 4g Abs. 2 S.1 BDSG zur Verfügung.'
        + '\n11.2.7 HEROLD unterrichtet den Kunden unverzüglich bei schwerwiegenden Störungen des Betriebsablaufes, bei Verdacht auf Datenschutzverletzung oder anderer Unregelmäßigkeiten bei der Verarbeitung der Daten des Kunden.'
        + '\n11.2.8 HEROLD bestätigt, dass ihm die einschlägigen datenschutzrechtlichen Vorschriften'},

        /*14*/{header: '', description: 'bekannt sind. Er verpflichtet sich, bei einer auftragsgemäßen Verarbeitung der personenbezogenen Daten des Kunden das Datengeheimnis zu wahren. HEROLD stellt sicher, dass die mit der Verarbeitung der Daten des Kunden befassten Mitarbeiter in die Schutzbestimmungen des Bundesdatenschutzgesetzes eingewiesen worden sind. Auskünfte an Dritte oder den Betroffenen darf HEROLD nur nach vorheriger schriftlicher Zustimmung durch den Kunden erteilen.'
        + '\n11.2.9 Ist der Kunde aufgrund geltender Datenschutzgesetze gegenüber einer Einzelperson verpflichtet, Auskünfte zur Erhebung, Verarbeitung oder Nutzung von Daten dieser Person zu geben, wird HEROLD nach schriftlicher Aufforderung den Kunden dabei unterstützen, diese Informationen bereit zu stellen.'
        + '\n11.2.10 Der Kunde kann sich nach Anmeldung zu Prüfzwecken in den Betriebsstätten zu den üblichen Geschäftszeiten ohne Störung des Betriebsablaufes von der Angemessenheit der Maßnahmen zur Einhaltung der technischen und organisatorischen Erfordernisse der für die Auftragsdatenverarbeitung einschlägigen Datenschutzgesetze überzeugen. Der Kunde kann sich hierzu sachverständiger Dritter bedienen. HEROLD ist verpflichtet, solche Kontrollen in jeder Beziehung zu dulden und zu unterstützen.'
        + '\n11.2.11 Bei Beendigung des Hostings darf HEROLD Daten nicht länger als ein Jahr auf Rechnern halten, sondern hat sie zu löschen.'
        + '\n11.3 Den Kunden treffen folgende Pflichten:'},

        /*15*/{header: '', description: '11.3.1 Die Pflicht zur Führung des öffentlichen Verfahrensverzeichnisses gem. § 4g Abs.2 S.2 BDSG liegt beim Kunden. Der Kunde hat auch die Benachrichtigungs- und Auskunftsansprüche und die Ansprüche von Betroffenen auf Löschung und Sperrung von personenbezogenen Daten zu erfüllen.'
        + '\n11.3.2 Der Kunde ist verpflichtet, HEROLD über etwaige Mängel, die der Kunde beim Umgang mit personenbezogenen Daten nach diesem Vertrag beobachtet, unverzüglich und vollständig zu unterrichten.'
        + '\n11.4 HEROLD ist berechtigt, Subunternehmer im In- und Ausland einzuschalten. Der Kunde ist damit einverstanden, dass sämtliche zur Vertragserfüllung erforderlichen Informationen und personenbezogenen Daten zu diesem Zweck an den Subunternehmer übermittelt werden.'},

        /*16*/{header: '\n12. Rechtswahl, Gerichtsstand', description: '12.1 Erfüllungsort und Gerichtsstand ist München. HEROLD ist berechtigt, den Kunden auch an seinem Sitz zu verklagen.'
        + '\n12.2 Für sämtliche Rechtsstreitigkeiten aus oder im Zusammenhang mit dem Vertragsverhältnis zwischen HEROLD und dem Kunden gilt das Recht der Bundesrepublik Deutschland unter Ausschluss der Verweisungsnormen des internationalen Privatrechts und unter Ausschluss des UN-Kaufrechts.'},

    ];

    pdfMake.fonts = {
        Helvetica: {
            normal: 'HelveticaNeueLT-LightCond.ttf',
            bold: 'HelveticaNeue-BoldCond.ttf',
        }
    };

    function renderTermsAndConditions(termsAndCons) {
        var content = [];

        content.push(
            {
                columns: [
                    [
                        {text: termsAndCons[0].header,   style: 'terms_header'},
                        {text: termsAndCons[0].description,   style: 'terms'},
                        {text: termsAndCons[2].header,   style: 'terms_header'},
                        {text: termsAndCons[2].description,   style: 'terms'},
                        {text: termsAndCons[3].header,   style: 'terms_header'},
                        {text: termsAndCons[3].description,   style: 'terms'},
                        {text: termsAndCons[4].header,   style: 'terms_header'},
                        {text: termsAndCons[4].description,   style: 'terms'},
                    ],
                    [
                        {text: termsAndCons[1].header,   style: 'terms_header'},
                        {text: termsAndCons[1].description,   style: 'terms'},
                        {text: termsAndCons[5].header,   style: 'terms_header'},
                        {text: termsAndCons[5].description,   style: 'terms'},
                    ]
                ]
            }
        );

        content.push({
            columns: [
                {
                    pageBreak: 'after',
                    text: ''
                }
            ]
        });

        content.push(

            {
                columns: [
                    [
                        {text: termsAndCons[6].header,   style: 'terms_header'},
                        {text: termsAndCons[6].description,   style: 'terms'},
                        {text: termsAndCons[8].header,   style: 'terms_header'},
                        {text: termsAndCons[8].description,   style: 'terms'},
                        {text: termsAndCons[9].header,   style: 'terms_header'},
                        {text: termsAndCons[9].description,   style: 'terms'},
                        {text: termsAndCons[10].header,   style: 'terms_header'},
                        {text: termsAndCons[10].description,   style: 'terms'},
                    ],

                    [
                        {text: termsAndCons[7].header,   style: 'terms_header'},
                        {text: termsAndCons[7].description,   style: 'terms'},
                        {text: termsAndCons[11].header,   style: 'terms_header'},
                        {text: termsAndCons[11].description,   style: 'terms'},
                        {text: termsAndCons[12].header,   style: 'terms_header'},
                        {text: termsAndCons[12].description,   style: 'terms'},
                        {text: termsAndCons[13].header,   style: 'terms_header'},
                        {text: termsAndCons[13].description,   style: 'terms'},
                    ]
                ]
            }
        );

        content.push({
            columns: [
                {
                    pageBreak: 'after',
                    text: ''
                }
            ]
        });

        content.push(

            {
                columns: [
                    [
                        {text: termsAndCons[14].header,   style: 'terms_header'},
                        {text: termsAndCons[14].description,   style: 'terms'},

                    ],

                    [
                        {text: termsAndCons[15].header,   style: 'terms_header'},
                        {text: termsAndCons[15].description,   style: 'terms'},
                        {text: termsAndCons[16].header,   style: 'terms_header'},
                        {text: termsAndCons[16].description,   style: 'terms'},
                    ]
                ]
            }
        );


        return content;
    }


    var dd = {

        content: [
            renderTermsAndConditions(termsAndConditions),
        ],

        images: {
            mySuperImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAABHCAIAAAAC1/wuAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATuSURBVHhe7dzhSxtnHMDx/SeKdrKVjdp1shdzloFvOljpCnaMUcYY1b2Y615MKXRv2u1daRxFYQzK+iYtcxskL3xhXixQ3FihJ1MaGQqxxc5YZdEZNI7Eyxp9EpPncpe7KPGX8/vj86q5u1zKfTWJT/JCa1MzALFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRjk6iZ4aMVK5sFsKXTlo20zju1R4wMupfa5iMEThley/uxkyvJeLT0R8Dvd1t6lDl2nsGh++NTzyaX3y2lt5WeznNdnr12WL80W/j90YGegqnV1Tb/2GJS6GE2tHjZFIrib/nY7+PB2/0nT6uH9bXSFTbTCM70eJknkavv1deaWtH721jeUttUMtsLT+83fdaa8kxDy/RkjHTjyP6g/UzEtU20zRIos8v3H8igy/vHi2v7d1bxnpW3Vb7ZNcf3jrbUjysiETzk4mH+7v0g/sUiWqbaRom0VxuJXq1eNV2DkaWTPXv+xtzKTLQWTismERz5qbxbbd2cJ8iUW0zjcdEV41gIDDkzs0vz79key+biZlJw3amZxMpyw+Hrfngx+rEjl2JJCv+Cs2/1ExUHrsXq9lk5Ir9ee43UXM17vA4d2bmcTJd4cfNxsT1VyzH9yMS1TbTeEw0Eerd29e9Gs6t64uxhfILN5MI9atb3xye1gs1M4v3R+zeVdrV0t03fH8xo+eQnRruUtscfKIlTyUcdPYFZ9Jqj8Js/3Xn7JF434hEtc00YhNtbvtsbFltvDsliVqfT5pPwn0dxX3tdXwaeqI3uvegDivRis8LPN51wyJRbTON3EQtl7tjosnIwLHy3W28ejX6r9qnMBISbTo3MrWh9lFDon5jvbxqG8dE83++czdTP1x0Orfq15/lt+jG9PA5das1Ufc/O5z2PcRE933XDYtEvY5jou6nrJkarj/La1Ez/tNHhV1I1EdI1OvUJ1Hnd3Qnp+eWy9/lLP8DJon6CIl6nfok6n6208m48fPXF9pLlgGRqI+QqNdxTNR9CWX2l+jK7B+j1RJdHvt8b5GQE8tLXCGJXvh+ZlPto4ZE/aa2y8txLxGJ7k42Gb32RvGA1kS354Lvnyi5RzsnPgjO6SsYJCT6+jcT69p5kajf+DvRXO6/P0e6X1QHrLB0IZuK3b1c5TMix9/uvxtLWfY87KULbW99MhR9qr+kYOmC79Ql0dRsNBQKu/RLoFctfPd4bi2nL14Lz+qr4krW6NotADTTa0vqjz7WWVqttM6uygLATGrF/oh7M3nnw53f4ZZEzfRqlf1tTosFgP5Tl0Q9TcYYandxL5V1fRVdUduqSRmBM4Vb67aM3uUUHo71GXiNwzJ6H/JZoid7QwtqWzWliTa39Xw3ZXnK6n2qfhjN5Rx0onwYzY+OVqL1+ki3yznIRPlIt18dtUR35L8YZTT6IObxi1FiD34ddffFKC5n34nmX0XvfjHKzcvvnCo/K587OokCDYlEAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQBwZqa/wd0Ed3Ls5EBJgAAAABJRU5ErkJggg=='
        },

        header:  function() {

            return {
                table: {
                    widths: [140, '*'],
                    body: [
                        [{image: 'mySuperImage', width: 140, height: 40, margin: [-5, 0, 0, -2], border: [false, false, false, true]},
                            {text: 'Allgemeine Geschäftsbedingungen der HEROLD Medien Data GmbH für\n Online-Services, wie z.B. MD Online, Online Buchungs-Tool, HEROLD Online\n Marketing Assistent und für die Platzierung von Werbung (Stand 15. März 2016)', fontSize: 10, alignment: 'right',  bold: false, margin: [0, 0, 0, 0], border: [false, false, false, true]}],
                    ]
                }, margin: [21,21]
            };
        },


        footer: function() {
            return {
                table: {
                    widths: ['*', '*'],
                    body: [
                        [{text: '',border: [false, false, false, false]},
                            {text: 'AGB Medien Data Products (V02_20160315)', fontSize: 6 , alignment: 'right',  bold: false, border: [false, false, false, false], margin: [0, 0, 0, 0]  }],
                        [{colSpan: 2, text: 'HEROLD Medien Data GmbH, D-80339 München, Garmischer Straße 4/V, Telefon: +49 89 1792633-10, Fax: +49 89 921850-20, E-Mail: info@heroldmedia.com, www.heroldmedia.com, Amtsgericht München, HRB 209 490, UID: DE294716459, Oberbank Deutschland- Kto-Nr.: 1001387040, BLZ 70120700, BIC: OBKLDEMX, IBAN: DE11701207001001387040', fontSize: 6, alignment: 'left', margin: [0, 0, 0, 0], border: [false, true, false, false]},{}]
                    ]
                }, margin: [21,0]
            };

        },


        // Styles
        styles: {
            terms_header: {
                bold: true,
                fontSize: 8.2,
                alignment: 'justify'
            },
            terms: {
                fontSize: 8.2,
                alignment: 'justify'
            },
        },

        // Default Styles
        defaultStyle: {
            columnGap: 10,
            font: 'Helvetica',
            // set font here to Calibri!!!
        },

        // Page margins
        pageMargins: [ 21, 70, 21, 55 ]
    };

    return dd;
}

