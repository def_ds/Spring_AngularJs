export function start() {

    var termsAndConditions = [
        /*0*/{header: '§ 1 Vertragsparteien\n', description: '1.1. Vertragsparteien sind die HEROLD Business Data GmbH („HEROLD“) und der Kunde. Kunden können Hotels und sonstige touristische Anbieter sein. Vertragspartner der Kunden („Gäste“) sind nicht Vertragspartei.'
        +'\n1.2. Die Leistungen von HEROLD sind regelmäßig mit Leistungen anderer Partner (z.B. seekda GmbH, Buchungsportale, PMSSoftware etc.) verknüpft bzw. abgestimmt („Partner“). Solche Leistungen werden dem Kunden direkt von dem Partner erbracht und es ist ausschließlich der Partner für die ordnungsgemäße Leistungserbringung verantwortlich, sei es dass die Leistungen vom Kunden direkt beim Partner beauftragt werden oder aufgrund von Gruppenverträgen von Partnern mit HEROLD für den'
        + 'Kunden erbracht werden. Hinsichtlich der Leistungen des Partners gelten die von dem Partner angegebenen Konditionen bzw. die Konditionen des Gruppenvertrages.'},

        /*1*/{header: '', description: '\nerfüllten Leistungen zurückzutreten. Unabhängig von dem Rücktritt ist HEROLD berechtigt, seine Leistungen bis zur vollständigen Zahlung des vereinbarten Entgelts zurückzubehalten, d.h. insbesondere das Online-Service einzustellen bzw. die vereinbarten Leistungen nicht zu erbringen.'
        + '8.4. Bei Verzug werden Verzugszinsen in der Höhe von 12% per anno sowie Mahn- und Inkassospesen verrechnet.'
        + '8.5. Für Leistungen, die aus einem Grund nicht ausgeführt werden, der nicht von HEROLD zu vertreten ist, gebührt HEROLD das vereinbarte Entgelt abzugsfrei.'
        + '8.6. Dem Preis wird eine Servicepauschale für mobile Datenaufbereitung aufgeschlagen. Die Höhe der Servicepauschale ist umsatz-abhängig und wird sowohl bei der erstmaligen Auftragserteilung, als auch bei Folgeaufträgen verrechnet.'},

        /*2*/{header: '\n§ 2 Anwendungsbereich', description: '2.1. Diese Allgemeinen Geschäftsbedingungen („AGB“) gelten für alle Rechtsgeschäfte zwischen HEROLD und dem Kunden, jeweils in der zum Zeitpunkt des Vertragsabschlusses gültigen Fassung.'
        + '\n2.2. Vertragsgrundlage der Geschäftsbeziehung zwischen HEROLD und dem Kunden sind (i) die Bestellung des Kunden („Bestellformular“), (ii) die aktuelle Leistungsbeschreibung („Leistungsbeschreibung“), (iii) die aktuelle Preisliste von HEROLD („Preisliste“), (iv) diese AGB sowie (v) sonstige schriftliche Vereinbarungen, die im Einzelfall als Vertragsinhalt vereinbart werden. Im Fall von Widersprüchen oder Abweichungen gelten die oben genannten Vertragsbestandteile in der oben genannten Reihenfolge.'
        + '\n2.3. Ein vom Kunden unterzeichnetes Bestellformular gilt als Angebot, das von HEROLD ohne Angabe von Gründen innerhalb von vier Wochen ab Unterzeichnung des Bestellformulars abgelehnt werden kann. Der Kunde ist vier Wochen ab Zugang des Angebots bei HEROLD an dieses gebunden. Das Angebot gilt als von HEROLD angenommen, wenn es nicht innerhalb dieser Frist schriftlich (auch mittels Telefax oder E-Mail) oder mündlich zurückgewiesen wurde. Die Vertreter von HEROLD sind nicht verpflichtet, die'
        + ' Zeichnungsberechtigung des Unterfertigers zu prüfen. Mangels Zurückweisung des Angebotes des Kunden durch HEROLD kommt der Vertrag zustande. Der Kunde anerkennt mit seinem Angebot (Bestellformular) diese AGB. Die AGB gelten für diesen Vertrag sowie für alle zukünftigen Verträge, auch wenn sie nicht nochmals ausdrücklich vereinbart werden. Die AGB gelten ebenfalls für nach Vertragsabschluss vorgenommene Vertragsänderungen. Allgemeine Geschäftsbedingungen des Kunden kommen nicht zur Anwendung.'},

        /*3*/{header: '\n§ 3 Beginn und Dauer des Vertrages', description: 'Das Vertragsverhältnis beginnt mangels fristgerechter Zurückweisung des Angebotes des Kunden gemäß Punkt 2.3. 30 Tage nach Unterzeichnung des Bestellformulars durch den Kunden. Das Vertragsverhältnis wird auf unbestimmte Zeit abgeschlossen und kann zum Ende eines jeden Vertragsjahres unter Einhaltung einer Kündigungsfrist von drei Monaten schriftlich gekündigt werden. Es gilt die im Bestellformular vereinbarte Mindestlaufzeit. Bei einer Kündigung vor Ablauf der Mindestvertragslaufzeit durch den Kunden hat dieser das auf die Mindestvertragslaufzeit ausständige Entgelt zu entrichten.'},

        /*4*/{header: '\n§ 4 Leistungsumfang, Auftragsabwicklung und Mitwirkungspflichten des Kunden', description: '4.1. HEROLD implementiert Online-Services zur Abwicklung von Online-Buchungen im touristischen Bereich. Darunter fallen unter anderem Services zur Erfassung von buchungsrelevanten Daten (Booking Manager), Buchungsmodule zur Verlinkung auf Webseiten und Buchungsportalen, Schnittstellen zu Buchungsplattformen sowie Schnittstellen zu Hotelverwaltungsprogrammen, wobei alle Anwendungen die Verwendung des Booking Manager als Basismodul voraussetzen. HEROLD oder dessen Partner synchronisieren die Daten mit den angeschlossenen Systemen.'
        +'\n4.2. Der Kunde wird HEROLD unverzüglich alle Informationen und Unterlagen zur Verfügung stellen, die für die Erbringung der Leistungen erforderlich sind. Er wird HEROLD von allen Vorgängen informieren, die für die Durchführung des Auftrages von Bedeutung sind, auch wenn diese Umstände erst während der Durchführung des Auftrages bekannt werden. Der Kunde trägt den Aufwand, der dadurch entsteht, dass Arbeiten infolge seiner unrichtigen, unvollständigen oder nachträglich geänderten Angaben von HEROLD wiederholt werden müssen oder verzögert werden.'
        + '\n4.3. Der Kunde ist weiters verpflichtet, die von ihm zur Verfügung gestellten Daten und Informationen, unabhängig davon ob die Daten und Informationen direkt vom Kunden oder von HEROLD in deren System oder in das System eines Partners (z.B. Buchungskanal) eingegeben werden, rechtlich auf eventuell bestehende Urheber-, Kennzeichen-, Wettbewerbsrechte oder sonstige Rechte Dritter sowie in zivil- und strafrechtlicher Hinsicht ausschließlich selbst zu prüfen. Eine Überwachung oder Überprüfung solcher Daten und Informationen durch HEROLD findet nicht statt. Eine Haftung von HEROLD bei einer Verletzung derartiger Rechte ist ausgeschlossen. Wird HEROLD wegen einer solchen Rechtsverletzung von einem Dritten, insbesondere aber auch von einem Partner in Anspruch genommen, so hält der Kunde HEROLD auf erste Anforderung vollständig schad- und klaglos. Der Kunde hat HEROLD sämtliche Nachteile, einschließlich der Kosten für die rechtliche Vertretung zu ersetzen, die durch eine Inanspruchnahme seitens Dritter entstehen.'
        + '\n4.4. Dem Kunden ist bekannt, dass irreführende, unrichtige oder rechtsverletzende Angaben (etwa zur Klassifizierung, Ausstattung, Lage oder Bezeichnung) Schadenersatz- und sonstige Ansprüche nach sich ziehen können. Der Kunde hält HEROLD für alle Ansprüche Dritter aus solchen Angaben auf erste Anforderung vollständig schad- und klaglos.'},

        /*5*/{header: '\n§ 5 Fremdleistungen / Beauftragung Dritter', description: '5.1. HEROLD ist nach freiem Ermessen berechtigt, die von ihr übernommenen Leistungen selbst auszuführen oder sich bei der Erbringung der Leistungen Dritter („Subunternehmer“) zu bedienen.'
        + '\n5.2. Sofern Leistungen unmittelbar von Dritten erbracht werden („Fremdleistungen“) erfolgt die Beauftragung entweder im eigenen Namen oder im Namen des Kunden, in jedem Fall aber auf Rechnung des Kunden. HEROLD wird solche Dritte sorgfältig auswählen und darauf achten, dass diese über die erforderliche fachliche Qualifikation verfügen.'},

        /*6*/{header: '\n§ 6 Termine und Fristen, Verzug', description: '6.1. Die von HEROLD angegebenen Fristen und Termine sind Richtwerte. Eine geringfügige Nichteinhaltung der Fristen und Termine von bis zu 14 Tagen berechtigt den Kunden nicht zur Geltendmachung von Verzugsfolgen. Darüber hinaus wird der Kunde erst dann die ihm gesetzlich zustehenden Rechte geltend machen, wenn er HEROLD unter Setzung einer angemessenen,'
        + ' mindestens aber 14-tägigen Nachfrist erfolglos gemahnt hat. Diese Frist beginnt mit dem Zugang einer schriftlichen Mahnung an HEROLD. Nach fruchtlosem Ablauf der Nachfrist kann der Kunde vom Vertrag zurücktreten.'
        + '\n6.2. Unabwendbare oder unvorhersehbare Ereignisse, insbesondere aber auch Verzögerungen bei Partnern von HEROLD entbinden HEROLD jedenfalls von der Einhaltung der vereinbarten Fristen und Termine. Gleiches gilt, wenn der Kunde mit seinen zur Durchführung der Leistungen notwendigen Verpflichtungen im Verzug ist. In diesen Fällen werden die vereinbarten Fristen und Termine zumindest im Ausmaß des Verzugs des Kunden bzw. des Partners hinausgeschoben.'},

        /*7*/{header: '\n§ 7 Auflösung aus wichtigem Grund, Verzug', description: '7.1. Der Vertrag kann jederzeit und ohne Einhaltung einer Kündigungsfrist von jeder Vertragspartei aus wichtigem Grund vorzeitig aufgelöst werden. Als wichtige Gründe, die HEROLD zur vorzeitigen Beendigung des Vertrages berechtigen, gelten insbesondere folgende Umstände:'
        + '\na) Der Kunde kommt den im Vertrag vereinbarten Zahlungen trotz Fälligkeit nicht nach;'
        + '\nb) Die finanzielle Situation des Kunden verschlechtert sich erheblich, über das Vermögen des Kunden wird ein Insolvenzverfahren eröffnet oder die Eröffnung eines Insolvenzverfahrens wird mangels kostendeckenden Vermögens abgelehnt oder ein Zwangsverwalter wird eingesetzt;'
        + '\nc) Wesentliche Änderung oder Beendigung des Vertrages mit einem oder mehreren Partnern (z.B. Buchungskanäle, Google, PMS-Hersteller etc.) aus welchem Grund auch immer;'
        + '\nd) Sonstige Nichterfüllung der vertraglichen und gesetzlichen Verpflichtungen durch den Kunden.'
        + '\n7.2. Bei Auflösung des Vertrages aus wichtigem Grund durch HEROLD hat der Kunde das Entgelt für die restliche Vertragslaufzeit vollständig und abzugsfrei zu bezahlen. Weiters haftet der Kunde bei Verschulden für einen darüber hinausgehenden Schaden.'},


        /*8*/{header: '\n§ 8 Entgelt und Fälligkeit', description: '8.1. Die Erbringung der Leistungen durch HEROLD erfolgt gegen Entgelt gemäß der jeweils gültigen Preisliste bzw. dem Bestellformular. Die Kunden stimmen zu, eine Einzugsermächtigung zugunsten HEROLD zu erteilen. Rechnungen werden dem Kunden auf dem Postweg oder auf elektronischem Weg zugestellt und sind binnen 30 Tagen ab Rechnungsdatum ohne Abzug zur Zahlung fällig. Die Rechnungslegung erfolgt unabhängig davon, ob seitens HEROLD bereits '
        + 'Leistungen erbracht wurden, sodass der Kunde vorleistungspflichtig ist. Sämtliche Entgelte sind durch Einmalzahlung für das gesamte jeweilige Vertragsjahr zu bezahlen, es sei denn es ist Zahlung in Teilbeträgen ausdrücklich und schriftlich vereinbart. HEROLD ist berechtigt, eingeräumte Zahlungsziele zu verkürzen und sämtliche Forderungen, einschließlich solcher aus anderen mit dem Kunden bestehender Verträge, fällig zu stellen, wenn der Kunde mit einer fälligen Forderung oder bei vereinbarter Teilzahlung '
        + 'mit einer Rate in Verzug gerät. Zahlungen gelten erst dann als bewirkt, wenn der Betrag auf dem Konto von HEROLD endgültig verfügbar ist. HEROLD ist berechtigt, trotz entgegenstehender Widmung des Kunden Zahlungen zur Begleichung der ältesten Rechnungsposten zzgl. Verzugszinsen und Kosten zu verwenden und zwar in der Reihenfolge: Kosten, Zinsen, Hauptforderung.'
        + '\n8.2. Für die Anbindung der PMS-Software des Kunden an die PMS-Schnittstelle des Online Buchungstools entstehende Kosten des PMS-Softwareanbieters werden dem Kunden direkt vom PMS-Softwareanbieter zusätzlich verrechnet und sind nicht vom Entgelt gemäß Punkt 8.1. umfasst.'
        + '\n8.3. Bei nicht fristgerechter Zahlung des Entgelts ist HEROLD berechtigt, von allen laufenden Verträgen bezüglich der noch nicht'},

        /*9*/{header: '\n§ 9 HEROLD als Vermittler', description: '9.1. Der Vertrag über die jeweiligen Leistungen des Kunden kommt ausschließlich zwischen dem Gast und dem Kunden zu Stande. HEROLD handelt lediglich als Vermittler zwischen dem Kunden und dem Gast und nicht im eigenen Namen und ist nicht Partei des Vertrages zwischen dem Gast und dem Kunden. '
        + '\n9.2. HEROLD haftet in keinem Fall für die Nichtverfügbarkeit einer Sache, die Gegenstand einer Reservierung war. Ein etwaiger Schaden, der durch eine solche Nichtverfügbarkeit entsteht, kann HEROLD nicht zur Last gelegt werden.'
        + '\n9.3. Sollte bei der Vermittlung von HEROLD ein Reiseveranstaltungsvertrag gemäß § 31 b ff. KSchG zwischen dem Gast und dem Kunden zustande kommen, obliegt es allein dem Kunden, für die Erfüllung der sich daraus zusätzlich ergebenden Pflichten Sorge zu tragen.'
        + '\n9.4. Der Kunde hält HEROLD auf erste Anforderung von allen Nachteilen welcher Art auch immer, einschließlich der Kosten für die rechtliche Vertretung, insbesondere wegen etwaiger Verletzungen aus dem Beherbergungsvertrag oder wegen der Qualifikation als Reiseveranstalter aufgrund von Angaben des Kunden vollständig schad- und klaglos.'},

        /*10*/{header: '\n§ 10 Änderung der Preise und des Leistungsumfangs', description: '10.1. HEROLD kann gegenüber Kunden das Entgelt unter Berücksichtigung aller in Betracht kommenden Umstände, insbesondere Veränderungen der gesetzlichen Rahmenbedingungen, Veränderungen des Personal- und Sachaufwandes, Veränderungen des Verbraucherpreisindex etc., nach billigem Ermessen ändern. Der Kunde stimmt solchen Änderungen bereits jetzt zu. Es gilt dann die jeweils aktuelle Preisliste von HEROLD.'
        +'\n10.2. Über den vorstehenden Punkt 10.1. hinausgehende Änderungen des Entgelts sowie Änderungen des Leistungsumfangs sind nur mit Zustimmung des Kunden möglich. HEROLD wird den Kunden in der Verständigung auf die jeweils gewünschte Änderung sowie auf sein Kündigungsrecht aufmerksam machen. Der Kunde hat das Recht, den Vertrag bis zum Inkrafttreten der Änderung kostenlos schriftlich zu kündigen. Festgehalten wird, dass Preisanpassungen im Ausmaß des Verbraucherpreisindex jedenfalls als Entgeltanpassung im Sinne von Punkt 10.1. gelten.'},

        /*11*/{header: '\n§ 11 Gewährleistung und Schadenersatz', description: '11.1. Den Kunden trifft die Pflicht, die erbrachte Leistung sofort auf allfällige Mängel zu untersuchen. Mängelrügen sind unverzüglich nach Leistungserbringung unter konkreter Beschreibung des Mangels schriftlich zu erheben.'
        + '\n11.2. Im Fall berechtigter und rechtzeitiger Mängelrüge steht dem Kunden nur das Recht auf Verbesserung oder Austausch der Leistung nach Wahl von HEROLD zu. Verbesserung oder Austausch erfolgt in angemessener Frist, wobei der Kunde HEROLD alle zur Untersuchung und Mängelbehebung erforderlichen Maßnahmen ermöglicht. HEROLD ist berechtigt, die Verbesserung der Leistung zu verweigern, wenn diese unmöglich, oder für HEROLD mit einem unverhältnismäßig hohen Aufwand verbunden ist.'
        + '\n11.3. Die Beweislastumkehr gemäß § 924 ABGB zu Lasten HEROLD ist ausgeschlossen.'
        + '\n11.4. Eine Haftung von HEROLD für Störungen in Bezug auf die Verfügbarkeit und die Erreichbarkeit des Internets sowie der Verbindung vom Kunden zu Partnern sowie zu Gästen ist ausgeschlossen. HEROLD haftet nicht für die Leistungen von Partnern, Netzwerkprobleme oder Systemausfälle bei Kunden oder Partnern, Buchungsfehler, die im System des Kunden oder des Partners auftreten oder die Kompatibilität der beim Kunden zum Einsatz kommenden Software mit den von HEROLD bzw. einem Partner zu '
        + 'erbringenden Leistungen. Es ist HEROLD nicht möglich, die Systeme des Kunden umfassend zu untersuchen und auf Kompatibilität zu überprüfen. HEROLD trifft diesbezüglich keine Warnpflicht und keine Haftung. HEROLD haftet nicht im Fall einer Nichterfüllung oder einer vollständigen oder teilweisen Verzögerung bei der Erfüllung, wenn dies auf höherer Gewalt beruht.'
        + '\n11.5. Ansonsten sind sämtliche Schadenersatzansprüche des Kunden ausgeschlossen, soweit sie nicht auf Vorsatz oder grober Fahrlässigkeit von HEROLD beruhen.'
        + '\n11.6. Jeder Schadenersatzanspruch kann bei sonstigem Verfall nur innerhalb von sechs Monaten ab Kenntnis des Schadens geltend gemacht werden.'
        + '\n11.7. In der Regel stehen die von HEROLD im Rahmen der Vertragserfüllung verwendeten Systeme 24 Stunden an 7 Tagen der Woche zur Verfügung. Die übliche durchschnittliche Systemverfügbarkeit innerhalb eines Vertragsjahres beträgt 99%.'
        + '\n11.8. Zur Einhaltung der Servicequalität notwendige Wartungen der Hard- und Software von HEROLD und seiner Partner, die mit längeren Serviceunterbrechungen als 30 Minuten verbunden sind, werden in der Regel in vordefinierten Wartungsfenstern jeweils täglich zwischen 22:00 Uhr und 6:00 Uhr durchgeführt. HEROLD behält sich vor, die Zeiten der vordefinierten Wartungsarbeiten einseitig zu ändern und zu erweitern. Serviceausfallszeiten, die durch Wartungsarbeiten begründet sind, zählen bei der Berechnung der Serviceverfügbarkeit nicht als nicht verfügbare Zeiten und gelten als suspendierte Zeiten.'},

        /*12*/{header: '\n§ 12 Datenschutz', description: '12.1. Der Kunde garantiert, dass die übermittelten Daten und Informationen in Übereinstimmung mit den geltenden gesetzlichen Standards verarbeitet wurden und hält HEROLD gegen jegliche Ansprüche oder Klagen Dritter schadlos, die gegen HEROLD vorgebracht werden und sich auf eine Verletzung von deren Rechten im Bereich Datenschutz berufen. Eine Haftung von HEROLD für die Korrektheit und Vollständigkeit der durch den Kunden oder für dessen Rechnung durch HEROLD oder Dritten bereitgestellten und erfassten Informationen und Daten ist ausgeschlossen.'
        + '\n12.2. HEROLD ist über die Vertragsdauer hinaus berechtigt, den Kunden, dessen Logo und die Art erbrachten Leistung auf seiner Website zu erwähnen und als Geschäftsreferenz zu nennen.'
        + '\n12.3. Der Kunde erklärt sich gemäß § 8 Abs. 1 Z 2 DSG 2000 damit einverstanden, dass HEROLD personen- und firmenbezogene Daten im Rahmen der gesetzlichen Bestimmungen erfasst, speichert, verarbeitet und überträgt, soweit dies zur Vertragsabwicklung erforderlich ist, sowie für Werbe- und Marketingzwecke verwendet. Der Kunde erklärt sich mit Angabe seiner Telefonnummer und seiner elektronischen Postadresse ausdrücklich einverstanden, von HEROLD Telefonanrufe und elektronische Post (E-Mails) zu Werbe- und Marketingzwecken, insbesondere zu Zwecken der Zusendung von Angeboten und Newsletter mit produktbezogenen Informationen und Werbematerialien sowie werblichen Informationen zum Unternehmen von HEROLD und Kunden von HEROLD, zu erhalten. Diese Zustimmungserklärung gilt über die vereinbarte oder tatsächliche Vertragsdauer hinaus, kann jedoch jederzeit durch Übermittlung eines E-Mails an kundenservice@herold.at widerrufen werden.'},

        /*13*/{header: '\n§ 13 Schlussbestimmungen', description: '13.1. Änderungen und/oder Ergänzungen des Vertrags bedürfen zu ihrer Rechtswirksamkeit der Schriftform, sofern in diesem Vertrag nicht Abweichendes vorgesehen ist. Gleiches gilt für das Abgehen von diesem Formerfordernis. Mündliche Nebenabreden bestehen nicht.'
        + '\n13.2. Sämtliche Anlagen zu dem Vertrag bilden einen integrierenden Bestandteil des Vertrages.'
        + '\n13.3. Sämtliche im Vertrag und im Bestellformular ausgewiesenen Beträge verstehen sich exklusive gesetzlicher Umsatzsteuer, sofern in dem Vertrag nicht ausdrücklich etwas Anderes vereinbart wird.'
        + '\n13.4. Es gilt österreichisches Recht unter Ausschluss von UN-Kaufrecht, des IPRG und der VO (EG) Nr. 593/2008 (Rom I). Ausschließlicher Gerichtsstand ist für beide Teile das sachlich zuständige und wertzuständige Gericht für den ersten Wiener Gemeindebezirk. Erfüllungsort ist der Sitz von HEROLD.'
        + '\n13.5. Sämtliche Rechte an der zum Einsatz kommenden Software sowie alle sonstigen Immaterialgüterrechte an den vereinbarten Leistungen stehen ausschließlich HEROLD oder seinen Partnern zu. Der Kunde erwirbt keine wie immer gearteten Nutzungs-oder sonstigen Rechte.'
        + '\n13.6. Die allfällige Unwirksamkeit und Undurchsetzbarkeit einzelner Bestimmungen dieser AGB lässt die Geltung der restlichen Bestimmungen unberührt. An die Stelle der unwirksamen oder undurchsetzbaren Bestimmung tritt eine wirksame Bestimmung, die dem wirtschaftlichen Zweck der zu ersetzenden Bestimmung möglichst nahe kommt. Das gilt auch für Vertragslücken.'
        + '\n13.7. Die Vertragsparteien verzichten, den Vertrag wegen Irrtums oder laesio enormis anzufechten sowie den Vertrag aus sonstigen Gründen zu wandeln, Vertragsanpassung zu begehren oder vom Vertrag zurückzutreten.'
        + '\n13.8. Die Aufrechnung von Forderungen des Kunden gegen Forderungen von HEROLD sowie ein Zurückbehaltungsrecht des Kunden ist ausgeschlossen.'
        + '\n13.9. Die Parteien werden den Abschluss des Vertrages und seine Bestimmungen sowie alle von der jeweils anderen Partei erhaltenen Informationen vertraulich behandeln, soweit nicht eine Offenlegung gegenüber Dritten nach dem Gesetz oder im Rahmen einer angemessenen Information der Mitarbeiter der Gesellschaft oder ihrer gewählten Vertreter geboten ist.'
        + '\n13.10. Diese Vereinbarung ist auch für alle Rechtsnachfolger der Vertragsparteien bindend. Die Vertragsparteien verpflichten sich, den Vertrag auf ihre jeweiligen Rechtsnachfolger zu überbinden. HEROLD ist berechtigt, den Vertrag auf einen Dritten zu überbinden. Der Kunde erteilt bereits jetzt seine diesbezügliche Zustimmung und verzichtet auf allfällige Widerspruchsrechte.'},

    ];


    pdfMake.fonts = {
        Helvetica: {
            normal: 'HelveticaNeueLT-LightCond.ttf',
            bold: 'HelveticaNeue-BoldCond.ttf',
        }
    };

    function renderTermsAndConditions(termsAndCons) {
        var content = [];

        content.push(

            {
                columns: [
                    [
                        {text: termsAndCons[0].header,   style: 'terms_header'},
                        {text: termsAndCons[0].description,   style: 'terms'},
                        {text: termsAndCons[2].header,   style: 'terms_header'},
                        {text: termsAndCons[2].description,   style: 'terms'},
                        {text: termsAndCons[3].header,   style: 'terms_header'},
                        {text: termsAndCons[3].description,   style: 'terms'},
                        {text: termsAndCons[4].header,   style: 'terms_header'},
                        {text: termsAndCons[4].description,   style: 'terms'},
                        {text: termsAndCons[5].header,   style: 'terms_header'},
                        {text: termsAndCons[5].description,   style: 'terms'},
                        {text: termsAndCons[6].header,   style: 'terms_header'},
                        {text: termsAndCons[6].description,   style: 'terms'},
                        {text: termsAndCons[7].header,   style: 'terms_header'},
                        {text: termsAndCons[7].description,   style: 'terms'},
                        {text: termsAndCons[8].header,   style: 'terms_header'},
                        {text: termsAndCons[8].description,   style: 'terms'},

                    ],

                    [
                        {text: termsAndCons[1].header,   style: 'terms_header'},
                        {text: termsAndCons[1].description,   style: 'terms'},
                        {text: termsAndCons[9].header,   style: 'terms_header',},
                        {text: termsAndCons[9].description,   style: 'terms'},
                        {text: termsAndCons[10].header,   style: 'terms_header'},
                        {text: termsAndCons[10].description,   style: 'terms'},
                        {text: termsAndCons[11].header,   style: 'terms_header',},
                        {text: termsAndCons[11].description,   style: 'terms'},
                        {text: termsAndCons[12].header,   style: 'terms_header',},
                        {text: termsAndCons[12].description,   style: 'terms'},
                        {text: termsAndCons[13].header,   style: 'terms_header',},
                        {text: termsAndCons[13].description,   style: 'terms'},

                    ]
                ]
            }
        );

        return content;
    }


    var dd = {

        content: [
            renderTermsAndConditions(termsAndConditions),
        ],

        images: {
            mySuperImage: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAABHCAIAAAAC1/wuAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAATuSURBVHhe7dzhSxtnHMDx/SeKdrKVjdp1shdzloFvOljpCnaMUcYY1b2Y615MKXRv2u1daRxFYQzK+iYtcxskL3xhXixQ3FihJ1MaGQqxxc5YZdEZNI7Eyxp9EpPncpe7KPGX8/vj86q5u1zKfTWJT/JCa1MzALFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRjk6iZ4aMVK5sFsKXTlo20zju1R4wMupfa5iMEThley/uxkyvJeLT0R8Dvd1t6lDl2nsGh++NTzyaX3y2lt5WeznNdnr12WL80W/j90YGegqnV1Tb/2GJS6GE2tHjZFIrib/nY7+PB2/0nT6uH9bXSFTbTCM70eJknkavv1deaWtH721jeUttUMtsLT+83fdaa8kxDy/RkjHTjyP6g/UzEtU20zRIos8v3H8igy/vHi2v7d1bxnpW3Vb7ZNcf3jrbUjysiETzk4mH+7v0g/sUiWqbaRom0VxuJXq1eNV2DkaWTPXv+xtzKTLQWTismERz5qbxbbd2cJ8iUW0zjcdEV41gIDDkzs0vz79key+biZlJw3amZxMpyw+Hrfngx+rEjl2JJCv+Cs2/1ExUHrsXq9lk5Ir9ee43UXM17vA4d2bmcTJd4cfNxsT1VyzH9yMS1TbTeEw0Eerd29e9Gs6t64uxhfILN5MI9atb3xye1gs1M4v3R+zeVdrV0t03fH8xo+eQnRruUtscfKIlTyUcdPYFZ9Jqj8Js/3Xn7JF434hEtc00YhNtbvtsbFltvDsliVqfT5pPwn0dxX3tdXwaeqI3uvegDivRis8LPN51wyJRbTON3EQtl7tjosnIwLHy3W28ejX6r9qnMBISbTo3MrWh9lFDon5jvbxqG8dE83++czdTP1x0Orfq15/lt+jG9PA5das1Ufc/O5z2PcRE933XDYtEvY5jou6nrJkarj/La1Ez/tNHhV1I1EdI1OvUJ1Hnd3Qnp+eWy9/lLP8DJon6CIl6nfok6n6208m48fPXF9pLlgGRqI+QqNdxTNR9CWX2l+jK7B+j1RJdHvt8b5GQE8tLXCGJXvh+ZlPto4ZE/aa2y8txLxGJ7k42Gb32RvGA1kS354Lvnyi5RzsnPgjO6SsYJCT6+jcT69p5kajf+DvRXO6/P0e6X1QHrLB0IZuK3b1c5TMix9/uvxtLWfY87KULbW99MhR9qr+kYOmC79Ql0dRsNBQKu/RLoFctfPd4bi2nL14Lz+qr4krW6NotADTTa0vqjz7WWVqttM6uygLATGrF/oh7M3nnw53f4ZZEzfRqlf1tTosFgP5Tl0Q9TcYYandxL5V1fRVdUduqSRmBM4Vb67aM3uUUHo71GXiNwzJ6H/JZoid7QwtqWzWliTa39Xw3ZXnK6n2qfhjN5Rx0onwYzY+OVqL1+ki3yznIRPlIt18dtUR35L8YZTT6IObxi1FiD34ddffFKC5n34nmX0XvfjHKzcvvnCo/K587OokCDYlEAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQB0UgUEI1EAdFIFBCNRAHRSBQQjUQBwZqa/wd0Ed3Ls5EBJgAAAABJRU5ErkJggg=='
        },

        header:  function() {

            return {
                table: {
                    widths: [140, '*'],
                    body: [
                        [{image: 'mySuperImage', width: 140, height: 40, margin: [-5, 0, 0, -2], border: [false, false, false, true]},
                            {text: 'Allgemeine Geschäftsbedingungen\n für das Online Buchungs-Tool', fontSize: 15, alignment: 'right',  bold: false, margin: [0, 0, 0, 0], border: [false, false, false, true]}],
                    ]
                }, margin: [21,21]
            };
        },


        footer: function() {
            return {
                table: {
                    widths: ['*', '*'],
                    body: [
                        [{text: '',border: [false, false, false, false]},
                            {text: 'AGB Online Buchungs-Tool (V04_21052014)', fontSize: 6 , alignment: 'right',  bold: false, border: [false, false, false, false], margin: [0, 0, 0, 0]  }],
                        [{colSpan: 2, text: 'HEROLD Business Data GmbH, A-2340 Mödling, Guntramsdorfer Straße 105, Telefon: +43 (0) 2236/401-38133, Fax +43 (0) 2236/401-35160 E-Mail: kundenservice@herold.at, www.herold.at Gerichtsstand Wien, UID: ATU57270679, DVR: 0871885, FN: 233171z AGB_CMS_Applikation (V19_20170522)', fontSize: 6, alignment: 'left', margin: [0, 0, 0, 0], border: [false, true, false, false]},{}]
                    ]
                }, margin: [21,0]
            };

        },


        // Styles
        styles: {
            terms_header: {
                bold: true,
                fontSize: 6,
                alignment: 'justify'
            },
            terms: {
                fontSize: 6,
                alignment: 'justify'
            },
        },

        // Default Styles
        defaultStyle: {
            columnGap: 10,
            font: 'Helvetica',
            // set font here to Calibri!!!
        },

        // Page margins
        pageMargins: [ 21, 70, 21, 55 ]
    };

    return dd;
}