import PdfService from './pdf.service';

export default app => {
    app.service('pdfService', PdfService);
}