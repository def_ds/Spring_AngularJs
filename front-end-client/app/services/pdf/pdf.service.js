import {start as a} from './content/AGB_Adwords-Kampagnen_V04_20140521';
import {start as b} from './content/AGB_CDRom_DVD_Datentraeger_V13_20150909_120x180_PRINT';
import {start as c} from './content/AGB_CMS_Applikation_V19_20170522';
import {start as d} from './content/AGB_Domain_Bereitstellung_V03_20160901';
import {start as e} from './content/AGB_MD_Offline_V09_20170531';
import {start as f} from './content/AGB_MD_Online_Business_V09_20170531';
import {start as g} from './content/AGB_MD_Online_Consumer_V02_15092014_WEB';
import {start as h} from './content/AGB_Medien_Data_DE_Products_V02_20160315';
import {start as i} from './content/AGB_Online_Buchungs_Tool_V04_21052014';
import {start as j} from './content/AGB_Produktsuche_V02_20150217';
import {start as k} from './content/Angebot_Datum_20170912_1537';

class PdfService {

    constructor($http) {
        this.$http = $http;
        this.dataUrl = 'http://localhost:8082';
    }

   create(str){
       let aa;
       switch (str) {
           case "a":
                aa = a();
               pdfMake.createPdf(aa).open();
               break;
           case "b":
               aa = b();
               pdfMake.createPdf(aa).open();
               break;
           case "c":
                aa = c();
               pdfMake.createPdf(aa).open();
               break;
           case "d":
                aa = d();
               pdfMake.createPdf(aa).open();
               break;
           case "e":
               aa = e();
               pdfMake.createPdf(aa).open();
               break;
           case "f":
               aa = f();
               pdfMake.createPdf(aa).open();
               break;
           case "g":
                aa = g();
               pdfMake.createPdf(aa).open();
               break;
           case "h":
               aa = h();
               pdfMake.createPdf(aa).open();
               break;
           case "i":
               aa = i();
               pdfMake.createPdf(aa).open();
               break;
           case "j":
                aa = j();
               pdfMake.createPdf(aa).open();
               break;
           case "k":
               aa = k();
               pdfMake.createPdf(aa).open();
       }
   }

}
export default PdfService;