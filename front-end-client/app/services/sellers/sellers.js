import SellersService from './sellers.service';

export default app => {
    app.service('sellersService', SellersService);
}
