const webpack = require('webpack');
const path = require('path');
const { dirname, join} = require('path');
var nodeEnvironment = process.env.NODE_ENV;
var bourbon = require('node-bourbon').includePaths;
const nodeModulesDirectory = path.resolve(__dirname, './node_modules');
// const OpenBrowserPlugin = require('open-browser-webpack-plugin');

module.exports = {
    entry: "./app/index.js",
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin({
            'INCLUDE_ALL_MODULES': function includeAllModulesGlobalFn(modulesArray, application) {
                modulesArray.forEach(function executeModuleIncludesFn(moduleFn) {
                    moduleFn(application);
                });
            },
            ENVIRONMENT: JSON.stringify(nodeEnvironment)
        })
    ],
    output: {
        path:__dirname+ '/dist/',
        filename: "bundle.js",
        publicPath: '/'
    },
    devServer: {
        inline: false,
        contentBase: "./dist",
    },
    module: {
        rules: [
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            {
                test: /\.scss$/,
                loader: `style-loader!css-loader!sass-loader?includePaths[]=${nodeModulesDirectory}`
            },
            {
                test: /\.css$/,
                loader: 'css-loader?includePaths[]=' + bourbon
            },
            {
                test: /\.js[x]?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'babel-loader',
                    }
                ]
            },
        ],
    },
};
